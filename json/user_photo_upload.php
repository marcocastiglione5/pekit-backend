<?php
$DB_USER='pekits_admin';
$DB_PASS='4dm1n';
$DB_HOST='localhost';
$DB_NAME='pekits_db';

extract($_POST);
extract($_GET);

if (!isset($userId)) {
	$response['status'] = 'false';
	$response['message'] = 'User id required.';
	echo json_encode($response);
	exit();
}

$mysqli = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

if (!isset($name)) {
	$name = '';
}
if($_FILES['userPhoto']['error'] > 0) {
	$response['status'] = 'false';
	$response['message'] = 'Error during uploading, try again';
	echo json_encode($response);
	$mysqli->close();
	exit();
}
$extsAllowed = array( 'jpg', 'jpeg', 'png', 'gif' );
$extUpload = strtolower( substr( strrchr($_FILES['userPhoto']['name'], '.') ,1) ) ;
if (in_array($extUpload, $extsAllowed) ) {
	$now = date("Y-m-d H:i:s");
	$fileName = idate('U').".$extUpload";
	$photoName = "$_SERVER[DOCUMENT_ROOT]/uploaded/$fileName";
	$photoURL = "http://www.pekits.info/uploaded/$fileName";
	$result = move_uploaded_file($_FILES['userPhoto']['tmp_name'], $photoName);
	if($result) {
		$sql = "UPDATE usuario SET foto = '$photoURL' WHERE id_usuario = $userId";
		$mysqli->query($sql);
		$response['status'] = 'true';
		$response['message'] = '¡La foto se cargó correctamente!';
		$response['userPhoto'] = $photoURL;
		$response['responseCode'] = 200;
		echo json_encode($response);
		$mysqli->close();
	} else {
		$response['status'] = 'false';
		$response['message'] = 'Error during uploading, try again';
		echo json_encode($response);
		$mysqli->close();
	}
} else {
	$response['status'] = 'false';
	$response['message'] = 'File is not valid. Please try again';
	echo json_encode($response);
	$mysqli->close();
}
?>


