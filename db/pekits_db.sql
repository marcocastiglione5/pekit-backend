-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 07, 2020 at 09:26 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pekits_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `apimaps`
--

CREATE TABLE `apimaps` (
  `id_lugar` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `icono` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `rating` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `types` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `user_ratings_total` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `longitud` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `palabra` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL,
  `id_municipio` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `zoom` int(11) NOT NULL,
  `apimap` int(11) NOT NULL,
  `telefono` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `horario` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `apimaps`
--

INSERT INTO `apimaps` (`id_lugar`, `nombre`, `direccion`, `icono`, `rating`, `types`, `user_ratings_total`, `latitud`, `longitud`, `palabra`, `extra`, `id_municipio`, `id_tipo`, `zoom`, `apimap`, `telefono`, `horario`) VALUES
(1, 'Hospital Veterinario Alvarez Camara', 'Calle Herschel 37, Anzures, Miguel Hidalgo, 11590 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '7', '19.4305794', '-99.1798303', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(2, 'CONSULTORIO VETERINARIO \"JOE\"', 'Esquina Atlixco, Campeche 405, Hipódromo Condesa, Cuauhtémoc, 06170 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '16', '19.4105974', '-99.1760638', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(3, 'Veterinaría Larami', 'Av 499 12, San Juan de Aragón VII Secc, Gustavo A. Madero, 07920 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '119', '19.4665157', '-99.0723621', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(4, 'Hospital Veterinario Durango', 'Calle de Durango 127, Roma Nte., Cuauhtémoc, 06700 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '319', '19.4208156', '-99.1624059', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(5, 'Veterinaria Farrera', 'Torres Adalid 704, Col del Valle Centro, Benito Juárez, 03100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '63', '19.3913453', '-99.1659063', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(6, 'Golden Pets, Veterinaria, Estética y Boutique', 'Acc. B, Moctezuma 102, Guerrero, Cuauhtémoc, 06300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest store establishment', '47', '19.443562', '-99.143878', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(7, 'Hospital Veterinario MEVET', 'Avenida Patriotismo 452, San Pedro de los Pinos, Benito Juárez, 03800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '124', '19.3878572', '-99.1826803', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(8, 'HOSPITAL VETERINARIO MAYAN PETS', 'Calle 307 202, Nueva Atzacoalco, Gustavo A. Madero, 07420 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '213', '19.4913129', '-99.0906695', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(9, 'Veterinaria San Pedro', 'Abraham Sánchez 112, San Pedro Xalpa, Azcapotzalco, 02719 Azcapotzalco, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '66', '19.4776896', '-99.2150746', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(10, 'Clinica Veterinaria', 'Lago Constanza 155, Lago Sur, Miguel Hidalgo, 11460 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '40', '19.4451023', '-99.1873327', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(11, 'VETERINARIA RODRIGUEZ', 'Beethoven 147, Peralvillo, Cuauhtémoc, 06220 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '73', '19.4616049', '-99.1365451', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(12, 'Clínica Veterinaria Cuenta Conmigo', 'Viad. Río de la Piedad 270-A, Asturias, Cuauhtémoc, 06850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '41', '19.4035896', '-99.1348872', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(13, 'Centro Veterinario México', 'Cincinnati 26, Cd. de los Deportes, Benito Juárez, 03710 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '1249', '19.3819683', '-99.1806074', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(14, 'VETERINARIA KANEM', 'Diego Becerra 59, San José Insurgentes, Benito Juárez, 03900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '27', '19.3663608', '-99.1854979', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(15, 'Camcor, Hospital Veterinario', 'Barranca del Muerto 490, Los Alpes, Álvaro Obregón, 01010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '95', '19.36356', '-99.192438', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(16, 'Consultorio Veterinario', 'Blvd. Miguel de Cervantes Saavedra 625, Col. Irrigación, Miguel Hidalgo, 11500 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '9', '19.4422039', '-99.21423', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(17, 'VETERINARIA Y ESTETICA CANINA', 'Avenida Girasol 54, INFONAVIT Iztacalco, Iztacalco, 08900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '48', '19.3838151', '-99.1031168', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(18, 'Consultorio Veterinario', 'Avenida Prado Norte 403, Lomas - Virreyes, Lomas de Chapultepec V Secc., Miguel Hidalgo, 11000 Ciudad de México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '7', '19.4276223', '-99.2109229', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(19, 'Especialidades Veterinarias del Sur', 'Prol Tajín 862, Sta Cruz Atoyac, Benito Juárez, 03310 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '24', '19.3687473', '-99.1573762', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(20, 'Veterinaria Casper', 'Rosario 268, Residencial Zacatenco, Gustavo A. Madero, 07369 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care point_of_interest establishment', '54', '19.5069541', '-99.125942', 'veterinario', 0, 1, 1, 13, 1, '', 0),
(21, 'Veterinario Israel Camacho Macotela', 'Calle Vicente Sta. María 1983, Félix Ireta, 58070 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '44', '19.6849903', '-101.1880278', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(22, 'Centro Veterinario Inara', 'Gardenia 48, Las Flores, 58160 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '7', '19.7025051', '-101.2067635', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(23, 'Clinica Veterinaria Puppys', 'Tte. Aleman 510, Chapultepec Sur, 58260 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '12', '19.6899723', '-101.1658351', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(24, 'Urgencias Veterinarias', 'Antonio Alzate 800, Cuauhtémoc, 58000 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.9', ' veterinary_care point_of_interest establishment', '11', '19.7005739', '-101.1836082', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(25, 'Veterinaria Magic Pets', 'Av. Fuentes de Morelia 195, Fuentes de Morelia, 58088 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care food point_of_interest store establishment', '22', '19.672594', '-101.204306', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(26, 'Veterinaria Lomas', 'Av. Montaña Monarca 2000, Int. 43, Altozano, Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '11', '19.6574759', '-101.1661487', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(27, 'Veterinaria Servicentro', 'Calle Artilleros de 1847 1520, Artilleros 47, 58260 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '66', '19.6894782', '-101.1596981', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(28, 'Hospital Estética veterinaria Pupet', 'Calle Tte. Coronel José Ma. Olvera 50, Nueva Chapultepec, 58260 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care point_of_interest establishment', '43', '19.688146', '-101.166568', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(29, 'Veterinaria Nuske', 'Juan Antonio Riaño 21, Camelinas, 58290 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care health point_of_interest establishment', '72', '19.6826521', '-101.1688845', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(30, 'Clinica Veterinaria Pupet', 'Blvd. García de León 710, Chapultepec Sur, 58260 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '10', '19.6901273', '-101.1730314', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(31, 'Centro Médico Veterinario Valle Real', 'Paseo Valle Real 2063, Valle Real, 58880 Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '138', '19.7744641', '-101.126233', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(32, 'Veterinaria Animal Pet', 'Av. Torreon Nuevo, Loma Real, 58116 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.6', ' veterinary_care point_of_interest establishment', '13', '19.7299374', '-101.198633', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(33, 'Granero y Veterinaria Black Mine', 'Francisco I Madero 2155, Isaac Arriaga, 58210 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' veterinary_care food point_of_interest store establishment', '15', '19.7075425', '-101.1720572', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(34, 'Veterinaria Valladolid', 'Manantial Mintzita 173, Manantiales, 58188 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '37', '19.7002436', '-101.2376323', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(35, 'Hospital Veterinario', 'Calz La Huerta 2129 A, Los Pinos de Michoacán, 58057 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care point_of_interest establishment', '7', '19.6857924', '-101.2148027', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(36, 'Clinica Veterinaria Y Estetica Canina \"Dr. Vet\"', 'Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '7', '19.6987755', '-101.250309', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(37, 'Veterinaria Zoomundo', 'Av. Torreon Nuevo 2111, 58160 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '30', '19.7393753', '-101.1996144', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(38, 'Veterinaria Vasco', 'Mercado Municipal, Madrigal de Las Altas Torres Local 12, Vasco de Quiroga, 58230 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '2', '19.7002888', '-101.173057', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(39, 'Veterinaria El Perro Negro', 'Av. Torreon Nuevo 1809, Pob Gertrudis Sánchez, 58116 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '8', '19.7366109', '-101.1993754', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(40, 'Veterinaria Boldy', 'Av. Pedregal 16, Int. B, Eduardo Ruíz, 58149 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '10', '19.7204044', '-101.2194751', 'veterinario', 0, 2, 1, 13, 1, '', 0),
(41, 'VETERINARIA DINO CENTRO HISTORICO', 'Av Arcos de Belén 62, Doctores, Cuauhtémoc, 06720 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '53', '19.4268849', '-99.1468692', 'veterinario', 0, 3, 1, 13, 1, '', 0),
(42, 'Clínica Veterinaria Coyoacán', 'Av. División del Nte. 2617, Del Carmen, Coyoacán, 04100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '87', '19.3537379', '-99.1521663', 'veterinario', 0, 3, 1, 13, 1, '', 0),
(43, 'Centro Médico Veterinario Del Valle', 'Calle de Artemio del Valle Arizpe 19, Col del Valle Centro, Benito Juárez, 03100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '29', '19.3962473', '-99.1710465', 'veterinario', 0, 3, 1, 13, 1, '', 0),
(44, 'Atención Medico Veterinaria Hospital de Especialidades', 'Calz. de Tlalpan 2247, Cd Jardín, Coyoacán, 04370 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care health point_of_interest establishment', '5', '19.3376142', '-99.1428218', 'veterinario', 0, 3, 1, 13, 1, '', 0),
(45, 'Acuamas Pet Shop, Veterinaria y Estética Canina', 'Lorenzo Boturini 339-A, Tránsito, Cuauhtémoc, 06820 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '77', '19.4173922', '-99.1285483', 'veterinario', 0, 3, 1, 13, 1, '', 0),
(46, 'Clínica Veterinaria Culhuacán', 'Av. Tlahuac 413, San Antonio Culhuacan, Iztapalapa, 09800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '13', '19.3395012', '-99.1090588', 'veterinario', 0, 3, 1, 13, 1, '', 0),
(47, 'Clínica Veterinaria VetMD', 'Av. Pdte. Plutarco Elías Calles 1364, Reforma Iztaccihuatl Sur, Iztacalco, 08840 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '37', '19.3777389', '-99.1351626', 'veterinario', 0, 3, 1, 13, 1, '', 0),
(48, 'Veterinaria Xamigua', 'Parroquia # 622 Local A esq. con Martín Mandalde, Del Valle, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '15', '19.369596', '-99.170639', 'veterinario', 0, 3, 1, 13, 1, '', 0),
(49, 'Medica Veterinaria Bichos', 'Efeso 9, Lomas Estrella 2da Secc, Iztapalapa, 09890 Iztapalapa, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '26', '19.3182817', '-99.0969247', 'veterinario', 0, 3, 1, 13, 1, '', 0),
(50, 'VETERINARIA Benji', 'Av. Tlahuac 9, Lomas Estrella 1ra Secc, Iztapalapa, 09880 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '17', '19.3226791', '-99.0966699', 'veterinario', 0, 3, 1, 13, 1, '', 0),
(51, 'Hospital Veterinario Kiin', 'Gral. Jesús Carranza 6, Ricardo Flores Magon, 53820 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.8', ' veterinary_care health point_of_interest establishment', '27', '19.4306068', '-99.247267', 'veterinario', 0, 4, 1, 13, 1, '', 0),
(52, 'Consultorio Veterinario \"Nosuet\"', 'Av, San Agustín #115, Lomas de San Agustin, 53490 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '61', '19.4609146', '-99.2581531', 'veterinario', 0, 4, 1, 13, 1, '', 0),
(53, 'Dinovet', 'Av Observatorio 457, Las Palmas, Álvaro Obregón, 01120 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.9', ' veterinary_care point_of_interest establishment', '234', '19.399738', '-99.2095814', 'veterinario', 0, 4, 1, 13, 1, '', 0),
(54, 'Centro Veterinario Interlomas', 'L-02, Blvrd. Interlomas No. 5, Parques de la Herradura, 52786 Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '34', '19.4024304', '-99.2697177', 'veterinario', 0, 4, 1, 13, 1, '', 0),
(55, 'Cientifica Veterinaria', 'Vasco de Quiroga 1608, Margarita Maza de Juárez, Álvaro Obregón, 01250 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '103', '19.382353', '-99.238116', 'veterinario', 0, 4, 1, 13, 1, '', 0),
(56, 'Dr. Yamaguchi', 'Av Paseo de los Leones 2831, Cumbres 4º. Sector Secc a, 64619 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '23', '25.7193336', '-100.3857188', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(57, 'Clínica Veterinaria 2000', 'Burócratas 512, Las Cumbres, 64610 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '63', '25.7088436', '-100.3696993', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(58, 'Agrorrey Distribuidor Veterinario', 'Calle Nueva Inglaterra 4046, Industrial Hab Abraham Lincoln, 64320 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '20', '25.7115111', '-100.3563088', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(59, 'K-NINO Clinica Veterinaria', 'Av Lázaro Cárdenas 4231, Torrebrisas, Las Brisas 5o. Sector, 64790 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care health point_of_interest store establishment', '27', '25.6192736', '-100.2814432', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(60, 'Zago Veterinaria y Hotel', 'Sendero Sur 2018, Valle del Márquez, 64790 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest store establishment', '47', '25.6273655', '-100.2938656', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(61, 'San Jemo Hospital Veterinario', 'Av Insurgentes 1600, San Jeronimo, Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' veterinary_care point_of_interest establishment', '7', '25.6803469', '-100.3651165', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(62, 'Centro Veterinario Monterrey', '5 de Mayo #847, Oriente, Centro, 64000 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care point_of_interest establishment', '13', '25.672806', '-100.306816', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(63, 'VETERINARIA EL COLMILLO', 'Pelicano 5363, Valle Verde 2 Sector, Valle Verde, 64117 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '77', '25.7355587', '-100.3772109', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(64, 'CLÍNICA Y ESTÉTICA VETERINARIA REGIO VET', 'Av Adolfo Ruiz Cortines Poniente 12OO-L5, La Purísima, 67130 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '38', '25.7031292', '-100.2355485', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(65, 'Veterinaria Cedros', 'Av. San José 4870, Los Cedros, 64370 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care health point_of_interest establishment', '6', '25.7169386', '-100.3715516', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(66, 'CEU. CLINICA VETERINARIA', 'Vista IE state, Av. Benito Juárez 1301-S AGUA Y DRENAJE, Sin Nombre de Col 31, vista city, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '189', '25.6795286', '-100.2413308', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(67, 'Hoca Veterinaria', 'Av. Linda Vista #306, Aragonés, 67124 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '51', '25.6996937', '-100.2546769', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(68, 'Veterinaria Alemán', 'Av Paseo de los Leones & 8a. Avenida, Cumbres 1º Sector, 64610 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '35', '25.6981893', '-100.3589108', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(69, 'Veterinaria Happy Dog', 'Calle Castelar Juan Pablo II 489, Independencia, 64720 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.6', ' veterinary_care health point_of_interest establishment', '18', '25.6587774', '-100.3207956', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(70, 'Love Hospital Animal', 'Santiago Tapia Pte. 1404, Centro, 64000 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' pet_store veterinary_care point_of_interest store establishment', '624', '25.6809029', '-100.3264162', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(71, 'Veterinaria Dog - Go Hospital Veterinario, Estética Canina', 'Paseo de Las Americas Pte. 2457, Contry La Silla 4o Sector, 67173 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '22', '25.6320562', '-100.2695196', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(72, 'orejitas y colitas Clinica Veterinaria y Estética Canina', 'Calle Antonio I. Villarreal 2112, Nueva Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '12', '25.6930534', '-100.277738', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(73, 'VETERINARIA PUPPY', 'General Plutarco Elías Calles 400 poniente, Nueva Exposición, 67150 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '82', '25.6804065', '-100.2471334', 'veterinario', 0, 13, 1, 13, 1, '', 0),
(74, 'Clínica Veterinaria del Bosque', 'Hacienda de Temixco 8, Bosques de Echegaray, 53310 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '424', '19.4867757', '-99.2319936', 'veterinario', 0, 6, 1, 13, 1, '', 0),
(75, 'Servicio Medico Veterinario Integral', 'Rtno. 2 Ingenieros Militares 30, Lomas de Sotelo, Miguel Hidalgo, 11200 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '155', '19.450818', '-99.215674', 'veterinario', 0, 6, 1, 13, 1, '', 0),
(76, 'Clínica veterinaria Azcapotzalco', 'Av Azcapotzalco 712, Centro de Azcapotzalco, Azcapotzalco, 02000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '126', '19.4840599', '-99.1858865', 'veterinario', 0, 6, 1, 13, 1, '', 0),
(77, 'Veterinaria Animalitos', 'Av Centenario 394, Merced Gómez, Álvaro Obregón, 01600 Álvaro Obregón, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '39', '19.3660559', '-99.2091167', 'veterinario', 0, 6, 1, 13, 1, '', 0),
(78, 'CLINICA VETERINARIA REVOLUCION', 'Av. Revolución 1133, Merced Gómez, Benito Juárez, 03900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '27', '19.3655801', '-99.1890349', 'veterinario', 0, 6, 1, 13, 1, '', 0),
(79, 'VETERINARIO', 'Av Centenario 965, Arcos de Centenario, Álvaro Obregón, 01560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '21', '19.3618321', '-99.225632', 'veterinario', 0, 6, 1, 13, 1, '', 0),
(80, 'Clinica Veterinaria Dr. Palma', 'García Torres 27, Periodista, Miguel Hidalgo, 11220 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '60', '19.4484888', '-99.2182701', 'veterinario', 0, 6, 1, 13, 1, '', 0),
(81, '7 VENADO Clínica Médico veterinaria', 'Avenida Jesús del Monte 146, Jesús del Monte, Cuajimalpa de Morelos, 05260 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' veterinary_care point_of_interest establishment', '89', '19.3640975', '-99.2932309', 'veterinario', 0, 6, 1, 13, 1, '', 0),
(82, 'Veterinaria Patita de Perro', 'Leonardo Da Vinci 92, Mixcoac, Benito Juárez, 03910 Benito Juárez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '15', '19.377037', '-99.1908157', 'veterinario', 0, 6, 1, 13, 1, '', 0),
(83, 'Centro Veterinario Granada', 'Calle Gral. Juan Cano 89, San Miguel Chapultepec I Secc, Miguel Hidalgo, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '92', '19.4117223', '-99.185276', 'veterinario', 0, 7, 1, 13, 1, '', 0),
(84, 'Centro Veterinario Animal Health', 'Lorenzo Buturini 393 L-8, Lorenzo Buturini, 15820 Venustiano Carranza, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '37', '19.417095', '-99.126255', 'veterinario', 0, 7, 1, 13, 1, '', 0),
(85, 'Veterinaria Zoo', 'Ahuanusco 85, Pedregal de Santo Domingo, Coyoacán, 04369 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '75', '19.322437', '-99.16972', 'veterinario', 0, 7, 1, 13, 1, '', 0),
(86, 'Spa & Consultorio Veterinario Kiara', 'Indianapolis 86, Nápoles, Benito Juárez, 03810 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.1', ' veterinary_care point_of_interest establishment', '63', '19.396598', '-99.1783454', 'veterinario', 0, 7, 1, 13, 1, '', 0),
(87, 'CLÍNICA VETERINARIA REAL', 'Av. San Jerónimo & Cruz Verde, Lomas Quebradas, La Magdalena Contreras, 10200 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '96', '19.3153866', '-99.2416125', 'veterinario', 0, 7, 1, 13, 1, '', 0),
(88, 'Clínica Veterinaria El Limbo', 'Calz. Desierto de los Leones 6287, Lomas de la Era, Álvaro Obregón, 01860 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '44', '19.3260718', '-99.2625807', 'veterinario', 0, 7, 1, 13, 1, '', 0),
(89, 'Clinica Veterinaria Delegacional Tlalpan', 'Becal Mza 98, Lomas de Padierna, Tlalpan, 14200 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '919', '19.2927412', '-99.2256218', 'veterinario', 0, 7, 1, 13, 1, '', 0),
(90, 'CLINICA VETERINARIA CAN ANGELES', 'Av. Revolución 110, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '44', '19.403928', '-99.184346', 'veterinario', 0, 8, 1, 13, 1, '', 0),
(91, 'Centro Veterinario Viveros', 'Calle Viveros de la Floresta 75, Hab Viveros de la Loma, 54080 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '51', '19.5224106', '-99.2179204', 'veterinario', 0, 8, 1, 13, 1, '', 0),
(92, 'Can Royal, Servicios Veterinarios', 'Paseo de Francia 163, Lomas Verdes 3ra Secc, 53125 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '98', '19.5181027', '-99.2561346', 'veterinario', 0, 8, 1, 13, 1, '', 0),
(93, 'Cihuacoatl Veterinaria', 'Piña 215 BIS, Nueva Santa María, Azcapotzalco, 02800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '394', '19.4663969', '-99.1701914', 'veterinario', 0, 8, 1, 13, 1, '', 0),
(94, 'Centro Veterinario Satélite', 'Cto. Oradores 10, Cd. Satélite, 53100 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care point_of_interest store establishment', '33', '19.519245', '-99.24547', 'veterinario', 0, 8, 1, 13, 1, '', 0),
(95, 'Veterinaria Garritas', 'Gral. José Montesinos 51, Daniel Garza al Poniente, Miguel Hidalgo, 11840 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '94', '19.4062241', '-99.1971126', 'veterinario', 0, 8, 1, 13, 1, '', 0),
(96, 'Clinica Veterinaria Plaza Vallejo', 'Calz. Vallejo 1111, Patera Vallejo I Secc, Gustavo A. Madero, 07710 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care food health point_of_interest store establishment', '80', '19.5034871', '-99.1564502', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(97, 'Clínica veterinaria Kitti', '2 L-7, Paseo de Las Palomas, plaza palomas, Las Alamedas, 52970 Cd López Mateos, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' pet_store veterinary_care food point_of_interest store establishment', '58', '19.5558424', '-99.2436135', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(98, 'Veterinaria \"La Potra Zaina\"', 'Av Cuautepec 22, Jorge Negrete, Gustavo A. Madero, 07280 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '55', '19.5232076', '-99.1421862', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(99, 'Clínica Veterinaria \"Chuchos y Mininos\"', 'Bulevar de Los Continentes 35, Valle Dorado, 54020 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care lodging point_of_interest store establishment', '263', '19.55171', '-99.2113642', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(100, 'Centro Medico Veterinario \"Animal Love\"', 'Calle Roma 172,Hab Izcalli Piramide,54140 Tlalnepantla, Méx., Izcalli Piramide, 54140 Ciudad de México, Méx., M', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care health point_of_interest establishment', '24', '19.540847', '-99.173461', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(101, 'Clinica Veterinaria Dr. Mascota #drmascotacdmx', 'Av Cuautepec 70, Jorge Negrete, Gustavo A. Madero, 07280 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '45', '19.526759', '-99.1419112', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(102, 'Veterinaria San Francisco de Asis', 'Buen Tono 198, Industrial, Gustavo A. Madero, 07800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '1272', '19.4757646', '-99.1259471', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(103, 'Hospital Veterinario Dogtor Pet', 'De las Flores 18 Lt 2 Mz 6, Jardines de Monterrey, 52926 Cd López Mateos, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png', '4.6', ' hospital veterinary_care point_of_interest establishment', '75', '19.5921385', '-99.2272448', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(104, 'Hospital Regional De Especialidades Veterinarias', 'Calle Planta Minatitlan 7, Hab Electra, 54060 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '77', '19.535766', '-99.217448', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(105, 'CLINICA VETERINARIA VALMAR', 'Antigua Calz. de Guadalupe 3, San Marcos, Azcapotzalco, 02020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '207', '19.4893677', '-99.1838769', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(106, 'CONSULTORIO MEDICO VETERINARIO', 'casi esquina calle 12 contra esquina calle 10, Av. Cuitláhuac 221, Pro Hogar, Azcapotzalco, 02900 Ciudad de Méx', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '158', '19.4736043', '-99.154926', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(107, 'Veterinaria Dra. Esther Quiroz', 'Ing. Carlos Daza 230, Guadalupe Insurgentes, Gustavo A. Madero, 07870 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '95', '19.4755379', '-99.1316061', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(108, 'Clínica Veterinaria Gustavo A. Madero', 'Martha 190, Guadalupe Tepeyac, Gustavo A. Madero, 07840 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.8', ' veterinary_care point_of_interest establishment', '270', '19.4698902', '-99.1183429', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(109, 'Clínica Veterinaria La Palma', 'Av. Guadalupe Victoria 77, Guadalupe Victoria I, Gustavo A. Madero, 07790 Gustavo A Madero, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '264', '19.5392324', '-99.1401364', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(110, 'Consultorio Médico Veterinario \"Dr. García\"', 'Guanábana 234 A (Invernadero Esquina, Vid, Nueva Santa María, 02800 CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '42', '19.4668404', '-99.1686304', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(111, 'Panamericana Veterinaria De Mexico Sa', 'Hacienda del Rosario 195, Prados del Rosario, Azcapotzalco, 02410 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '19.5020074', '-99.2104306', 'veterinario', 0, 9, 1, 13, 1, '', 0),
(112, 'Veterinaria Scooby Doo', 'Avenida 6, San Blas Dos, 54870 Cuautitlán, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '39', '19.661478', '-99.1645134', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(113, 'Hospital Integral Veterinario', 'Av Lago de Guadalupe 1008, Bosques de Ixtacala, 52919 Cd López Mateos, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '85', '19.6090447', '-99.2397293', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(114, 'Consultorio y Quirófano Médico Veterinaria \"Nozomi\"', 'Segundo piso, Dinamarca, Centro Urbano, 54750 Cuautitlán Izcalli, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' doctor veterinary_care health point_of_interest establishment', '92', '19.6695077', '-99.2099451', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(115, 'Consultorio Veterinario Alfa-Vet', 'Av. Tianguistengo 28, Cumbria, 54740 Cuautitlán Izcalli, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '77', '19.6530679', '-99.2128191', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(116, 'Clinica Veterinaria Arámbula (2483)', 'Blvd. Tultitlán Pte. 36, Los Reyes, 54900 Tultitlán de Mariano Escobedo, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '44', '19.6404006', '-99.1674323', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(117, 'Clínica Veterinaria Salamandra', 'Sector 62, Av Iztaccíhuatl 3, Unidad Habitacional Infonavit Norte, 54720 Cuautitlán Izcalli, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '115', '19.6732265', '-99.2207707', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(118, 'Veterinaria*Kahun*', 'Av Emiliano Zapata 4, Universidad Autónoma Metropolitana, 52919 Cd López Mateos, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care point_of_interest establishment', '43', '19.6060457', '-99.2476911', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(119, 'Centro Médico Veterinario Lomas del Bosque', 'Calle B. de Pino 6, Lomas del Bosque, 54765 Cuautitlán Izcalli, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '32', '19.6124731', '-99.239994', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(120, 'Centro Veterinario Mundo Mascota', 'Av. De Los Pájaros 32, Granjas Guadalupe, 54474 Villa Nicolás Romero, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '42', '19.6021181', '-99.2808647', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(121, 'Clínica Veterinaria Scooby Doo', 'Calle 27, Lote 2 Mz. 43, San Blas Dos, 54870 Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.9', ' veterinary_care point_of_interest establishment', '72', '19.6614597', '-99.1644953', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(122, 'Médica Veterinaria de Asís', 'Francisco Villa 32, Francisco Sarabia, 54473 Villa Nicolás Romero, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '46', '19.5972665', '-99.2818342', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(123, 'Emo Veterinario', 'Camino a Tepotzotlán Mz. 4 Lt. 2 B, Axotlan, 54715 Cuautitlán Izcalli, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '32', '19.6943252', '-99.2164091', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(124, 'Veterinaria Las Rosas', 'Ignacio Zaragoza 205A, Guadalupe, 54800 Cuautitlán de Romero Rubio, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '44', '19.6706497', '-99.1830547', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(125, 'Clínica Veterinaria Tlaiximatini', 'Valle De Las Alamedas 15, Izcalli del Valle, 54945 Buenavista, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '26', '19.5871966', '-99.183027', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(126, 'Clínica Veterinaria Jaimes', 'General Mz 56 Lt 2, Gral. José Ma. Pino Suárez, 54870 Cuautitlán, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '60', '19.6739915', '-99.1689815', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(127, 'Servicio Veterinario y Estética Pet Land', 'Av. Tlaloc 22, Axotlan, 54719 Cuautitlán Izcalli, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '99', '19.6979004', '-99.2281052', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(128, 'Hospital Veterinario Belén', 'Independencia 22, Benito Juárez, 54942 Buenavista, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' veterinary_care point_of_interest establishment', '140', '19.605488', '-99.1666169', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(129, 'Clínica Veterinaria Mundo Animal', 'Brolevard. Ignacio Zaragoza 131, Campestre Liberacion, 54474 Villa Nicolás Romero, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' veterinary_care point_of_interest establishment', '6', '19.6009067', '-99.2760185', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(130, 'Veterinaria El + Perron', 'Blvd. Ignacio Zaragoza 23, Lomas de San Miguel, 52928 Cd López Mateos, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '13', '19.588896', '-99.2363446', 'veterinario', 0, 10, 1, 13, 1, '', 0),
(131, 'Clínica Veterinaria de Especialidades Acanceh', 'Acanceh 164, Pedregal de San Nicolás 1ra Secc, Tlalpan, 14100 Tlalpan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '91', '19.2893017', '-99.2372082', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(132, 'Centro Veterinario Bosques de Tarango', 'Av Centenario 3012, Bosques de Tarango, Álvaro Obregón, 01580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '83', '19.3506827', '-99.24935', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(133, 'Clínica Aguilas Veterinaria', 'Calz. de las Águilas 1265, Puente Colorado, Álvaro Obregón, 01730 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '9', '19.3487702', '-99.2291334', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(134, 'Centro Veterinario Chamizal', 'Av Stim 105, Lomas del Chamizal, Cuajimalpa de Morelos, 05120 Cuajimalpa, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '24', '19.3963803', '-99.2585293', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(135, 'Clínica Veterinaria Lilas', 'Calle Paseo de Las Lilas 92, Bosques de las Lomas, Cuajimalpa de Morelos, 11910 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.8', ' veterinary_care point_of_interest establishment', '15', '19.3886763', '-99.2478577', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(136, 'Clínica Veterinaria \"Safari\"', 'Carr. a Sta. Rosa 640, Santa Rosa Xochiac, Álvaro Obregón, 01830 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.9', ' veterinary_care point_of_interest establishment', '71', '19.330067', '-99.2871257', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(137, 'Hospital Veterinario Albiter', 'Izamal 306, Entre Calles: Halacho y Tenosique, Héroes de Padierna, Tlalpan, 14200 Ciudad de México, CDMX, Méxic', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' pharmacy veterinary_care health point_of_interest store establishment', '193', '19.2869709', '-99.2226386', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(138, 'Veterinaria Tony', 'Esquina Av. de las Torres, Flor de Liz 1, Torres de Potrero, Álvaro Obregón, 01840 Ciudad de México, CDMX, Méxi', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '36', '19.3316572', '-99.2452472', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(139, 'Clínica Veterinaria Navidad', 'Av. Pastores 150, Granjas Navidad, Cuajimalpa de Morelos, 05210 Cuajimalpa, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '63', '19.3736', '-99.283821', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(140, 'Clinica Veterinaria y Estetica Canina. \"SPA Tu Mascota\"', 'Lic. Castillo Ledón 262, Cuajimalpa, Cuajimalpa de Morelos, 05000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '11', '19.3625318', '-99.2971014', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(141, 'Clínica Integral Veterinaria Santa Lucía', 'Av. Sta. Lucia 215, Olivar del Conde 2da Secc, Álvaro Obregón, 01408 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '72', '19.36965', '-99.219517', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(142, 'Veterinaria Caleb', 'Calle Yucalpeten 1 Mz 356 Ltt 1, Tlalpan, 14200 CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '35', '19.2820495', '-99.2209558', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(143, 'CLINICA VETERINARIA MEDICAN CENTER', 'Plaza Tetelpan, Calz. Dexierto de los Leones 5051 Local 1-A, Álvaro Obregón, Tetelpan, 01700 Ciudad de México, ', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.6', ' veterinary_care point_of_interest establishment', '7', '19.341723', '-99.231275', 'veterinario', 0, 11, 1, 13, 1, '', 0),
(144, 'Clínica Veterinaria Albeitar sucursal Plaza Bugambilias', 'boulevar bugambilias 4460 local B 19, Bugambilias, 45237 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' veterinary_care point_of_interest store establishment', '55', '20.6006882', '-103.4487408', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(145, 'Consorcio Veterinario De Occidente Sa De Cv', 'Aguador 3444, La Calma, 45070 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '11', '20.6335361', '-103.4181716', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(146, 'Veterinaria de Las torres', 'Sta. Esther 252B, Santa Margarita1a Secc., 45140 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '1', '20.7281167', '-103.4137982', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(147, '\"VETERINARIA PASEOS\"', 'Av. Enrique Ladrón de Guevara 3355, Paseos del Sol, 45070 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '146', '20.6351524', '-103.4263964', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(148, 'Clínica Veterinaria San Jose', 'Av Monte Carmelo 461, Hacienda San Jose, 45106 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '48', '20.6012313', '-103.4064551', 'veterinario', 0, 12, 1, 13, 1, '', 0);
INSERT INTO `apimaps` (`id_lugar`, `nombre`, `direccion`, `icono`, `rating`, `types`, `user_ratings_total`, `latitud`, `longitud`, `palabra`, `extra`, `id_municipio`, `id_tipo`, `zoom`, `apimap`, `telefono`, `horario`) VALUES
(149, 'Clínica Veterinaria del Parque', 'cerca de Av Sebastian Bach 5699, La estancia, 45030 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '8', '20.6747685', '-103.4345037', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(150, 'VETERINARIA TARZAN', 'Av. Artes Plásticas Nte. 267, Miravalle, 44970 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '26', '20.6153844', '-103.3473381', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(151, 'Veterinaria Kana', 'Calle Gregorio Dávila 1099, Mezquitan Country, 44260 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '19', '20.6908271', '-103.3608596', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(152, 'Especialidades Veterinarias de Guadalajara', 'Av. Patria 1150, Mirador del Sol, 45054 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '2.8', ' veterinary_care health point_of_interest establishment', '10', '20.654093', '-103.423459', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(153, 'VETERINARIA SANTA ALICIA', 'Prol. Av. Guadalupe 6730-17, Guadalupe Inn, Exitmex, 45037 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '26', '20.6594734', '-103.4453428', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(154, 'Veterinaria VetCare', 'Paulino Navarro 1251B, Los Maestros, 45150 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '19', '20.7234058', '-103.3796856', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(155, 'Clínica Veterinaria La Vaquita', 'Av Venustiano Carranza 938, Constitución, 45180 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '34', '20.72745', '-103.369406', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(156, 'VETERINARIA SANTOS LADRIDOS', 'Barra de Navidad 80, Vallarta Poniente, 44110 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '28', '20.6732187', '-103.3912185', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(157, 'CLINICA VETERINARIA COPERNICO', 'Av. Nicolás Copérnico 4259, Arboledas, 45070 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '50', '20.6321055', '-103.4248795', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(158, 'Centro Veterinario La Cima', 'Arco Valentino 592, Arcos de Zapopan, 45130 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '45', '20.7421316', '-103.409382', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(159, 'Veterinaria Santa Margarita', 'Av Sta Margarita 438-C, La Mora, Los Girasoles, 45136 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '19', '20.7302871', '-103.4220165', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(160, 'Veterinaria Del Real', 'Avenida Obreros de Cananea 525 Dr Simeón De Uría 525-A, Col, Villas Belenes, Los Paraísos, 45157 Zapopan, Jal.,', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '16', '20.7300491', '-103.3822254', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(161, 'Clinica Veterinaria San Gabriel', 'Del Vigía 425, El Vigía, 45140 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '30', '20.7324255', '-103.3932975', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(162, 'Veterinaria California', '9 #191 D, Seattle, 45150 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '7', '20.717601', '-103.371915', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(163, 'CHERSES', 'Misión Santa Julia 6000, Arcos de Guadalupe, 45037 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '35', '20.6590819', '-103.4363282', 'veterinario', 0, 12, 1, 13, 1, '', 0),
(164, 'hospital veterinario y acuario', 'Av Coahuila #202, Coahuila, 67257 Juárez, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '50', '25.6625091', '-100.1424466', 'veterinario', 0, 14, 1, 13, 1, '', 0),
(165, 'Veterinaria Sam', 'Camino a Jardines de La Silla 226, Hacienda de Guadalupe 2o Sector, 67190 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '15', '25.6443663', '-100.1861302', 'veterinario', 0, 14, 1, 13, 1, '', 0),
(166, 'Veterinaria Santa Rosa', 'Av Sta Rosa de Lima 1628, Santa María Sector A, 67190 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '41', '25.6546213', '-100.1939429', 'veterinario', 0, 14, 1, 13, 1, '', 0),
(167, 'VETERINARIA LA MODERNA', 'Calle Pablo A. de La Garza 1523, Moderna, 64530 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest store establishment', '21', '25.6976935', '-100.2866838', 'veterinario', 0, 14, 1, 13, 1, '', 0),
(168, 'Veterinaria santa fe', 'Gral. Plutarco Elías Calles 1144, La Joya, 67164 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '18', '25.6847731', '-100.2041009', 'veterinario', 0, 14, 1, 13, 1, '', 0),
(169, 'Hospital Veterinario Zoo', 'Av. Orquídea 103, Hacienda Las Margaritas 8vo. Sector, 66647 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.9', ' veterinary_care point_of_interest establishment', '24', '25.7080215', '-100.1500656', 'veterinario', 0, 14, 1, 13, 1, '', 0),
(170, 'Servicios Médicos Veterinarios', 'Av. Adolfo López Mateos, Residencial Colibri, Residencial Colibrí, 67189 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' doctor veterinary_care health point_of_interest establishment', '5', '25.6770712', '-100.2153209', 'veterinario', 0, 14, 1, 13, 1, '', 0),
(171, 'veterinaria canino real', '66633, Ignacio Sepúlveda SN_C, La Encarcacion, Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '1', '25.734661', '-100.2032994', 'veterinario', 0, 14, 1, 13, 1, '', 0),
(172, 'Veterinaria Dr Bracho', 'Lombardo Toledano 2973, Alta Vista Sur, 64740 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '21', '25.6404992', '-100.2893657', 'veterinario', 0, 14, 1, 13, 1, '', 0),
(173, 'VETERINARIA CURE PET?S', 'San Agustín #417, San Cristóbal, 67190 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care lodging health point_of_interest establishment', '0', '25.6691673', '-100.1994128', 'veterinario', 0, 14, 1, 13, 1, '', 0),
(174, 'Veterinaria Smart Petz', 'Av Eloy Cavazos #2213, Contry Sol, 67174 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care point_of_interest establishment', '31', '25.6600165', '-100.2566121', 'veterinario', 0, 14, 1, 13, 1, '', 0),
(175, 'Veterinaria My DOG', 'Av. del Teléfono #1003, Alberta Escamilla, 66640 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '31', '25.734637', '-100.173565', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(176, 'Mascotas Hospital Apodaca (Clinica Veterinaria)', 'Av. Concordia #3645, Moderno Apodaca II, 66600 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '24', '25.7736602', '-100.2009275', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(177, 'Veterinaria Medican', 'Dione #808, Arboledas de Santa Rosa, 66600 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '30', '25.816078', '-100.240801', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(178, 'Veterinaria pet healt center', 'Calle Tulipán 220, Los Encinos, 66612 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '42', '25.7921737', '-100.264443', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(179, 'VETERINARIA RODMAN', 'Calle, Av. Rhodesia del Nte. 108?B, Lomas del Pedregal, 66648 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.9', ' veterinary_care point_of_interest establishment', '14', '25.7301508', '-100.1716494', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(180, 'Clínica Veterinaria Albéitar', 'Inspiración 541, Paseo de Las Palmas ÌII, 66635 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' pharmacy veterinary_care health point_of_interest store establishment', '21', '25.7785879', '-100.2648742', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(181, 'Veterinaria Vanit', 'Av Afganistán #119, Prados de La Cieneguita, 66636 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '26', '25.7783927', '-100.2558908', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(182, 'Veterinaria Sauceda', 'Río Orinoco 124, Misión de Huinala, 66646 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '42', '25.7549722', '-100.1619205', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(183, 'Veterinaria Los Ángeles Monterrey', 'Av. Rómulo Garza #437, Los Laureles, 66470 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '33', '25.7204791', '-100.2323147', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(184, 'Dogtor Chaparrin', 'Av. Casa Blanca 602, Rincon de Casa Blanca 2do Sector, 66478 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '33', '25.732921', '-100.2293002', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(185, 'Veterinaria Obregón', 'Nuevo León 1536, Santo Domingo, 66437 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '2', '25.7515731', '-100.260168', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(186, 'Veterinaria de la Fuente', 'Av. Guadalajara 810, Sin Nombre de Col 39, 67113 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '30', '25.7161736', '-100.1924383', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(187, 'Veterinaria San Isidro', 'Río Guadalquivir 623, San Isidro, 66646 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '10', '25.7594676', '-100.1542452', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(188, 'Veterinaria Robles', 'Ave. México #306, Hacienda de San Miguel, 67113 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '10', '25.7225775', '-100.1911123', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(189, 'Veterinaria La Fe 2', 'Blvd. Acapulco 105c, Eduardo Caballero, 67117 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '11', '25.7240636', '-100.1935795', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(190, 'I-Vet Animal Center', 'Av. Calz. Unión 1016a, Parque la Talaverna, 66473 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '4', '25.7165159', '-100.2317313', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(191, 'Servicios Veterinarios Santa Rosa', 'Calle, Río San Lorenzo 328, Balcones de Santa Rosa, 66610 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '26', '25.803365', '-100.229049', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(192, 'Veterinaria Animalia', 'Alondra 300, Enramada V, 66635 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '16', '25.7661823', '-100.2568383', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(193, 'Clinica Veterinaria Rosales', '343, ÌII, Av Rio Nilo, Pueblo Nuevo, 66646 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '12', '25.7608338', '-100.163514', 'veterinario', 0, 15, 1, 13, 1, '', 0),
(194, 'Veterinaria Castillo', 'Pelícano 421, Hacienda Las Palmas, 66635 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care point_of_interest establishment', '60', '25.7712188', '-100.2657681', 'veterinario', 0, 16, 1, 13, 1, '', 0),
(195, 'Econovet.mx Veterinaria', 'Av. Benito Juárez 131, El Canada, 66054 Cd Gral Escobedo, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care health point_of_interest establishment', '45', '25.7814151', '-100.2897584', 'veterinario', 0, 16, 1, 13, 1, '', 0),
(196, 'Acutus Medicina Veterinaria', '1a Avenida, Av. Paseo de Cumbres 545, 64610 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '87', '25.6969498', '-100.3625312', 'veterinario', 0, 16, 1, 13, 1, '', 0),
(197, 'Veterinaria del Rosario', 'Jorge González Camarena # 205, El Roble, 66414 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '98', '25.7640398', '-100.2804358', 'veterinario', 0, 16, 1, 13, 1, '', 0),
(198, 'Veterinaria HVM', 'Paseo de La Amistad 191, Monte Real INFONAVIT, 66057 Cd Gral Escobedo, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' pharmacy veterinary_care health point_of_interest store establishment', '39', '25.799894', '-100.3143606', 'veterinario', 0, 16, 1, 13, 1, '', 0),
(199, 'Veterinaria Doctor Olguín', 'Av Penitenciaria 2311, Simon Bolívar, 64250 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '37', '25.724524', '-100.3404186', 'veterinario', 0, 16, 1, 13, 1, '', 0),
(200, 'Centro Oftalmológico Veterinario', 'Calle Fray Eusebio Kino 249, Villa Universidad, 66420 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' doctor veterinary_care health point_of_interest establishment', '11', '25.7470325', '-100.3027855', 'veterinario', 0, 16, 1, 13, 1, '', 0),
(201, 'Veterinaria Mexico', 'Calle Rep. Mexicana 1100-A, México Lindo, 66410 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' veterinary_care point_of_interest establishment', '21', '25.766012', '-100.272279', 'veterinario', 0, 16, 1, 13, 1, '', 0),
(202, 'Veterinaria Rodriguez', 'Iberia 228, INFONAVIT Topo Grande, 66074 Cd Gral Escobedo, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '34', '25.783747', '-100.3135705', 'veterinario', 0, 16, 1, 13, 1, '', 0),
(203, 'Veterinaria San Nicolas', 'Calle Benito Juarez 305, Centro, 66400 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '33', '25.7544563', '-100.2936189', 'veterinario', 0, 17, 1, 13, 1, '', 0),
(204, 'VETERINARIA', 'Av. Los Fresnos 612, Fresnos II, 66635 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '2', '25.7686573', '-100.2517649', 'veterinario', 0, 17, 1, 13, 1, '', 0),
(205, 'Veterinaria Selvavet', 'Calle Rep. Mexicana #342, El Fundador, 66414 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '7', '25.7581092', '-100.275573', 'veterinario', 0, 17, 1, 13, 1, '', 0),
(206, 'Veterinaria kòokay', 'Gustavo Díaz Ordaz 5064, Croc, 64200 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '78', '25.7760408', '-100.3724742', 'veterinario', 0, 17, 1, 13, 1, '', 0),
(207, 'Veterinaria San Francisco de Asís', 'Av. Benito Juárez 313, Hacienda del Cañada, 66054 Cd Gral Escobedo, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '26', '25.781077', '-100.2868193', 'veterinario', 0, 17, 1, 13, 1, '', 0),
(208, 'Veterinaria Marvet', 'Calle E Sexta 409, Misión de Fundadores, 66612 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '50', '25.7941668', '-100.2485266', 'veterinario', 0, 17, 1, 13, 1, '', 0),
(209, 'Clínica Veterinaria El Gran Danes', 'Prof. Rodolfo González # 101, Solidaridad Soc, 66050 Cd Gral Escobedo, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '21', '25.7821842', '-100.3289839', 'veterinario', 0, 17, 1, 13, 1, '', 0),
(210, 'Clínica Veterinaria Enrique', 'Calle Rep. Mexicana # 522, Valle Del Roble, Residencial Roble 6to Sector, 66414 San Nicolás de los Garza, N.L.,', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '43', '25.7601171', '-100.2743614', 'veterinario', 0, 17, 1, 13, 1, '', 0),
(211, 'Veterinaria Dr Carbajal', 'Calle Luis M. Farias 200, Nuevo Periferico Nte Secc 1, 66423 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '21', '25.752356', '-100.300451', 'veterinario', 0, 17, 1, 13, 1, '', 0),
(212, 'Veterinaria Roacho', 'Massenet 171, Peralvillo, Cuauhtémoc, 06220 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '49', '19.464496', '-99.1395133', 'veterinario', 0, 19, 1, 13, 1, '', 0),
(213, 'Clínica Veterinaria Dr. H. Mondragón', 'Av Mario Colin & Av Toltecas, San Javier, 54030 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '70', '19.5353814', '-99.1898207', 'veterinario', 0, 19, 1, 13, 1, '', 0),
(214, 'Clinica Veterinaria \"Xolo\"', 'Real Pirules 24, Lomas de San Lorenzo, 52975 Cd López Mateos, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '60', '19.5466991', '-99.2254847', 'veterinario', 0, 19, 1, 13, 1, '', 0),
(215, 'Clínica Veterinaria Sagitario', 'Calz. San Esteban 99, Naucalpan, 53489 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '19', '19.4596431', '-99.2322392', 'veterinario', 0, 19, 1, 13, 1, '', 0),
(216, 'Consultorio Veterinario y Estética San Lorenzo.', '28 de Noviembre de 1911 Mz 8 Lt 9, esquina calle Bases Orgánicas, San Lorenzo la Cebada, 16035 Ciudad de México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care health point_of_interest establishment', '41', '19.285838', '-99.1160739', 'veterinario', 0, 20, 1, 13, 1, '', 0),
(217, 'Veterinaria Servicio médico integral', 'Calle 5 de Mayo 59, San Andrés Totoltepec, Tlalpan, 14400 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '55', '19.2527154', '-99.1734492', 'veterinario', 0, 20, 1, 13, 1, '', 0),
(218, 'VETERINARIA AZTECAS', 'Lt 4, Av. Aztecas Mz 105, Ajusco, Coyoacán, 04300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '22', '19.3269679', '-99.156272', 'veterinario', 0, 20, 1, 13, 1, '', 0),
(219, 'Clínica Veterinaria Zoomundo', 'Av. H. Escuela Naval Militar 754, Coapa, Alianza Popular Revolucionaria, Coyoacán, 04800 Ciudad de México, CDMX', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care food point_of_interest store establishment', '7', '19.318488', '-99.123825', 'veterinario', 0, 20, 1, 13, 1, '', 0),
(220, 'Veterinaria Hunter', 'Av Mexico 1620, San Marcos, Xochimilco, 16050 Xochimilco, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '47', '19.2646523', '-99.1133767', 'veterinario', 0, 20, 1, 13, 1, '', 0),
(221, 'Quetzal Servicios Veterinarios 360°', 'Casa 16, Av De Las Torres 66, Miguel Hidalgo 2da Secc, Tlalpan, 14250 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '71', '19.282605', '-99.195313', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(222, 'Clinica Veterinaria KANIS', 'Calle Otomíes 120, Tlalcoligia, Tlalpan, 14430 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '44', '19.274263', '-99.172149', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(223, 'VETERINARIA ZOOCAN', 'Av. Miguel Hidalgo 50, Sta Úrsula Xitla, Tlalpan, 14420 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '104', '19.2804832', '-99.1813806', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(224, 'Veterinaria Trakares', 'Callejón Abasolo 536 Santa María Tepepan, Amp Tepepan, 16020 Ciudad de México, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' pharmacy veterinary_care health point_of_interest store establishment', '70', '19.2705786', '-99.1428414', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(225, 'Clínica veterinaria Kenya', 'Carr. Federal A Cuernavaca 6068-A, San Andrés Totoltepec, Tlalpan, 14400 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '21', '19.256997', '-99.177672', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(226, 'Medyzoo Clínica Veterinaria', 'Calzada, Arenal 610, Santa María Tepepan, Xochimilco, 16020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.8', ' veterinary_care health point_of_interest establishment', '4', '19.2803974', '-99.1377891', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(227, 'Veterinaria La Carreta', 'Cedral 11, San Andrés Totoltepec, Tlalpan, 14640 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '9', '19.266533', '-99.160983', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(228, 'Clínica Veterinaria Maviadsa', 'Joya & Diamante, Valle Escondido, Tlalpan, 14600 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care point_of_interest establishment', '27', '19.279447', '-99.142285', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(229, 'Veterinaria Dr. Eguiluz', 'Entronque Picacho-Ajusco 678 Local C, Tlalpan, Jardines del Ajusco, 14200 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '15', '19.2874161', '-99.2180512', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(230, 'Veterinaria El Caporal', 'Ramón Corona 1, San Miguel Ajusco, Tlalpan, 14710 San Miguel Ajusco, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '7', '19.2200546', '-99.2032237', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(231, 'Forrajería y Veterinaria', '91, Tlalpan, Mehiko, Av. Cruz Blanca, San Miguel Topilejo, Tlalpan, 14500 San Miguel Topilejo, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '2', '19.1997342', '-99.1486803', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(232, 'Estética canina y veterinaria Shampoo', 'La Troje 2, María Esther Zuno de Echeverría, Tlalpan, 14659 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '42', '19.2545373', '-99.1801052', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(233, 'Hospital Veterinario Dr. Guerrero', 'Aljibe 49, Sta Úrsula Xitla, Tlalpan, 14420 Tlalpan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '55', '19.2783378', '-99.1801698', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(234, 'Clínica Veterinaria', 'Pedregal de San Nicolás 2da Secc, 14100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '5', '19.2881673', '-99.2367505', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(235, 'Veterinaria Fabela', 'Av. Tetiz 257, Pedregal de San Nicolás 3ra Secc, Tlalpan, 14100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '117', '19.2852164', '-99.2327971', 'veterinario', 0, 21, 1, 13, 1, '', 0),
(236, 'VETERINARIA SAMMY', 'Calle Hacienda de Tala 2459A, Colonia Oblatos, 44700 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '2', '20.692606', '-103.290861', 'veterinario', 0, 22, 1, 13, 1, '', 0),
(237, 'Clínica Veterinaria Mascovet', 'Calle Juan de Dios Robledo 756, Blanco y Cuéllar, 44730 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '93', '20.681646', '-103.310348', 'veterinario', 0, 22, 1, 13, 1, '', 0),
(238, 'Servicios Veterinarios', 'Calle 5 de Febrero 2859, Rancho Blanco, 45560 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '24', '20.6364945', '-103.3186807', 'veterinario', 0, 22, 1, 13, 1, '', 0),
(239, 'Clinica Veterinaria Medical Vet.', 'C,, San Pedro 4583, Villas de Guadalupe, Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' veterinary_care point_of_interest establishment', '50', '20.7502366', '-103.3444837', 'veterinario', 0, 22, 1, 13, 1, '', 0),
(240, 'VETERINARIA MASCORAMA', 'esq. Valle de los Duraznos, Calz Federalistas 2374, Jardines del Valle, 45138 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '66', '20.7484814', '-103.4285119', 'veterinario', 0, 22, 1, 13, 1, '', 0),
(241, 'Veterinaria TlaquePet', 'Calle Rep. de Argentina 46, Colonial Tlaquepaque, 45570 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '45', '20.6321019', '-103.3101704', 'veterinario', 0, 22, 1, 13, 1, '', 0),
(242, 'VETERINARIA Garras', 'Av Antonio Diaz Soto y Gama 702, Rancho Nuevo, 44240 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '62', '20.7189223', '-103.3248601', 'veterinario', 0, 22, 1, 13, 1, '', 0),
(243, 'VETERINARIA CASTILLO', 'Av. Miguel López de Legaspi 1897, Jardines de La Cruz, 44950 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' hospital veterinary_care point_of_interest establishment', '17', '20.6390694', '-103.3787582', 'veterinario', 0, 22, 1, 13, 1, '', 0),
(244, 'CLINICA VETERINARIA SANTA ELENA', 'Av Fidel Velázquez Sánchez 957, Santa Elena Alcalde, 44220 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care food point_of_interest store establishment', '61', '20.7119639', '-103.345255', 'veterinario', 0, 22, 1, 13, 1, '', 0),
(245, 'DISTRIBUIDORA EL ARCA DE NOE', 'Av Mariano Otero 2152, Col Americana, Moderna, 44140 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' veterinary_care point_of_interest establishment', '30', '20.6656003', '-103.3721427', 'veterinario', 0, 22, 1, 13, 1, '', 0),
(246, 'Veterinaria VIMUR', 'Calle 5 de Febrero 2758, Rancho Blanco, 45560 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '60', '20.6381614', '-103.3187861', 'veterinario', 0, 22, 1, 13, 1, '', 0),
(247, 'Dog Wash Estética Canina', 'Av. del Estudiante 240-A, Matamoros, 58139 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '3', '19.6992969', '-101.1669707', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(248, 'Estética Canina Kokibob', 'Calle Gaspar de Villadiego 458, Cepamisa, 58199 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest store establishment', '16', '19.696792', '-101.2165157', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(249, 'Estetica Canina Morelia', 'Calle Alberto Alvarado 645, Villa Universidad, 58060 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '8', '19.6867438', '-101.199153', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(250, 'Accesorios y Estética Canina Luna', 'Paseos de los Sauces 474, Int. 1, Prados Verdes, 58110 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest store establishment', '22', '19.7187236', '-101.2021036', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(251, 'Estetica Canina Especializada', 'Thomas Alva Edison 376, Electricistas, 58290 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '5', '19.6850386', '-101.1769768', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(252, 'Clinica Veterinaria Estetica Canina', 'Calle Vicente Sta. María No. 2089 A, Félix Ireta, 58070 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.8', ' veterinary_care point_of_interest establishment', '5', '19.6828566', '-101.1881496', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(253, 'Estetica Canina Especializada', 'Perif. Paseo de la República 2267, Camelinas, 58290 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '19.6823571', '-101.1706502', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(254, 'Veterinaria y Estética Canina Animalitos', 'Av. Michoacán s/n, La Colina, Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '3', '19.7092084', '-101.2124232', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(255, 'Estetica Canina La Estrella Moreliana', 'Jalisco #789, Isaac Arriaga, 58210 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '19.7125016', '-101.1716835', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(256, 'Pupet - Estética Canina', 'Cornl. Felipe Santiago Xicoténcatl 114, Nueva Chapultepec, 58260 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' point_of_interest establishment', '1', '19.6944945', '-101.1730206', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(257, 'Estética Canina', 'Calle Purépecha 316, Lomas de Santiaguito, Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '19.7232418', '-101.1780225', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(258, 'Clínica Veterinaria y Estetica Canina San Francisco', 'Calle Juan de Dios Peza 169, Tenencia Sta Maria de Guido, 58090 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '16', '19.673402', '-101.1887778', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(259, 'Estetica Canina Y Accesorios Baldo', 'Calle Manuela Medina #418 c, Margarita Maza de Juárez, 58150 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '19.6931327', '-101.2535556', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(260, 'Estética y Boutique Canina', 'Plaza Fiesta Camelinas, Av. Ventura Puente 1843, Local H-18, Electricistas, 58290 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '1', ' veterinary_care point_of_interest store establishment', '1', '19.6823917', '-101.1794712', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(261, 'Estetica Canina', 'Dr Miguel Silva González, 58110 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '19.7194078', '-101.1884288', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(262, 'Estetica Canina', 'Villas del Real, 58116 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' point_of_interest establishment', '3', '19.7319184', '-101.1995411', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(263, 'Estetica Canina Y Accesorios', 'Margarita Maza de Juárez, 58337 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4', ' grocery_or_supermarket food point_of_interest store establishment', '1', '19.6931371', '-101.2535405', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(264, 'Hospital Estética Veterinaria Pupet', 'Jose Maria, Calle Tte. Coronel José Ma. Olvera 50, Nueva Chapultepec, 58260 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' veterinary_care point_of_interest establishment', '41', '19.6882472', '-101.1665767', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(265, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(266, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(267, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(268, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(269, 'ESTETICA CANINA GINARI', 'Torres Adalid 1960, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.3883157', '-99.1489664', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(270, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(271, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(272, 'estetica canina', 'Campesinos 58, Granjas Esmeralda, Iztapalapa, 09810 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '3', '19.3538885', '-99.1197793', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(273, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(274, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(275, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(276, 'Estética Canina', 'Xola 1603, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '19.3953551', '-99.1508691', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(277, 'Estética Canina Peek', 'Benito Juárez 99 Local B, Los Reyes Culhuacán, 09840 Iztapalapa, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' point_of_interest establishment', '4', '19.3461767', '-99.1028036', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(278, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(279, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(280, 'Estetica Canina Romeo', 'A UNA CUADRA DE METRO BUS DR. CERCA METRO ETIOPIA, Calle Monte Albán 25, Vértiz Narvarte, Benito Juárez, 03020 ', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '10', '19.3985148', '-99.1501756', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(281, 'Estética canina \"Hachiko\"', 'Privada de, Av. Carlota Armero # 162, Culhuacan CTM VII, Coyoacán, 04480 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.324653', '-99.114666', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(282, 'Estetica Canina RIK YORK', 'Calle Tres Guerras 21, Centro, Cuauhtémoc, 06040 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '2', '19.4297735', '-99.1509652', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(283, 'Estética Canina', 'Lic. Irineo Paz 3, San Juan, Benito Juárez, 03730 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '6', '19.377491', '-99.1827478', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(284, 'Estética Canina Y Felina - Ramón Ricardo', '06700, Medellín 205, Roma Nte., Cuauhtémoc, Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4112799', '-99.1645055', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(285, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(286, 'ESTETICA CANINA ALMENDRA', 'Avenida caporal andador 5 duplex 6 casa 2, VILLA COAPA, 14390 MEXICO DISTRITO FEDERAL, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '11', '19.290308', '-99.128405', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(287, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(288, 'MetePet Estética Canina', 'Paseo San Isidro 1127, Barrio de Santiaguito, 52140 Metepec, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '16', '19.2529379', '-99.5933877', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(289, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(290, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(291, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(292, 'Manchas Estética Canina', 'Avenida Moreda 201, El Roble, 42185 Fraccionamientos del Sur, Hgo., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '15', '20.0572402', '-98.758868', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(293, 'Estética Canina', 'Av Rodrigo Gómez 6050, Valle del Topo Chico, 64259 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' point_of_interest establishment', '4', '25.7299819', '-100.3428523', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(294, 'Caniche Estética Canina', 'Real de Cumbres 825, Real Cumbres, 64346 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' point_of_interest establishment', '13', '25.7523844', '-100.4030353', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(295, 'Malawi Estetica canina', 'Av. Anillo Periferico 1201, Colinas de San Jerónimo, 64630 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '4', '25.6867653', '-100.375209', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(296, 'Estetica Canina Guau Wash', 'Blvd. Acapulco, Misión de San Miguel, 66648 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '10', '25.717117', '-100.17594', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(297, 'Estética Canina', 'Santa María, 25049 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.41133', '-100.9533314', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(298, 'VETERINARIA ESTETICA CANINA Happy Dog', 'Av. José Alvarado 2008, Residencial La Española, 64820 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' veterinary_care point_of_interest establishment', '7', '25.6592588', '-100.2741918', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(299, 'ESTÉTICA CANINA y FORRAJERA PERROS & GATOS', 'Blvd. Solidaridad 564, Ignacio Zaragoza, 25156 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.4273471', '-100.933154', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(300, 'Estetica Canina Servicio A Domicilio \"PIOLIN\"', 'Calle Rafael Platón Sánchez 1118, Treviño, 64570 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' veterinary_care point_of_interest establishment', '1', '25.6842272', '-100.3017114', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(301, 'ESTÉTICA CANINA', '21 de Marzo 1219, Francisco I Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.6846096', '-100.281349', 'estetica+canina', 0, 13, 2, 13, 1, '', 0);
INSERT INTO `apimaps` (`id_lugar`, `nombre`, `direccion`, `icono`, `rating`, `types`, `user_ratings_total`, `latitud`, `longitud`, `palabra`, `extra`, `id_municipio`, `id_tipo`, `zoom`, `apimap`, `telefono`, `horario`) VALUES
(302, 'Clínica Veterinaria y Estética Canina', 'Blvrd Morelos 302, Morelos, 25017 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '60', '25.4275321', '-100.9521371', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(303, 'MASCOTAS Y ESTETICA CANINA', 'Calle E Sexta s/n, Metroplex I, 66612 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.6', ' veterinary_care point_of_interest establishment', '5', '25.7861213', '-100.2489783', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(304, 'ESTÉTICA CANINA', 'Brasil 197, Villa Olímpica, 67180 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.1', ' veterinary_care point_of_interest establishment', '7', '25.6530451', '-100.2088103', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(305, 'Veterinaria Las Quintas', 'Av. Adolfo López Mateos 403, Residencial Las Quintas, 67167 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '27', '25.6916868', '-100.2125701', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(306, 'Estética Canina en Monterrey', 'Anillo Periférico 500, Jardines del Mezquital, 66445 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.7496067', '-100.2429674', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(307, 'Estética Canina Happy Dog', 'Libertad 1403, Nuevo Repueblo, 64700 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '1', '25.6565035', '-100.3038722', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(308, 'Clinica Veterinaria Dr. Mauricio Roque en Monterrey', 'Republica Mexicana 800, Riberas de las Puentes, 66465 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '7', '25.737504', '-100.286272', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(309, 'Acutus Clínica y Estética Canina', 'Calle Gobernadores 404-4, Burócratas del Estado, 64380 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '4', '25.7088503', '-100.3661012', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(310, 'ESTETICA CANINA Little Can', 'Av 519 28, San Juan de Aragón I Secc, Gustavo A. Madero, 07969 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '28', '19.454127', '-99.09388', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(311, 'Estetica Canina Best Friend', 'Mark Twain 4852, Jardines de la Patria, 45110 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '20', '20.6832646', '-103.4177981', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(312, 'Estética Canina Mi Mejor Amigo', 'Calle Gaspar Bolaños 757, Jardines Alcalde, 44298 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '28', '20.706922', '-103.3416524', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(313, 'Estética Canina Dival', 'Canario 1107, 8 de Julio, 44910 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' point_of_interest establishment', '14', '20.6493526', '-103.3586221', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(314, 'Estetica Canina Isa', 'Calle Constitución 119A, Tonalá Centro, 45400 Tonalá, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '21', '20.626655', '-103.243176', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(315, 'Estética canina profesional Kovo', 'Onza 3954, Lagos de Oriente, 44770 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' point_of_interest establishment', '10', '20.6688595', '-103.2757939', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(316, 'Estética Canina', 'De Las Torres 847, Santa Margarita1a Secc., 45140 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '20.7297059', '-103.4124114', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(317, 'Happy Tails Estetica Canina', 'Av Conchita 3768, Loma Bonita, 45086 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '17', '20.6386196', '-103.405114', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(318, 'Estética Canina', '5 de Febrero 2859, Rancho Blanco, 45560 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest store establishment', '5', '20.6328426', '-103.3185623', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(319, 'ESTETICA CANINA', 'Av. Francisco Javier Mina 3107, Agustín Yáñez (La Florida), 44790 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6635065', '-103.293983', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(320, 'ESTETICA CANINA', 'Plomo, esquina Calle Paseo de las Palmas 183, Arenales Tapatíos, 45066 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '2', '20.6351257', '-103.4683462', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(321, 'Estetica Canina Peter', 'Diamante 2724, Arenales Tapatíos, 45066 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6354378', '-103.4559005', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(322, 'Estética Canina Memo', 'Coya 621, Providencia, 44670 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '20.7019816', '-103.378224', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(323, 'Estética Canina Herrera', 'Deportes 252, El Campesino, 45590 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.59883', '-103.334233', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(324, 'Ohana pets estetica canina', 'Av. Miguel Angel 271, La Estancia, 45030 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '20.6708535', '-103.4254495', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(325, 'Estética Canina Peter', 'Calle López Portillo 26A, El Briseño, 45236 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.6236649', '-103.4349434', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(326, 'Barber Dog Estetica canina', '45300, La Capilla, 45300 Tala, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' doctor health point_of_interest establishment', '1', '20.6521995', '-103.6971012', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(327, 'Estética Canina Kelly', 'Calle José María Narváez 1194, San Isidro, 44740 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '9', '20.6808527', '-103.2979021', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(328, 'Estética Canina', 'Calle María C. Bancalari, esquina con Heriberto M. Aja 3093, Echeverría, 44970 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.6238465', '-103.3622976', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(329, 'Estética Canina Cardenal', 'Calle Cardenal 5241, Las Juntas, 45590 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6049569', '-103.3382877', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(330, 'Estética Canina PetClan', 'Calle Francisco de Ayza 1295, Progreso, 44730 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest store establishment', '10', '20.6713626', '-103.3072671', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(331, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(332, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(333, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(334, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(335, 'ESTETICA CANINA GINARI', 'Torres Adalid 1960, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.3883157', '-99.1489664', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(336, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(337, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(338, 'ESTETICA CANINA Little Can', 'Av 519 28, San Juan de Aragón I Secc, Gustavo A. Madero, 07969 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '28', '19.454127', '-99.09388', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(339, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(340, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(341, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(342, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(343, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(344, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(345, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(346, 'Estética Canina Peek', 'Benito Juárez 99 Local B, Los Reyes Culhuacán, 09840 Iztapalapa, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' point_of_interest establishment', '4', '19.3461767', '-99.1028036', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(347, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(348, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(349, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(350, 'Manchas Estética Canina', 'Avenida Moreda 201, El Roble, 42185 Fraccionamientos del Sur, Hgo., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '15', '20.0572402', '-98.758868', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(351, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(352, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(353, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(354, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(355, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(356, 'ESTETICA CANINA GINARI', 'Torres Adalid 1960, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.3883157', '-99.1489664', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(357, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(358, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(359, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(360, 'ESTETICA CANINA Little Can', 'Av 519 28, San Juan de Aragón I Secc, Gustavo A. Madero, 07969 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '28', '19.454127', '-99.09388', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(361, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(362, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(363, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(364, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(365, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(366, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(367, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(368, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(369, 'Manchas Estética Canina', 'Avenida Moreda 201, El Roble, 42185 Fraccionamientos del Sur, Hgo., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '15', '20.0572402', '-98.758868', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(370, 'Estetica Canina RIK YORK', 'Calle Tres Guerras 21, Centro, Cuauhtémoc, 06040 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '2', '19.4297735', '-99.1509652', 'estetica+canina', 0, 1, 2, 13, 1, '', 0),
(371, 'Estética Canina Kokibob', 'Calle Gaspar de Villadiego 458, Cepamisa, 58199 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest store establishment', '16', '19.696792', '-101.2165157', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(372, 'Dog Wash Estética Canina', 'Av. del Estudiante 240-A, Matamoros, 58139 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '3', '19.6992969', '-101.1669707', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(373, 'Estetica Canina Morelia', 'Calle Alberto Alvarado 645, Villa Universidad, 58060 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '8', '19.6867438', '-101.199153', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(374, 'Estetica Canina Especializada', 'Thomas Alva Edison 376, Electricistas, 58290 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care point_of_interest establishment', '5', '19.6850386', '-101.1769768', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(375, 'Accesorios y Estética Canina Luna', 'Paseos de los Sauces 474, Int. 1, Prados Verdes, 58110 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest store establishment', '22', '19.7187236', '-101.2021036', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(376, 'Clinica Veterinaria Estetica Canina', 'Calle Vicente Sta. María No. 2089 A, Félix Ireta, 58070 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.8', ' veterinary_care point_of_interest establishment', '5', '19.6828566', '-101.1881496', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(377, 'Clinica Veterinaria Y Estetica Canina \"Dr. Vet\"', 'Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '7', '19.6987755', '-101.250309', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(378, 'Estetica Canina Especializada', 'Perif. Paseo de la República 2267, Camelinas, 58290 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '19.6823571', '-101.1706502', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(379, 'Veterinaria y Estética Canina Animalitos', 'Av. Michoacán s/n, La Colina, Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '3', '19.7092084', '-101.2124232', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(380, 'Estetica Canina La Estrella Moreliana', 'Jalisco #789, Isaac Arriaga, 58210 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '19.7125016', '-101.1716835', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(381, 'Pupet - Estética Canina', 'Cornl. Felipe Santiago Xicoténcatl 114, Nueva Chapultepec, 58260 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' point_of_interest establishment', '1', '19.6944945', '-101.1730206', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(382, 'Estética Canina', 'Calle Purépecha 316, Lomas de Santiaguito, Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '19.7232418', '-101.1780225', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(383, 'Clínica Veterinaria y Estetica Canina San Francisco', 'Calle Juan de Dios Peza 169, Tenencia Sta Maria de Guido, 58090 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' veterinary_care point_of_interest establishment', '16', '19.673402', '-101.1887778', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(384, 'Estetica Canina Y Accesorios Baldo', 'Calle Manuela Medina #418 c, Margarita Maza de Juárez, 58150 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '19.6931327', '-101.2535556', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(385, 'Estética y Boutique Canina', 'Plaza Fiesta Camelinas, Av. Ventura Puente 1843, Local H-18, Electricistas, 58290 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '1', ' veterinary_care point_of_interest store establishment', '1', '19.6823917', '-101.1794712', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(386, 'Estetica Canina', 'Dr Miguel Silva González, 58110 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '19.7194078', '-101.1884288', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(387, 'Estetica Canina', 'Villas del Real, 58116 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' point_of_interest establishment', '3', '19.7319184', '-101.1995411', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(388, 'Spa Majarani / Estética Canina Profesional', 'Morelos #112 Rancho, La Cruz, 38640 Celaya, Gto., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '18', '20.4597094', '-100.8034635', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(389, 'Estética Canina Profesional \"Los Gemelos\"', 'General Lázaro Cárdenas 222, Centro, 61650 Tacámbaro de Codallos, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '5', ' pet_store point_of_interest store establishment', '4', '19.2344841', '-101.4565383', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(390, 'Estetica Canina Y Accesorios', 'Margarita Maza de Juárez, 58337 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4', ' grocery_or_supermarket food point_of_interest store establishment', '1', '19.6931371', '-101.2535405', 'estetica+canina', 0, 2, 2, 13, 1, '', 0),
(391, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(392, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(393, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(394, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(395, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(396, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(397, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(398, 'ESTETICA CANINA ALMENDRA', 'Avenida caporal andador 5 duplex 6 casa 2, VILLA COAPA, 14390 MEXICO DISTRITO FEDERAL, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '11', '19.290308', '-99.128405', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(399, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(400, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(401, 'MetePet Estética Canina', 'Paseo San Isidro 1127, Barrio de Santiaguito, 52140 Metepec, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '16', '19.2529379', '-99.5933877', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(402, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(403, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(404, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(405, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(406, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(407, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(408, 'Estética canina \"Hachiko\"', 'Privada de, Av. Carlota Armero # 162, Culhuacan CTM VII, Coyoacán, 04480 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.324653', '-99.114666', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(409, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(410, 'Manchas Estética Canina', 'Avenida Moreda 201, El Roble, 42185 Fraccionamientos del Sur, Hgo., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '15', '20.0572402', '-98.758868', 'estetica+canina', 0, 3, 2, 13, 1, '', 0),
(411, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(412, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(413, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(414, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(415, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(416, 'ESTETICA CANINA ALMENDRA', 'Avenida caporal andador 5 duplex 6 casa 2, VILLA COAPA, 14390 MEXICO DISTRITO FEDERAL, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '11', '19.290308', '-99.128405', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(417, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(418, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(419, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(420, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(421, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(422, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(423, 'MetePet Estética Canina', 'Paseo San Isidro 1127, Barrio de Santiaguito, 52140 Metepec, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '16', '19.2529379', '-99.5933877', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(424, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(425, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(426, 'Estética Canina Peek', 'Benito Juárez 99 Local B, Los Reyes Culhuacán, 09840 Iztapalapa, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' point_of_interest establishment', '4', '19.3461767', '-99.1028036', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(427, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(428, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(429, 'Estética canina \"Hachiko\"', 'Privada de, Av. Carlota Armero # 162, Culhuacan CTM VII, Coyoacán, 04480 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.324653', '-99.114666', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(430, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 4, 2, 13, 1, '', 0),
(431, 'Estética Canina', 'Av Rodrigo Gómez 6050, Valle del Topo Chico, 64259 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' point_of_interest establishment', '4', '25.7299819', '-100.3428523', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(432, 'Caniche Estética Canina', 'Real de Cumbres 825, Real Cumbres, 64346 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' point_of_interest establishment', '13', '25.7523844', '-100.4030353', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(433, 'Malawi Estetica canina', 'Av. Anillo Periferico 1201, Colinas de San Jerónimo, 64630 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '4', '25.6867653', '-100.375209', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(434, 'Estetica Canina Guau Wash', 'Blvd. Acapulco, Misión de San Miguel, 66648 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '10', '25.717117', '-100.17594', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(435, 'Estética Canina', 'Santa María, 25049 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.41133', '-100.9533314', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(436, 'VETERINARIA ESTETICA CANINA Happy Dog', 'Av. José Alvarado 2008, Residencial La Española, 64820 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' veterinary_care point_of_interest establishment', '7', '25.6592588', '-100.2741918', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(437, 'ESTÉTICA CANINA y FORRAJERA PERROS & GATOS', 'Blvd. Solidaridad 564, Ignacio Zaragoza, 25156 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.4273471', '-100.933154', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(438, 'Estetica Canina Servicio A Domicilio \"PIOLIN\"', 'Calle Rafael Platón Sánchez 1118, Treviño, 64570 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' veterinary_care point_of_interest establishment', '1', '25.6842272', '-100.3017114', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(439, 'ESTÉTICA CANINA', '21 de Marzo 1219, Francisco I Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.6846096', '-100.281349', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(440, 'Clínica Veterinaria y Estética Canina', 'Blvrd Morelos 302, Morelos, 25017 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '60', '25.4275321', '-100.9521371', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(441, 'orejitas y colitas Clinica Veterinaria y Estética Canina', 'Calle Antonio I. Villarreal 2112, Nueva Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '12', '25.6930534', '-100.277738', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(442, 'MASCOTAS Y ESTETICA CANINA', 'Calle E Sexta s/n, Metroplex I, 66612 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.6', ' veterinary_care point_of_interest establishment', '5', '25.7861213', '-100.2489783', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(443, 'ESTÉTICA CANINA', 'Brasil 197, Villa Olímpica, 67180 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.1', ' veterinary_care point_of_interest establishment', '7', '25.6530451', '-100.2088103', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(444, 'Veterinaria Dog - Go Hospital Veterinario, Estética Canina', 'Paseo de Las Americas Pte. 2457, Contry La Silla 4o Sector, 67173 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '22', '25.6320562', '-100.2695196', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(445, 'Veterinaria Las Quintas', 'Av. Adolfo López Mateos 403, Residencial Las Quintas, 67167 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '27', '25.6916868', '-100.2125701', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(446, 'Estética Canina en Monterrey', 'Anillo Periférico 500, Jardines del Mezquital, 66445 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.7496067', '-100.2429674', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(447, 'Estética Canina Happy Dog', 'Libertad 1403, Nuevo Repueblo, 64700 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '1', '25.6565035', '-100.3038722', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(448, 'Clinica Veterinaria Dr. Mauricio Roque en Monterrey', 'Republica Mexicana 800, Riberas de las Puentes, 66465 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '7', '25.737504', '-100.286272', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(449, 'Acutus Clínica y Estética Canina', 'Calle Gobernadores 404-4, Burócratas del Estado, 64380 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '4', '25.7088503', '-100.3661012', 'estetica+canina', 0, 13, 2, 13, 1, '', 0),
(450, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(451, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(452, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(453, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(454, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(455, 'ESTETICA CANINA GINARI', 'Torres Adalid 1960, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.3883157', '-99.1489664', 'estetica+canina', 0, 6, 2, 13, 1, '', 0);
INSERT INTO `apimaps` (`id_lugar`, `nombre`, `direccion`, `icono`, `rating`, `types`, `user_ratings_total`, `latitud`, `longitud`, `palabra`, `extra`, `id_municipio`, `id_tipo`, `zoom`, `apimap`, `telefono`, `horario`) VALUES
(456, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(457, 'ESTETICA CANINA Little Can', 'Av 519 28, San Juan de Aragón I Secc, Gustavo A. Madero, 07969 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '28', '19.454127', '-99.09388', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(458, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(459, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(460, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(461, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(462, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(463, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(464, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(465, 'Estética Canina Peek', 'Benito Juárez 99 Local B, Los Reyes Culhuacán, 09840 Iztapalapa, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' point_of_interest establishment', '4', '19.3461767', '-99.1028036', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(466, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(467, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(468, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(469, 'Manchas Estética Canina', 'Avenida Moreda 201, El Roble, 42185 Fraccionamientos del Sur, Hgo., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '15', '20.0572402', '-98.758868', 'estetica+canina', 0, 6, 2, 13, 1, '', 0),
(470, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(471, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(472, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(473, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(474, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(475, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(476, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(477, 'ESTETICA CANINA ALMENDRA', 'Avenida caporal andador 5 duplex 6 casa 2, VILLA COAPA, 14390 MEXICO DISTRITO FEDERAL, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '11', '19.290308', '-99.128405', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(478, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(479, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(480, 'MetePet Estética Canina', 'Paseo San Isidro 1127, Barrio de Santiaguito, 52140 Metepec, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '16', '19.2529379', '-99.5933877', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(481, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(482, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(483, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(484, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(485, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(486, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(487, 'Estética canina \"Hachiko\"', 'Privada de, Av. Carlota Armero # 162, Culhuacan CTM VII, Coyoacán, 04480 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.324653', '-99.114666', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(488, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(489, 'Manchas Estética Canina', 'Avenida Moreda 201, El Roble, 42185 Fraccionamientos del Sur, Hgo., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '15', '20.0572402', '-98.758868', 'estetica+canina', 0, 7, 2, 13, 1, '', 0),
(490, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(491, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(492, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(493, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(494, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(495, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(496, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(497, 'ESTETICA CANINA ALMENDRA', 'Avenida caporal andador 5 duplex 6 casa 2, VILLA COAPA, 14390 MEXICO DISTRITO FEDERAL, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '11', '19.290308', '-99.128405', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(498, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(499, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(500, 'MetePet Estética Canina', 'Paseo San Isidro 1127, Barrio de Santiaguito, 52140 Metepec, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '16', '19.2529379', '-99.5933877', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(501, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(502, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(503, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(504, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(505, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(506, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(507, 'Estética canina \"Hachiko\"', 'Privada de, Av. Carlota Armero # 162, Culhuacan CTM VII, Coyoacán, 04480 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.324653', '-99.114666', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(508, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(509, 'Manchas Estética Canina', 'Avenida Moreda 201, El Roble, 42185 Fraccionamientos del Sur, Hgo., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '15', '20.0572402', '-98.758868', 'estetica+canina', 0, 8, 2, 13, 1, '', 0),
(510, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(511, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(512, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(513, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(514, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(515, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(516, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(517, 'ESTETICA CANINA ALMENDRA', 'Avenida caporal andador 5 duplex 6 casa 2, VILLA COAPA, 14390 MEXICO DISTRITO FEDERAL, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '11', '19.290308', '-99.128405', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(518, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(519, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(520, 'MetePet Estética Canina', 'Paseo San Isidro 1127, Barrio de Santiaguito, 52140 Metepec, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '16', '19.2529379', '-99.5933877', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(521, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(522, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(523, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(524, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(525, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(526, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(527, 'Estética canina \"Hachiko\"', 'Privada de, Av. Carlota Armero # 162, Culhuacan CTM VII, Coyoacán, 04480 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.324653', '-99.114666', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(528, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(529, 'Manchas Estética Canina', 'Avenida Moreda 201, El Roble, 42185 Fraccionamientos del Sur, Hgo., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '15', '20.0572402', '-98.758868', 'estetica+canina', 0, 9, 2, 13, 1, '', 0),
(530, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(531, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(532, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(533, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(534, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(535, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(536, 'ESTETICA CANINA GINARI', 'Torres Adalid 1960, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.3883157', '-99.1489664', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(537, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(538, 'ESTETICA CANINA ALMENDRA', 'Avenida caporal andador 5 duplex 6 casa 2, VILLA COAPA, 14390 MEXICO DISTRITO FEDERAL, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '11', '19.290308', '-99.128405', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(539, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(540, 'ESTETICA CANINA Little Can', 'Av 519 28, San Juan de Aragón I Secc, Gustavo A. Madero, 07969 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '28', '19.454127', '-99.09388', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(541, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(542, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(543, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(544, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(545, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(546, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(547, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(548, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(549, 'Manchas Estética Canina', 'Avenida Moreda 201, El Roble, 42185 Fraccionamientos del Sur, Hgo., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '15', '20.0572402', '-98.758868', 'estetica+canina', 0, 10, 2, 13, 1, '', 0),
(550, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(551, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(552, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(553, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(554, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(555, 'ESTETICA CANINA ALMENDRA', 'Avenida caporal andador 5 duplex 6 casa 2, VILLA COAPA, 14390 MEXICO DISTRITO FEDERAL, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '11', '19.290308', '-99.128405', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(556, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(557, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(558, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(559, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(560, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(561, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(562, 'MetePet Estética Canina', 'Paseo San Isidro 1127, Barrio de Santiaguito, 52140 Metepec, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '16', '19.2529379', '-99.5933877', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(563, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(564, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(565, 'Estética Canina Peek', 'Benito Juárez 99 Local B, Los Reyes Culhuacán, 09840 Iztapalapa, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' point_of_interest establishment', '4', '19.3461767', '-99.1028036', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(566, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(567, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(568, 'Estética canina \"Hachiko\"', 'Privada de, Av. Carlota Armero # 162, Culhuacan CTM VII, Coyoacán, 04480 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.324653', '-99.114666', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(569, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 11, 2, 13, 1, '', 0),
(570, 'Estetica Canina Best Friend', 'Mark Twain 4852, Jardines de la Patria, 45110 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '20', '20.6832646', '-103.4177981', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(571, 'Estética Canina Mi Mejor Amigo', 'Calle Gaspar Bolaños 757, Jardines Alcalde, 44298 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '28', '20.706922', '-103.3416524', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(572, 'Estética Canina Dival', 'Canario 1107, 8 de Julio, 44910 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' point_of_interest establishment', '14', '20.6493526', '-103.3586221', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(573, 'Estetica Canina Isa', 'Calle Constitución 119A, Tonalá Centro, 45400 Tonalá, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '21', '20.626655', '-103.243176', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(574, 'Estética canina profesional Kovo', 'Onza 3954, Lagos de Oriente, 44770 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' point_of_interest establishment', '10', '20.6688595', '-103.2757939', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(575, 'Estética Canina', 'De Las Torres 847, Santa Margarita1a Secc., 45140 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '20.7297059', '-103.4124114', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(576, 'Happy Tails Estetica Canina', 'Av Conchita 3768, Loma Bonita, 45086 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '17', '20.6386196', '-103.405114', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(577, 'Estética Canina', '5 de Febrero 2859, Rancho Blanco, 45560 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest store establishment', '5', '20.6328426', '-103.3185623', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(578, 'ESTETICA CANINA', 'Av. Francisco Javier Mina 3107, Agustín Yáñez (La Florida), 44790 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6635065', '-103.293983', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(579, 'ESTETICA CANINA', 'Plomo, esquina Calle Paseo de las Palmas 183, Arenales Tapatíos, 45066 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '2', '20.6351257', '-103.4683462', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(580, 'Estetica Canina Peter', 'Diamante 2724, Arenales Tapatíos, 45066 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6354378', '-103.4559005', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(581, 'Estética Canina Memo', 'Coya 621, Providencia, 44670 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '20.7019816', '-103.378224', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(582, 'Estética Canina Herrera', 'Deportes 252, El Campesino, 45590 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.59883', '-103.334233', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(583, 'Ohana pets estetica canina', 'Av. Miguel Angel 271, La Estancia, 45030 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '20.6708535', '-103.4254495', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(584, 'Estética Canina Peter', 'Calle López Portillo 26A, El Briseño, 45236 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.6236649', '-103.4349434', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(585, 'Barber Dog Estetica canina', '45300, La Capilla, 45300 Tala, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' doctor health point_of_interest establishment', '1', '20.6521995', '-103.6971012', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(586, 'Estética Canina Kelly', 'Calle José María Narváez 1194, San Isidro, 44740 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '9', '20.6808527', '-103.2979021', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(587, 'Estética Canina', 'Calle María C. Bancalari, esquina con Heriberto M. Aja 3093, Echeverría, 44970 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.6238465', '-103.3622976', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(588, 'Estética Canina Cardenal', 'Calle Cardenal 5241, Las Juntas, 45590 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6049569', '-103.3382877', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(589, 'Estética Canina PetClan', 'Calle Francisco de Ayza 1295, Progreso, 44730 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest store establishment', '10', '20.6713626', '-103.3072671', 'estetica+canina', 0, 12, 2, 13, 1, '', 0),
(590, 'Estética Canina', 'Av Rodrigo Gómez 6050, Valle del Topo Chico, 64259 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' point_of_interest establishment', '4', '25.7299819', '-100.3428523', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(591, 'Caniche Estética Canina', 'Real de Cumbres 825, Real Cumbres, 64346 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' point_of_interest establishment', '13', '25.7523844', '-100.4030353', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(592, 'Malawi Estetica canina', 'Av. Anillo Periferico 1201, Colinas de San Jerónimo, 64630 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '4', '25.6867653', '-100.375209', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(593, 'Estetica Canina Guau Wash', 'Blvd. Acapulco, Misión de San Miguel, 66648 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '10', '25.717117', '-100.17594', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(594, 'Estética Canina', 'Santa María, 25049 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.41133', '-100.9533314', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(595, 'VETERINARIA ESTETICA CANINA Happy Dog', 'Av. José Alvarado 2008, Residencial La Española, 64820 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' veterinary_care point_of_interest establishment', '7', '25.6592588', '-100.2741918', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(596, 'ESTÉTICA CANINA y FORRAJERA PERROS & GATOS', 'Blvd. Solidaridad 564, Ignacio Zaragoza, 25156 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.4273471', '-100.933154', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(597, 'Estetica Canina Servicio A Domicilio \"PIOLIN\"', 'Calle Rafael Platón Sánchez 1118, Treviño, 64570 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' veterinary_care point_of_interest establishment', '1', '25.6842272', '-100.3017114', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(598, 'ESTÉTICA CANINA', '21 de Marzo 1219, Francisco I Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.6846096', '-100.281349', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(599, 'Clínica Veterinaria y Estética Canina', 'Blvrd Morelos 302, Morelos, 25017 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '60', '25.4275321', '-100.9521371', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(600, 'orejitas y colitas Clinica Veterinaria y Estética Canina', 'Calle Antonio I. Villarreal 2112, Nueva Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '12', '25.6930534', '-100.277738', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(601, 'MASCOTAS Y ESTETICA CANINA', 'Calle E Sexta s/n, Metroplex I, 66612 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.6', ' veterinary_care point_of_interest establishment', '5', '25.7861213', '-100.2489783', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(602, 'ESTÉTICA CANINA', 'Brasil 197, Villa Olímpica, 67180 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.1', ' veterinary_care point_of_interest establishment', '7', '25.6530451', '-100.2088103', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(603, 'Veterinaria Dog - Go Hospital Veterinario, Estética Canina', 'Paseo de Las Americas Pte. 2457, Contry La Silla 4o Sector, 67173 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '22', '25.6320562', '-100.2695196', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(604, 'Veterinaria Las Quintas', 'Av. Adolfo López Mateos 403, Residencial Las Quintas, 67167 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '27', '25.6916868', '-100.2125701', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(605, 'Estética Canina en Monterrey', 'Anillo Periférico 500, Jardines del Mezquital, 66445 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.7496067', '-100.2429674', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(606, 'Estética Canina Happy Dog', 'Libertad 1403, Nuevo Repueblo, 64700 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '1', '25.6565035', '-100.3038722', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(607, 'Clinica Veterinaria Dr. Mauricio Roque en Monterrey', 'Republica Mexicana 800, Riberas de las Puentes, 66465 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '7', '25.737504', '-100.286272', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(608, 'Acutus Clínica y Estética Canina', 'Calle Gobernadores 404-4, Burócratas del Estado, 64380 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '4', '25.7088503', '-100.3661012', 'estetica+canina', 0, 14, 2, 13, 1, '', 0),
(609, 'Estética Canina', 'Av Rodrigo Gómez 6050, Valle del Topo Chico, 64259 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' point_of_interest establishment', '4', '25.7299819', '-100.3428523', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(610, 'Caniche Estética Canina', 'Real de Cumbres 825, Real Cumbres, 64346 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' point_of_interest establishment', '13', '25.7523844', '-100.4030353', 'estetica+canina', 0, 15, 2, 13, 1, '', 0);
INSERT INTO `apimaps` (`id_lugar`, `nombre`, `direccion`, `icono`, `rating`, `types`, `user_ratings_total`, `latitud`, `longitud`, `palabra`, `extra`, `id_municipio`, `id_tipo`, `zoom`, `apimap`, `telefono`, `horario`) VALUES
(611, 'Malawi Estetica canina', 'Av. Anillo Periferico 1201, Colinas de San Jerónimo, 64630 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '4', '25.6867653', '-100.375209', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(612, 'Estetica Canina Guau Wash', 'Blvd. Acapulco, Misión de San Miguel, 66648 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '10', '25.717117', '-100.17594', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(613, 'Estética Canina', 'Santa María, 25049 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.41133', '-100.9533314', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(614, 'VETERINARIA ESTETICA CANINA Happy Dog', 'Av. José Alvarado 2008, Residencial La Española, 64820 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' veterinary_care point_of_interest establishment', '7', '25.6592588', '-100.2741918', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(615, 'ESTÉTICA CANINA y FORRAJERA PERROS & GATOS', 'Blvd. Solidaridad 564, Ignacio Zaragoza, 25156 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.4273471', '-100.933154', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(616, 'Estetica Canina Servicio A Domicilio \"PIOLIN\"', 'Calle Rafael Platón Sánchez 1118, Treviño, 64570 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' veterinary_care point_of_interest establishment', '1', '25.6842272', '-100.3017114', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(617, 'ESTÉTICA CANINA', '21 de Marzo 1219, Francisco I Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.6846096', '-100.281349', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(618, 'Clínica Veterinaria y Estética Canina', 'Blvrd Morelos 302, Morelos, 25017 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '60', '25.4275321', '-100.9521371', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(619, 'orejitas y colitas Clinica Veterinaria y Estética Canina', 'Calle Antonio I. Villarreal 2112, Nueva Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '12', '25.6930534', '-100.277738', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(620, 'MASCOTAS Y ESTETICA CANINA', 'Calle E Sexta s/n, Metroplex I, 66612 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.6', ' veterinary_care point_of_interest establishment', '5', '25.7861213', '-100.2489783', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(621, 'ESTÉTICA CANINA', 'Brasil 197, Villa Olímpica, 67180 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.1', ' veterinary_care point_of_interest establishment', '7', '25.6530451', '-100.2088103', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(622, 'Veterinaria Dog - Go Hospital Veterinario, Estética Canina', 'Paseo de Las Americas Pte. 2457, Contry La Silla 4o Sector, 67173 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '22', '25.6320562', '-100.2695196', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(623, 'Veterinaria Las Quintas', 'Av. Adolfo López Mateos 403, Residencial Las Quintas, 67167 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '27', '25.6916868', '-100.2125701', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(624, 'Estética Canina en Monterrey', 'Anillo Periférico 500, Jardines del Mezquital, 66445 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.7496067', '-100.2429674', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(625, 'Estética Canina Happy Dog', 'Libertad 1403, Nuevo Repueblo, 64700 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '1', '25.6565035', '-100.3038722', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(626, 'Clinica Veterinaria Dr. Mauricio Roque en Monterrey', 'Republica Mexicana 800, Riberas de las Puentes, 66465 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '7', '25.737504', '-100.286272', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(627, 'Acutus Clínica y Estética Canina', 'Calle Gobernadores 404-4, Burócratas del Estado, 64380 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '4', '25.7088503', '-100.3661012', 'estetica+canina', 0, 15, 2, 13, 1, '', 0),
(628, 'Estética Canina', 'Av Rodrigo Gómez 6050, Valle del Topo Chico, 64259 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' point_of_interest establishment', '4', '25.7299819', '-100.3428523', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(629, 'Caniche Estética Canina', 'Real de Cumbres 825, Real Cumbres, 64346 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' point_of_interest establishment', '13', '25.7523844', '-100.4030353', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(630, 'Malawi Estetica canina', 'Av. Anillo Periferico 1201, Colinas de San Jerónimo, 64630 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '4', '25.6867653', '-100.375209', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(631, 'Estetica Canina Guau Wash', 'Blvd. Acapulco, Misión de San Miguel, 66648 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '10', '25.717117', '-100.17594', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(632, 'Estética Canina', 'Santa María, 25049 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.41133', '-100.9533314', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(633, 'VETERINARIA ESTETICA CANINA Happy Dog', 'Av. José Alvarado 2008, Residencial La Española, 64820 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' veterinary_care point_of_interest establishment', '7', '25.6592588', '-100.2741918', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(634, 'ESTÉTICA CANINA y FORRAJERA PERROS & GATOS', 'Blvd. Solidaridad 564, Ignacio Zaragoza, 25156 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.4273471', '-100.933154', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(635, 'Estetica Canina Servicio A Domicilio \"PIOLIN\"', 'Calle Rafael Platón Sánchez 1118, Treviño, 64570 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' veterinary_care point_of_interest establishment', '1', '25.6842272', '-100.3017114', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(636, 'ESTÉTICA CANINA', '21 de Marzo 1219, Francisco I Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.6846096', '-100.281349', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(637, 'Clínica Veterinaria y Estética Canina', 'Blvrd Morelos 302, Morelos, 25017 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '60', '25.4275321', '-100.9521371', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(638, 'orejitas y colitas Clinica Veterinaria y Estética Canina', 'Calle Antonio I. Villarreal 2112, Nueva Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '12', '25.6930534', '-100.277738', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(639, 'MASCOTAS Y ESTETICA CANINA', 'Calle E Sexta s/n, Metroplex I, 66612 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.6', ' veterinary_care point_of_interest establishment', '5', '25.7861213', '-100.2489783', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(640, 'ESTÉTICA CANINA', 'Brasil 197, Villa Olímpica, 67180 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.1', ' veterinary_care point_of_interest establishment', '7', '25.6530451', '-100.2088103', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(641, 'Veterinaria Dog - Go Hospital Veterinario, Estética Canina', 'Paseo de Las Americas Pte. 2457, Contry La Silla 4o Sector, 67173 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '22', '25.6320562', '-100.2695196', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(642, 'Veterinaria Las Quintas', 'Av. Adolfo López Mateos 403, Residencial Las Quintas, 67167 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '27', '25.6916868', '-100.2125701', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(643, 'Estética Canina en Monterrey', 'Anillo Periférico 500, Jardines del Mezquital, 66445 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.7496067', '-100.2429674', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(644, 'Estética Canina Happy Dog', 'Libertad 1403, Nuevo Repueblo, 64700 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '1', '25.6565035', '-100.3038722', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(645, 'Clinica Veterinaria Dr. Mauricio Roque en Monterrey', 'Republica Mexicana 800, Riberas de las Puentes, 66465 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '7', '25.737504', '-100.286272', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(646, 'Acutus Clínica y Estética Canina', 'Calle Gobernadores 404-4, Burócratas del Estado, 64380 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '4', '25.7088503', '-100.3661012', 'estetica+canina', 0, 16, 2, 13, 1, '', 0),
(647, 'Estética Canina', 'Av Rodrigo Gómez 6050, Valle del Topo Chico, 64259 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' point_of_interest establishment', '4', '25.7299819', '-100.3428523', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(648, 'Caniche Estética Canina', 'Real de Cumbres 825, Real Cumbres, 64346 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' point_of_interest establishment', '13', '25.7523844', '-100.4030353', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(649, 'Malawi Estetica canina', 'Av. Anillo Periferico 1201, Colinas de San Jerónimo, 64630 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '4', '25.6867653', '-100.375209', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(650, 'Estetica Canina Guau Wash', 'Blvd. Acapulco, Misión de San Miguel, 66648 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' veterinary_care point_of_interest establishment', '10', '25.717117', '-100.17594', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(651, 'Estética Canina', 'Santa María, 25049 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.41133', '-100.9533314', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(652, 'VETERINARIA ESTETICA CANINA Happy Dog', 'Av. José Alvarado 2008, Residencial La Española, 64820 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' veterinary_care point_of_interest establishment', '7', '25.6592588', '-100.2741918', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(653, 'ESTÉTICA CANINA y FORRAJERA PERROS & GATOS', 'Blvd. Solidaridad 564, Ignacio Zaragoza, 25156 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.4273471', '-100.933154', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(654, 'Estetica Canina Servicio A Domicilio \"PIOLIN\"', 'Calle Rafael Platón Sánchez 1118, Treviño, 64570 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' veterinary_care point_of_interest establishment', '1', '25.6842272', '-100.3017114', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(655, 'ESTÉTICA CANINA', '21 de Marzo 1219, Francisco I Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '25.6846096', '-100.281349', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(656, 'Clínica Veterinaria y Estética Canina', 'Blvrd Morelos 302, Morelos, 25017 Saltillo, Coah., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' veterinary_care point_of_interest establishment', '60', '25.4275321', '-100.9521371', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(657, 'orejitas y colitas Clinica Veterinaria y Estética Canina', 'Calle Antonio I. Villarreal 2112, Nueva Madero, 64560 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '12', '25.6930534', '-100.277738', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(658, 'MASCOTAS Y ESTETICA CANINA', 'Calle E Sexta s/n, Metroplex I, 66612 Cd Apodaca, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.6', ' veterinary_care point_of_interest establishment', '5', '25.7861213', '-100.2489783', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(659, 'ESTÉTICA CANINA', 'Brasil 197, Villa Olímpica, 67180 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.1', ' veterinary_care point_of_interest establishment', '7', '25.6530451', '-100.2088103', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(660, 'Veterinaria Dog - Go Hospital Veterinario, Estética Canina', 'Paseo de Las Americas Pte. 2457, Contry La Silla 4o Sector, 67173 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.7', ' veterinary_care point_of_interest establishment', '22', '25.6320562', '-100.2695196', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(661, 'Veterinaria Las Quintas', 'Av. Adolfo López Mateos 403, Residencial Las Quintas, 67167 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' veterinary_care point_of_interest establishment', '27', '25.6916868', '-100.2125701', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(662, 'Estética Canina en Monterrey', 'Anillo Periférico 500, Jardines del Mezquital, 66445 San Nicolás de los Garza, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '25.7496067', '-100.2429674', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(663, 'Estética Canina Happy Dog', 'Libertad 1403, Nuevo Repueblo, 64700 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '1', '25.6565035', '-100.3038722', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(664, 'Clinica Veterinaria Dr. Mauricio Roque en Monterrey', 'Republica Mexicana 800, Riberas de las Puentes, 66465 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' veterinary_care point_of_interest establishment', '7', '25.737504', '-100.286272', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(665, 'Acutus Clínica y Estética Canina', 'Calle Gobernadores 404-4, Burócratas del Estado, 64380 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' veterinary_care point_of_interest establishment', '4', '25.7088503', '-100.3661012', 'estetica+canina', 0, 17, 2, 13, 1, '', 0),
(666, 'Estetica Canina Best Friend', 'Mark Twain 4852, Jardines de la Patria, 45110 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '20', '20.6832646', '-103.4177981', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(667, 'Estética Canina Mi Mejor Amigo', 'Calle Gaspar Bolaños 757, Jardines Alcalde, 44298 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '28', '20.706922', '-103.3416524', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(668, 'Estética Canina Dival', 'Canario 1107, 8 de Julio, 44910 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' point_of_interest establishment', '14', '20.6493526', '-103.3586221', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(669, 'Estetica Canina Isa', 'Calle Constitución 119A, Tonalá Centro, 45400 Tonalá, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '21', '20.626655', '-103.243176', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(670, 'Estética canina profesional Kovo', 'Onza 3954, Lagos de Oriente, 44770 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' point_of_interest establishment', '10', '20.6688595', '-103.2757939', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(671, 'Estética Canina', 'De Las Torres 847, Santa Margarita1a Secc., 45140 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '20.7297059', '-103.4124114', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(672, 'Happy Tails Estetica Canina', 'Av Conchita 3768, Loma Bonita, 45086 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '17', '20.6386196', '-103.405114', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(673, 'Estética Canina', '5 de Febrero 2859, Rancho Blanco, 45560 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest store establishment', '5', '20.6328426', '-103.3185623', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(674, 'ESTETICA CANINA', 'Av. Francisco Javier Mina 3107, Agustín Yáñez (La Florida), 44790 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6635065', '-103.293983', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(675, 'ESTETICA CANINA', 'Plomo, esquina Calle Paseo de las Palmas 183, Arenales Tapatíos, 45066 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '2', '20.6351257', '-103.4683462', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(676, 'Estetica Canina Peter', 'Diamante 2724, Arenales Tapatíos, 45066 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6354378', '-103.4559005', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(677, 'Estética Canina Memo', 'Coya 621, Providencia, 44670 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '20.7019816', '-103.378224', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(678, 'Estética Canina Herrera', 'Deportes 252, El Campesino, 45590 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.59883', '-103.334233', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(679, 'Ohana pets estetica canina', 'Av. Miguel Angel 271, La Estancia, 45030 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '20.6708535', '-103.4254495', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(680, 'Estética Canina Peter', 'Calle López Portillo 26A, El Briseño, 45236 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.6236649', '-103.4349434', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(681, 'Barber Dog Estetica canina', '45300, La Capilla, 45300 Tala, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' doctor health point_of_interest establishment', '1', '20.6521995', '-103.6971012', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(682, 'Estética Canina Kelly', 'Calle José María Narváez 1194, San Isidro, 44740 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '9', '20.6808527', '-103.2979021', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(683, 'Estética Canina', 'Calle María C. Bancalari, esquina con Heriberto M. Aja 3093, Echeverría, 44970 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.6238465', '-103.3622976', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(684, 'Estética Canina Cardenal', 'Calle Cardenal 5241, Las Juntas, 45590 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6049569', '-103.3382877', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(685, 'Estética Canina PetClan', 'Calle Francisco de Ayza 1295, Progreso, 44730 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest store establishment', '10', '20.6713626', '-103.3072671', 'estetica+canina', 0, 18, 2, 13, 1, '', 0),
(686, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(687, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(688, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(689, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(690, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(691, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(692, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(693, 'ESTETICA CANINA ALMENDRA', 'Avenida caporal andador 5 duplex 6 casa 2, VILLA COAPA, 14390 MEXICO DISTRITO FEDERAL, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '11', '19.290308', '-99.128405', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(694, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(695, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(696, 'MetePet Estética Canina', 'Paseo San Isidro 1127, Barrio de Santiaguito, 52140 Metepec, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '16', '19.2529379', '-99.5933877', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(697, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(698, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(699, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(700, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(701, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(702, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(703, 'Estética canina \"Hachiko\"', 'Privada de, Av. Carlota Armero # 162, Culhuacan CTM VII, Coyoacán, 04480 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.324653', '-99.114666', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(704, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(705, 'Manchas Estética Canina', 'Avenida Moreda 201, El Roble, 42185 Fraccionamientos del Sur, Hgo., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '15', '20.0572402', '-98.758868', 'estetica+canina', 0, 19, 2, 13, 1, '', 0),
(706, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(707, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(708, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(709, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(710, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(711, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(712, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(713, 'ESTETICA CANINA ALMENDRA', 'Avenida caporal andador 5 duplex 6 casa 2, VILLA COAPA, 14390 MEXICO DISTRITO FEDERAL, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '11', '19.290308', '-99.128405', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(714, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(715, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(716, 'MetePet Estética Canina', 'Paseo San Isidro 1127, Barrio de Santiaguito, 52140 Metepec, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '16', '19.2529379', '-99.5933877', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(717, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(718, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(719, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(720, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(721, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(722, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(723, 'Estética canina \"Hachiko\"', 'Privada de, Av. Carlota Armero # 162, Culhuacan CTM VII, Coyoacán, 04480 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.324653', '-99.114666', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(724, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(725, 'Estetica Canina RIK YORK', 'Calle Tres Guerras 21, Centro, Cuauhtémoc, 06040 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '2', '19.4297735', '-99.1509652', 'estetica+canina', 0, 20, 2, 13, 1, '', 0),
(726, 'Estetica Canina A Domicilio PetCare Cdmx edo mex Atizapan', 'Calle Gral. Gómez Pedraza 3, San Miguel Chapultepec II Secc, 11850 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.3', ' point_of_interest establishment', '59', '19.413444', '-99.181414', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(727, 'Estetica Canina Burbujitas', 'Luna 200, Buenavista, Cuauhtémoc, 06350 Buenavista, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '10', '19.4490006', '-99.1472107', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(728, 'Estetica Canina a Domicilio MOVIL CDMX zona metropolitana', 'Av. Benjamín Franklin 48, Escandón I Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.4', ' pet_store veterinary_care point_of_interest store establishment', '90', '19.4068093', '-99.1819285', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(729, 'Estética Canina Wowpísimo', 'Av, Paseo de los Sauces No. 12, U.H. Valle del Tenayo, 54147 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '24', '19.5444886', '-99.1715544', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(730, 'Estética canina', 'Calle Joaquín Velázquez de León 24, San Rafael, Cuauhtémoc, 06470 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.4416032', '-99.165115', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(731, 'ESTETICA CANINA ALMENDRA', 'Avenida caporal andador 5 duplex 6 casa 2, VILLA COAPA, 14390 MEXICO DISTRITO FEDERAL, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '11', '19.290308', '-99.128405', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(732, 'Estética Canina López Hermanos', 'Camino a San Mateo Nopala 7, Valle de San Mateo, 53240 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' veterinary_care food point_of_interest store establishment', '16', '19.4911367', '-99.2510578', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(733, 'Estética Canina Ginari', 'Av. de las Americas 116, Moderna, Benito Juárez, 03510 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.5', ' veterinary_care point_of_interest establishment', '4', '19.394016', '-99.1347262', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(734, 'Estetica Canina Profesional', 'Calle 39, Ignacio Zaragoza, Venustiano Carranza, 15000 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3.7', ' point_of_interest establishment', '3', '19.4115727', '-99.0915841', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(735, 'ESTÉTICA CANINA YOKO', 'Zempoala 68-A, Narvarte Poniente, Benito Juárez, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '19', '19.3989637', '-99.147881', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(736, 'Estética Canina \"The Pelos\"', 'Calle Guerrero 365, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '25', '19.4552311', '-99.1450921', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(737, 'Estética Canina Portales', 'Necaxa 140, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '20', '19.3716429', '-99.1486773', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(738, 'MetePet Estética Canina', 'Paseo San Isidro 1127, Barrio de Santiaguito, 52140 Metepec, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '16', '19.2529379', '-99.5933877', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(739, 'Estética Canina', 'Parroquia 329, Golondrinas 1ra Amp, Benito Juárez, 03100 Benito Juarez, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '19.3703693', '-99.175239', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(740, 'Estética Canina Dogerts', 'Calle Andalucía 87, San Rafael, Azcapotzalco, 02010 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '6', '19.4860782', '-99.1884559', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(741, 'Estética Canina Peek', 'Benito Juárez 99 Local B, Los Reyes Culhuacán, 09840 Iztapalapa, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4', ' point_of_interest establishment', '4', '19.3461767', '-99.1028036', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(742, 'Estética Canina Pepelos', 'Av. del Taller 765, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '19.410724', '-99.09982', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(743, 'PETISSA Estética Canina', 'Puerto Topolobampo 91, Casas Alemán, Gustavo A. Madero, 07580 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '4', '19.4779487', '-99.0849998', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(744, 'Estética canina \"Hachiko\"', 'Privada de, Av. Carlota Armero # 162, Culhuacan CTM VII, Coyoacán, 04480 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '19.324653', '-99.114666', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(745, 'Estética Canina The Doogs', 'Tepetlapa 2041, Coapa, Coyoacán, 04918 Coyoacan, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.1', ' point_of_interest establishment', '7', '19.3153444', '-99.1093574', 'estetica+canina', 0, 21, 2, 13, 1, '', 0),
(746, 'Estetica Canina Best Friend', 'Mark Twain 4852, Jardines de la Patria, 45110 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.5', ' point_of_interest establishment', '20', '20.6832646', '-103.4177981', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(747, 'Estética Canina Mi Mejor Amigo', 'Calle Gaspar Bolaños 757, Jardines Alcalde, 44298 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.8', ' point_of_interest establishment', '28', '20.706922', '-103.3416524', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(748, 'Estética Canina Dival', 'Canario 1107, 8 de Julio, 44910 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.9', ' point_of_interest establishment', '14', '20.6493526', '-103.3586221', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(749, 'Estetica Canina Isa', 'Calle Constitución 119A, Tonalá Centro, 45400 Tonalá, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.3', ' point_of_interest establishment', '21', '20.626655', '-103.243176', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(750, 'Estética canina profesional Kovo', 'Onza 3954, Lagos de Oriente, 44770 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.6', ' point_of_interest establishment', '10', '20.6688595', '-103.2757939', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(751, 'Estética Canina', 'De Las Torres 847, Santa Margarita1a Secc., 45140 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '20.7297059', '-103.4124114', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(752, 'Happy Tails Estetica Canina', 'Av Conchita 3768, Loma Bonita, 45086 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest establishment', '17', '20.6386196', '-103.405114', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(753, 'Estética Canina', '5 de Febrero 2859, Rancho Blanco, 45560 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.4', ' point_of_interest store establishment', '5', '20.6328426', '-103.3185623', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(754, 'ESTETICA CANINA', 'Av. Francisco Javier Mina 3107, Agustín Yáñez (La Florida), 44790 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6635065', '-103.293983', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(755, 'ESTETICA CANINA', 'Plomo, esquina Calle Paseo de las Palmas 183, Arenales Tapatíos, 45066 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '2', '20.6351257', '-103.4683462', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(756, 'Estetica Canina Peter', 'Diamante 2724, Arenales Tapatíos, 45066 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6354378', '-103.4559005', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(757, 'Estética Canina Memo', 'Coya 621, Providencia, 44670 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '3', ' point_of_interest establishment', '1', '20.7019816', '-103.378224', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(758, 'Estética Canina Herrera', 'Deportes 252, El Campesino, 45590 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.59883', '-103.334233', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(759, 'Ohana pets estetica canina', 'Av. Miguel Angel 271, La Estancia, 45030 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '2', '20.6708535', '-103.4254495', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(760, 'Estética Canina Peter', 'Calle López Portillo 26A, El Briseño, 45236 Zapopan, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.6236649', '-103.4349434', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(761, 'Barber Dog Estetica canina', '45300, La Capilla, 45300 Tala, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' doctor health point_of_interest establishment', '1', '20.6521995', '-103.6971012', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(762, 'Estética Canina Kelly', 'Calle José María Narváez 1194, San Isidro, 44740 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '4.2', ' point_of_interest establishment', '9', '20.6808527', '-103.2979021', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(763, 'Estética Canina', 'Calle María C. Bancalari, esquina con Heriberto M. Aja 3093, Echeverría, 44970 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' point_of_interest establishment', '0', '20.6238465', '-103.3622976', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(764, 'Estética Canina Cardenal', 'Calle Cardenal 5241, Las Juntas, 45590 San Pedro Tlaquepaque, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest establishment', '1', '20.6049569', '-103.3382877', 'estetica+canina', 0, 22, 2, 13, 1, '', 0),
(765, 'Estética Canina PetClan', 'Calle Francisco de Ayza 1295, Progreso, 44730 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' point_of_interest store establishment', '10', '20.6713626', '-103.3072671', 'estetica+canina', 0, 22, 2, 13, 1, '', 0);
INSERT INTO `apimaps` (`id_lugar`, `nombre`, `direccion`, `icono`, `rating`, `types`, `user_ratings_total`, `latitud`, `longitud`, `palabra`, `extra`, `id_municipio`, `id_tipo`, `zoom`, `apimap`, `telefono`, `horario`) VALUES
(766, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(767, 'Host a Pet | Cuidadores de perros | Consentirán a tu perrito en su casa como tu lo harías', 'Avenida Mazatlan 152A, Hipódromo Condesa, Cuauhtémoc, 06170 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '47', '19.4107956', '-99.1788314', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(768, 'Dog and Roll', '03303, Tokio 319, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.6', ' restaurant food point_of_interest establishment', '201', '19.370093', '-99.152531', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(769, 'The Rusty Cafe Gourmet', 'Eje 5 Sur 65, Américas Unidas, Benito Juárez, 03610 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png', '3.7', ' bakery cafe lodging food point_of_interest store establishment', '20', '19.3833684', '-99.1424897', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(770, 'Pet Central', 'Av México 49, Colonia Condesa, Cuauhtémoc, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '3.2', ' pet_store point_of_interest store establishment', '99', '19.4129697', '-99.1701036', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(771, 'Santa Pizza México', 'Calle Guillermo Gonzalez Camarena 11, 3, Santa Fe, Zedec Sta Fé, Álvaro Obregón, 01210 Ciudad de México, CDMX, ', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '171', '19.367851', '-99.2622048', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(772, 'MILO´S', 'Amsterdam, Calle Celaya &, Hipódromo Condesa, Cuauhtémoc, 06100 CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.3', ' restaurant food point_of_interest establishment', '946', '19.4143627', '-99.166915', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(773, 'CAN ITO', 'Santa María La Ribera 102, Sta María la Ribera, Cuauhtémoc, 06400 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.4', ' point_of_interest store establishment', '55', '19.4463111', '-99.157556', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(774, 'Friendly Dogs', '5 de ignacio zaragoza 21-B, Rtno. 3 Ignacio Zaragoza, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de Méx', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '19.425206', '-99.109071', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(775, 'Smart Dogs', 'Parroquia 412, Col del Valle Sur, Benito Juárez, 03100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.6', ' point_of_interest store establishment', '290', '19.3704201', '-99.1741405', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(776, 'Pinche Gringo BBQ', 'Cumbres de Maltrata 360, Narvarte Oriente, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '4812', '19.3931215', '-99.1520934', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(777, 'El Ocho', 'Av México 111, Hipódromo, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png', '4.3', ' cafe restaurant food point_of_interest establishment', '3555', '19.4102994', '-99.1700098', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(778, 'EL RINCON DE LOS PERRIJOS', 'Melchor Ocampo 306, planta baja, Cuauhtemoc, Cuauhtémoc, 06500 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '5', ' pet_store veterinary_care health point_of_interest store establishment', '8', '19.4318673', '-99.1720003', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(779, 'Estación Mascota', 'Calle 312 89, Nueva Atzacoalco, Gustavo A. Madero, 07420 Ciudad de México, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.6', ' pharmacy veterinary_care lodging health point_of_interest store establishment', '69', '19.4985332', '-99.0850871', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(780, 'Bocca Buona Viva La Pizza De Verdad', 'San Lorenzo 153, Tlacoquemecatl del Valle, Benito Juárez, 03200 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '603', '19.3750925', '-99.1762743', 'pet+friendly', 0, 1, 3, 13, 1, '', 0),
(781, 'Confort Iinn', 'Exhacienda de Cantabria 40, Residencial San José de la Huerta, 58197 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '3.3', ' lodging point_of_interest establishment', '3', '19.6802038', '-101.2255547', 'pet+friendly', 0, 2, 3, 13, 1, '', 0),
(782, 'Mexican Loft', 'Calle de Eduardo Ruiz 750, Centro histórico de Morelia, 58000 Morelia, Mich., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '4.8', ' lodging point_of_interest establishment', '8', '19.7056844', '-101.1992798', 'pet+friendly', 0, 2, 3, 13, 1, '', 0),
(783, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(784, 'Dog and Roll', '03303, Tokio 319, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.6', ' restaurant food point_of_interest establishment', '201', '19.370093', '-99.152531', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(785, 'Host a Pet | Cuidadores de perros | Consentirán a tu perrito en su casa como tu lo harías', 'Avenida Mazatlan 152A, Hipódromo Condesa, Cuauhtémoc, 06170 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '47', '19.4107956', '-99.1788314', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(786, 'Santa Pizza México', 'Calle Guillermo Gonzalez Camarena 11, 3, Santa Fe, Zedec Sta Fé, Álvaro Obregón, 01210 Ciudad de México, CDMX, ', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '171', '19.367851', '-99.2622048', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(787, 'The Rusty Cafe Gourmet', 'Eje 5 Sur 65, Américas Unidas, Benito Juárez, 03610 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png', '3.7', ' bakery cafe lodging food point_of_interest store establishment', '20', '19.3833684', '-99.1424897', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(788, 'Central Pets', 'Av. División del Nte. No. 2953, El Rosedal, Coyoacán, 04330 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '4.7', ' lodging point_of_interest establishment', '37', '19.3419068', '-99.1487863', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(789, 'El Ocho', 'Av México 111, Hipódromo, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png', '4.3', ' cafe restaurant food point_of_interest establishment', '3555', '19.4102994', '-99.1700098', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(790, 'El Parnita', 'Av. Yucatan 84, Roma Nte., Cuauhtémoc, 06700 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.5', ' restaurant food point_of_interest establishment', '1459', '19.4140759', '-99.1626234', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(791, 'MILO´S', 'Amsterdam, Calle Celaya &, Hipódromo Condesa, Cuauhtémoc, 06100 CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.3', ' restaurant food point_of_interest establishment', '946', '19.4143627', '-99.166915', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(792, 'CAN ITO', 'Santa María La Ribera 102, Sta María la Ribera, Cuauhtémoc, 06400 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.4', ' point_of_interest store establishment', '55', '19.4463111', '-99.157556', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(793, 'La Esquina de los Milagros', 'Parque Centenario 18, Coyoacán TNT, Coyoacán, 04000 Coyoacán, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '892', '19.3489342', '-99.1642235', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(794, 'Mister Lukas. Pasteles para perros', 'Avenida Miguel Hidalgo 47 Int 02 Alcaldía, Del Carmen, 04100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.6', ' bakery food point_of_interest store establishment', '22', '19.351331', '-99.1530313', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(795, 'Friendly Dogs', '5 de ignacio zaragoza 21-B, Rtno. 3 Ignacio Zaragoza, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de Méx', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '19.425206', '-99.109071', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(796, 'Smart Dogs', 'Parroquia 412, Col del Valle Sur, Benito Juárez, 03100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.6', ' point_of_interest store establishment', '290', '19.3704201', '-99.1741405', 'pet+friendly', 0, 3, 3, 13, 1, '', 0),
(797, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(798, 'Santa Pizza México', 'Calle Guillermo Gonzalez Camarena 11, 3, Santa Fe, Zedec Sta Fé, Álvaro Obregón, 01210 Ciudad de México, CDMX, ', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '171', '19.367851', '-99.2622048', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(799, 'Host a Pet | Cuidadores de perros | Consentirán a tu perrito en su casa como tu lo harías', 'Avenida Mazatlan 152A, Hipódromo Condesa, Cuauhtémoc, 06170 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '47', '19.4107956', '-99.1788314', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(800, 'Dog and Roll', '03303, Tokio 319, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.6', ' restaurant food point_of_interest establishment', '201', '19.370093', '-99.152531', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(801, 'The Rusty Cafe Gourmet', 'Eje 5 Sur 65, Américas Unidas, Benito Juárez, 03610 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png', '3.7', ' bakery cafe lodging food point_of_interest store establishment', '20', '19.3833684', '-99.1424897', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(802, 'Pet Central', 'Av México 49, Colonia Condesa, Cuauhtémoc, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '3.2', ' pet_store point_of_interest store establishment', '99', '19.4129697', '-99.1701036', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(803, 'CAN ITO', 'Santa María La Ribera 102, Sta María la Ribera, Cuauhtémoc, 06400 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.4', ' point_of_interest store establishment', '55', '19.4463111', '-99.157556', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(804, 'Mister Lukas. Pasteles para perros', 'Avenida Miguel Hidalgo 47 Int 02 Alcaldía, Del Carmen, 04100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.6', ' bakery food point_of_interest store establishment', '22', '19.351331', '-99.1530313', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(805, 'Smart Dogs', 'Parroquia 412, Col del Valle Sur, Benito Juárez, 03100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.6', ' point_of_interest store establishment', '290', '19.3704201', '-99.1741405', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(806, 'Pinche Gringo BBQ', 'Cumbres de Maltrata 360, Narvarte Oriente, 03020 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '4812', '19.3931215', '-99.1520934', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(807, 'El Ocho', 'Av México 111, Hipódromo, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png', '4.3', ' cafe restaurant food point_of_interest establishment', '3555', '19.4102994', '-99.1700098', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(808, 'EL RINCON DE LOS PERRIJOS', 'Melchor Ocampo 306, planta baja, Cuauhtemoc, Cuauhtémoc, 06500 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '5', ' pet_store veterinary_care health point_of_interest store establishment', '8', '19.4318673', '-99.1720003', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(809, 'Hound Dog', 'Cuauhtémoc 148, Del Carmen, Coyoacán, 04100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '3.9', ' restaurant food point_of_interest establishment', '377', '19.3510684', '-99.1623509', 'pet+friendly', 0, 4, 3, 13, 1, '', 0),
(810, 'Quinta Uresti', '7 de Julio 2811, Vicente Guerrero, 67160 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '4', ' lodging point_of_interest establishment', '6', '25.6890144', '-100.2293978', 'pet+friendly', 0, 13, 3, 13, 1, '', 0),
(811, 'HomeAirbnb', 'Hércules 1501, Nueva Linda Vista, 67110 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '0', ' lodging point_of_interest establishment', '0', '25.7042084', '-100.2289605', 'pet+friendly', 0, 13, 3, 13, 1, '', 0),
(812, 'Señor Dog® & Señor Cat®', 'Edificio Legorreta, Nuevo Sur, Av. Revolución 2703, Ladrillera, 64830 N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.4', ' point_of_interest store establishment', '14', '25.653361', '-100.274602', 'pet+friendly', 0, 13, 3, 13, 1, '', 0),
(813, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 6, 3, 13, 1, '', 0),
(814, 'Santa Pizza México', 'Calle Guillermo Gonzalez Camarena 11, 3, Santa Fe, Zedec Sta Fé, Álvaro Obregón, 01210 Ciudad de México, CDMX, ', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '171', '19.367851', '-99.2622048', 'pet+friendly', 0, 6, 3, 13, 1, '', 0),
(815, 'Host a Pet | Cuidadores de perros | Consentirán a tu perrito en su casa como tu lo harías', 'Avenida Mazatlan 152A, Hipódromo Condesa, Cuauhtémoc, 06170 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '47', '19.4107956', '-99.1788314', 'pet+friendly', 0, 6, 3, 13, 1, '', 0),
(816, 'SE VENDE DEPARTAMENTO 5518147851', 'Agricultura 104, Escandón II Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '0', ' lodging point_of_interest establishment', '0', '19.4004119', '-99.1779418', 'pet+friendly', 0, 6, 3, 13, 1, '', 0),
(817, 'hospedaje LOS AILES', 'COLEGIO WILLIAMS JAMES, 01530 EN LA PRIVADA DEL, Av. Tamaulipas 1190, Álvaro Obregón, 01530 CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '0', ' lodging point_of_interest establishment', '0', '19.363769', '-99.246367', 'pet+friendly', 0, 6, 3, 13, 1, '', 0),
(818, 'Fagaddie', 'Huixquilucan de Degollado, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '4.7', ' lodging point_of_interest establishment', '10', '19.3707291', '-99.3391556', 'pet+friendly', 0, 6, 3, 13, 1, '', 0),
(819, 'Dog and Roll', '03303, Tokio 319, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.6', ' restaurant food point_of_interest establishment', '201', '19.370093', '-99.152531', 'pet+friendly', 0, 6, 3, 13, 1, '', 0),
(820, 'Santa Pizza México', 'Calle Guillermo Gonzalez Camarena 11, 3, Santa Fe, Zedec Sta Fé, Álvaro Obregón, 01210 Ciudad de México, CDMX, ', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '171', '19.367851', '-99.2622048', 'pet+friendly', 0, 7, 3, 13, 1, '', 0),
(821, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 7, 3, 13, 1, '', 0),
(822, 'Host a Pet | Cuidadores de perros | Consentirán a tu perrito en su casa como tu lo harías', 'Avenida Mazatlan 152A, Hipódromo Condesa, Cuauhtémoc, 06170 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '47', '19.4107956', '-99.1788314', 'pet+friendly', 0, 7, 3, 13, 1, '', 0),
(823, 'Dog and Roll', '03303, Tokio 319, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.6', ' restaurant food point_of_interest establishment', '201', '19.370093', '-99.152531', 'pet+friendly', 0, 7, 3, 13, 1, '', 0),
(824, 'Central Pets', 'Av. División del Nte. No. 2953, El Rosedal, Coyoacán, 04330 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '4.7', ' lodging point_of_interest establishment', '37', '19.3419068', '-99.1487863', 'pet+friendly', 0, 7, 3, 13, 1, '', 0),
(825, 'The Rusty Cafe Gourmet', 'Eje 5 Sur 65, Américas Unidas, Benito Juárez, 03610 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png', '3.7', ' bakery cafe lodging food point_of_interest store establishment', '20', '19.3833684', '-99.1424897', 'pet+friendly', 0, 7, 3, 13, 1, '', 0),
(826, 'Pet Central', 'Av México 49, Colonia Condesa, Cuauhtémoc, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '3.2', ' pet_store point_of_interest store establishment', '99', '19.4129697', '-99.1701036', 'pet+friendly', 0, 7, 3, 13, 1, '', 0),
(827, 'MILO´S', 'Amsterdam, Calle Celaya &, Hipódromo Condesa, Cuauhtémoc, 06100 CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.3', ' restaurant food point_of_interest establishment', '946', '19.4143627', '-99.166915', 'pet+friendly', 0, 7, 3, 13, 1, '', 0),
(828, 'Kitchen 6', 'Teotihuacan 14, Colonia Condesa, Cuauhtémoc, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' bar restaurant food point_of_interest establishment', '944', '19.4117963', '-99.1677173', 'pet+friendly', 0, 7, 3, 13, 1, '', 0),
(829, 'La Esquina de los Milagros', 'Parque Centenario 18, Coyoacán TNT, Coyoacán, 04000 Coyoacán, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '892', '19.3489342', '-99.1642235', 'pet+friendly', 0, 7, 3, 13, 1, '', 0),
(830, 'Mister Lukas. Pasteles para perros', 'Avenida Miguel Hidalgo 47 Int 02 Alcaldía, Del Carmen, 04100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.6', ' bakery food point_of_interest store establishment', '22', '19.351331', '-99.1530313', 'pet+friendly', 0, 7, 3, 13, 1, '', 0),
(831, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 8, 3, 13, 1, '', 0),
(832, 'Host a Pet | Cuidadores de perros | Consentirán a tu perrito en su casa como tu lo harías', 'Avenida Mazatlan 152A, Hipódromo Condesa, Cuauhtémoc, 06170 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '47', '19.4107956', '-99.1788314', 'pet+friendly', 0, 8, 3, 13, 1, '', 0),
(833, 'Santa Pizza México', 'Calle Guillermo Gonzalez Camarena 11, 3, Santa Fe, Zedec Sta Fé, Álvaro Obregón, 01210 Ciudad de México, CDMX, ', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '171', '19.367851', '-99.2622048', 'pet+friendly', 0, 8, 3, 13, 1, '', 0),
(834, 'Pet Central', 'Av México 49, Colonia Condesa, Cuauhtémoc, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '3.2', ' pet_store point_of_interest store establishment', '99', '19.4129697', '-99.1701036', 'pet+friendly', 0, 8, 3, 13, 1, '', 0),
(835, 'Dog and Roll', '03303, Tokio 319, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.6', ' restaurant food point_of_interest establishment', '201', '19.370093', '-99.152531', 'pet+friendly', 0, 8, 3, 13, 1, '', 0),
(836, 'MILO´S', 'Amsterdam, Calle Celaya &, Hipódromo Condesa, Cuauhtémoc, 06100 CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.3', ' restaurant food point_of_interest establishment', '946', '19.4143627', '-99.166915', 'pet+friendly', 0, 8, 3, 13, 1, '', 0),
(837, 'Kitchen 6', 'Teotihuacan 14, Colonia Condesa, Cuauhtémoc, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' bar restaurant food point_of_interest establishment', '944', '19.4117963', '-99.1677173', 'pet+friendly', 0, 8, 3, 13, 1, '', 0),
(838, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(839, 'Guardería y Pensión de perros Casa Escan', 'Av de la Manzana 43, San Miguel Xochimanga, 52927 Cd López Mateos, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '0', ' lodging point_of_interest establishment', '0', '19.5682169', '-99.2176459', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(840, 'Host a Pet | Cuidadores de perros | Consentirán a tu perrito en su casa como tu lo harías', 'Avenida Mazatlan 152A, Hipódromo Condesa, Cuauhtémoc, 06170 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '47', '19.4107956', '-99.1788314', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(841, 'Casa Boutique Genova', 'Genova 30 Casa i, Juárez, 06600 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '22', '19.4270027', '-99.1643029', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(842, 'Giornale', 'Pafnuncio Padilla 26, Cd. Satélite, 53100 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' bakery cafe restaurant food point_of_interest store establishment', '643', '19.5127434', '-99.2319018', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(843, 'Barrio Satélite', 'Paseo de la Primavera 102, La Florida, 53160 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png', '4.4', ' bar restaurant food point_of_interest establishment', '715', '19.4991096', '-99.2362325', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(844, 'Toreo Parque Central', 'Blvrd Manuel Ávila Camacho 5, Lomas de Sotelo, 53390 Naucalpan de Juárez, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.5', ' shopping_mall point_of_interest establishment', '52819', '19.4548229', '-99.2191452', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(845, 'Petland', 'Av. Adolfo Ruiz Cortines 255, Las Margaritas, 52977 Cd López Mateos, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.1', ' point_of_interest store establishment', '306', '19.5499862', '-99.2742265', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(846, 'Rinconcito Pizza Carne y Vino', 'Calle Cairo 130, Claveria, Azcapotzalco, 02080 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.5', ' restaurant food point_of_interest establishment', '437', '19.4690376', '-99.1851309', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(847, 'Giornale', 'Eje 4 Nte 334, Magdalena de las Salinas, Gustavo A. Madero, 07370 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.3', ' restaurant bakery cafe food point_of_interest store establishment', '695', '19.484089', '-99.133415', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(848, 'Mi Pawndilla', 'Av. Jardin #330, Col del Gas, Azcapotzalco, 02950 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '5', ' lodging point_of_interest establishment', '7', '19.468983', '-99.1595607', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(849, 'Parque Purina', 'Cumbria, 54740 Cuautitlán Izcalli, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_recreational-71.png', '4.1', ' park point_of_interest establishment', '322', '19.649431', '-99.2251486', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(850, 'Parque Vía Vallejo', 'Calz. Vallejo 1090, Sta Cruz de las Salinas, Azcapotzalco, 02300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.5', ' shopping_mall point_of_interest establishment', '33862', '19.4875169', '-99.152552', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(851, 'Applebee?s Mundo E', 'Perif. Blvd. Manuel Ávila Camacho 1007, San Lucas Tepetlacalco, 54055 Tlalnepantla de Baz, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4', ' restaurant food point_of_interest establishment', '495', '19.5247551', '-99.2276401', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(852, 'MILO´S', 'Amsterdam, Calle Celaya &, Hipódromo Condesa, Cuauhtémoc, 06100 CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.3', ' restaurant food point_of_interest establishment', '946', '19.4143627', '-99.166915', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(853, 'Don Paletto', 'Mar Egeo 227, Popotla, Miguel Hidalgo, 11400 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.5', ' cafe restaurant food point_of_interest store establishment', '451', '19.4579747', '-99.1791046', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(854, 'Friendly Dogs', '5 de ignacio zaragoza 21-B, Rtno. 3 Ignacio Zaragoza, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de Méx', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '19.425206', '-99.109071', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(855, 'Pet Central', 'Av México 49, Colonia Condesa, Cuauhtémoc, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '3.2', ' pet_store point_of_interest store establishment', '99', '19.4129697', '-99.1701036', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(856, 'CAN ITO', 'Santa María La Ribera 102, Sta María la Ribera, Cuauhtémoc, 06400 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.4', ' point_of_interest store establishment', '55', '19.4463111', '-99.157556', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(857, 'Miyana Comercial', 'Av Ejército Nacional 769, Granada, Miguel Hidalgo, 11520 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.5', ' shopping_mall point_of_interest establishment', '15546', '19.4395421', '-99.1999006', 'pet+friendly', 0, 9, 3, 13, 1, '', 0),
(858, 'Guardería y Pensión de perros Casa Escan', 'Av de la Manzana 43, San Miguel Xochimanga, 52927 Cd López Mateos, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '0', ' lodging point_of_interest establishment', '0', '19.5682169', '-99.2176459', 'pet+friendly', 0, 10, 3, 13, 1, '', 0),
(859, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 10, 3, 13, 1, '', 0),
(860, 'Santa Pizza México', 'Calle Guillermo Gonzalez Camarena 11, 3, Santa Fe, Zedec Sta Fé, Álvaro Obregón, 01210 Ciudad de México, CDMX, ', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '171', '19.367851', '-99.2622048', 'pet+friendly', 0, 11, 3, 13, 1, '', 0),
(861, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 11, 3, 13, 1, '', 0),
(862, 'hospedaje LOS AILES', 'COLEGIO WILLIAMS JAMES, 01530 EN LA PRIVADA DEL, Av. Tamaulipas 1190, Álvaro Obregón, 01530 CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '0', ' lodging point_of_interest establishment', '0', '19.363769', '-99.246367', 'pet+friendly', 0, 11, 3, 13, 1, '', 0),
(863, 'El Pitbull Feliz', 'Moritas 19, 52768 San Juan Yautepec, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '3.5', ' lodging point_of_interest establishment', '2', '19.3521603', '-99.3413062', 'pet+friendly', 0, 11, 3, 13, 1, '', 0),
(864, 'Fagaddie', 'Huixquilucan de Degollado, Méx., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '4.7', ' lodging point_of_interest establishment', '10', '19.3707291', '-99.3391556', 'pet+friendly', 0, 11, 3, 13, 1, '', 0),
(865, 'Hostaluk', 'Calle Federación 26, La Perla, 44360 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '2', '20.6792117', '-103.3392484', 'pet+friendly', 0, 12, 3, 13, 1, '', 0),
(866, 'Café Providencia Chapultepec', 'Av Chapultepec Sur 126, Col Americana, Lafayette, 44160 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png', '4.2', ' cafe food point_of_interest establishment', '2070', '20.6742194', '-103.3689327', 'pet+friendly', 0, 12, 3, 13, 1, '', 0),
(867, 'Quinta Uresti', '7 de Julio 2811, Vicente Guerrero, 67160 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '4', ' lodging point_of_interest establishment', '6', '25.6890144', '-100.2293978', 'pet+friendly', 0, 14, 3, 13, 1, '', 0),
(868, 'HomeAirbnb', 'Hércules 1501, Nueva Linda Vista, 67110 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '0', ' lodging point_of_interest establishment', '0', '25.7042084', '-100.2289605', 'pet+friendly', 0, 14, 3, 13, 1, '', 0),
(869, 'Happy Dog', 'Blvrd Acapulco 3948, Torrebrisas, Las Brisas 10o. Sector, 64780 Monterrey, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '223', '25.6217957', '-100.2843693', 'pet+friendly', 0, 14, 3, 13, 1, '', 0),
(870, 'HomeAirbnb', 'Hércules 1501, Nueva Linda Vista, 67110 Guadalupe, N.L., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '0', ' lodging point_of_interest establishment', '0', '25.7042084', '-100.2289605', 'pet+friendly', 0, 15, 3, 13, 1, '', 0),
(871, 'Dollar General', '505 Black Dog Trail, Caney, KS 67333, Estados Unidos', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4', ' grocery_or_supermarket home_goods_store food point_of_interest store establishment', '152', '37.007068', '-95.9279', 'pet+friendly', 0, 17, 3, 13, 1, '', 0),
(872, 'Microtel Inn & Suites by Wyndham Miami', '2015 E Steve Owens Blvd, Miami, OK 74355, Estados Unidos', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '4.1', ' lodging point_of_interest establishment', '312', '36.8726943', '-94.8572828', 'pet+friendly', 0, 17, 3, 13, 1, '', 0),
(873, 'Pet Shop', '1833 S 10th St, Independence, KS 67301, Estados Unidos', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.6', ' point_of_interest store establishment', '70', '37.2032612', '-95.7111771', 'pet+friendly', 0, 17, 3, 13, 1, '', 0),
(874, 'Hostaluk', 'Calle Federación 26, La Perla, 44360 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '2', '20.6792117', '-103.3392484', 'pet+friendly', 0, 18, 3, 13, 1, '', 0),
(875, 'Café Providencia Chapultepec', 'Av Chapultepec Sur 126, Col Americana, Lafayette, 44160 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png', '4.2', ' cafe food point_of_interest establishment', '2070', '20.6742194', '-103.3689327', 'pet+friendly', 0, 18, 3, 13, 1, '', 0),
(876, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 19, 3, 13, 1, '', 0),
(877, 'Host a Pet | Cuidadores de perros | Consentirán a tu perrito en su casa como tu lo harías', 'Avenida Mazatlan 152A, Hipódromo Condesa, Cuauhtémoc, 06170 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '47', '19.4107956', '-99.1788314', 'pet+friendly', 0, 19, 3, 13, 1, '', 0),
(878, 'Pet Central', 'Av México 49, Colonia Condesa, Cuauhtémoc, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '3.2', ' pet_store point_of_interest store establishment', '99', '19.4129697', '-99.1701036', 'pet+friendly', 0, 19, 3, 13, 1, '', 0),
(879, 'Santa Pizza México', 'Calle Guillermo Gonzalez Camarena 11, 3, Santa Fe, Zedec Sta Fé, Álvaro Obregón, 01210 Ciudad de México, CDMX, ', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '171', '19.367851', '-99.2622048', 'pet+friendly', 0, 19, 3, 13, 1, '', 0),
(880, 'MILO´S', 'Amsterdam, Calle Celaya &, Hipódromo Condesa, Cuauhtémoc, 06100 CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.3', ' restaurant food point_of_interest establishment', '946', '19.4143627', '-99.166915', 'pet+friendly', 0, 19, 3, 13, 1, '', 0),
(881, 'Kitchen 6', 'Teotihuacan 14, Colonia Condesa, Cuauhtémoc, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' bar restaurant food point_of_interest establishment', '944', '19.4117963', '-99.1677173', 'pet+friendly', 0, 19, 3, 13, 1, '', 0),
(882, 'CAN ITO', 'Santa María La Ribera 102, Sta María la Ribera, Cuauhtémoc, 06400 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '4.4', ' point_of_interest store establishment', '55', '19.4463111', '-99.157556', 'pet+friendly', 0, 19, 3, 13, 1, '', 0),
(883, 'Friendly Dogs', '5 de ignacio zaragoza 21-B, Rtno. 3 Ignacio Zaragoza, Jardín Balbuena, Venustiano Carranza, 15900 Ciudad de Méx', 'https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png', '0', ' veterinary_care point_of_interest establishment', '0', '19.425206', '-99.109071', 'pet+friendly', 0, 19, 3, 13, 1, '', 0),
(884, 'El Ocho', 'Av México 111, Hipódromo, 06100 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png', '4.3', ' cafe restaurant food point_of_interest establishment', '3555', '19.4102994', '-99.1700098', 'pet+friendly', 0, 19, 3, 13, 1, '', 0),
(885, 'EL RINCON DE LOS PERRIJOS', 'Melchor Ocampo 306, planta baja, Cuauhtemoc, Cuauhtémoc, 06500 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png', '5', ' pet_store veterinary_care health point_of_interest store establishment', '8', '19.4318673', '-99.1720003', 'pet+friendly', 0, 19, 3, 13, 1, '', 0),
(886, 'Dog and Roll', '03303, Tokio 319, Portales Nte, Benito Juárez, 03300 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.6', ' restaurant food point_of_interest establishment', '201', '19.370093', '-99.152531', 'pet+friendly', 0, 20, 3, 13, 1, '', 0),
(887, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 20, 3, 13, 1, '', 0),
(888, 'SE VENDE DEPARTAMENTO 5518147851', 'Agricultura 104, Escandón II Secc, Miguel Hidalgo, 11800 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '0', ' lodging point_of_interest establishment', '0', '19.4004119', '-99.1779418', 'pet+friendly', 0, 20, 3, 13, 1, '', 0),
(889, 'Central Pets', 'Av. División del Nte. No. 2953, El Rosedal, Coyoacán, 04330 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '4.7', ' lodging point_of_interest establishment', '37', '19.3419068', '-99.1487863', 'pet+friendly', 0, 20, 3, 13, 1, '', 0),
(890, 'The Rusty Cafe Gourmet', 'Eje 5 Sur 65, Américas Unidas, Benito Juárez, 03610 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/cafe-71.png', '3.7', ' bakery cafe lodging food point_of_interest store establishment', '20', '19.3833684', '-99.1424897', 'pet+friendly', 0, 20, 3, 13, 1, '', 0),
(891, 'Santa Pizza México', 'Calle Guillermo Gonzalez Camarena 11, 3, Santa Fe, Zedec Sta Fé, Álvaro Obregón, 01210 Ciudad de México, CDMX, ', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '171', '19.367851', '-99.2622048', 'pet+friendly', 0, 20, 3, 13, 1, '', 0),
(892, 'Iztak Ocelot', 'Antiguo Canal Cuemanco, Zona Ecologica de Xochimilco, Xochimilco, 16040 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '4.2', ' lodging point_of_interest establishment', '6', '19.2800853', '-99.1021111', 'pet+friendly', 0, 20, 3, 13, 1, '', 0),
(893, 'Host a Pet | Cuidadores de perros | Consentirán a tu perrito en su casa como tu lo harías', 'Avenida Mazatlan 152A, Hipódromo Condesa, Cuauhtémoc, 06170 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '47', '19.4107956', '-99.1788314', 'pet+friendly', 0, 20, 3, 13, 1, '', 0),
(894, 'La Esquina de los Milagros', 'Parque Centenario 18, Coyoacán TNT, Coyoacán, 04000 Coyoacán, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '892', '19.3489342', '-99.1642235', 'pet+friendly', 0, 20, 3, 13, 1, '', 0),
(895, 'MonPet', 'Sta Úrsula Xitla, Tlalpan, 14420 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '0', ' lodging point_of_interest establishment', '0', '19.2842684', '-99.1763455', 'pet+friendly', 0, 21, 3, 13, 1, '', 0),
(896, 'Greenway Restaurante 100% Pet Friendly Polanco', 'Av. Homero 530, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX, México', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.4', ' restaurant food point_of_interest establishment', '187', '19.435396', '-99.190099', 'pet+friendly', 0, 21, 3, 13, 1, '', 0),
(897, 'Santa Pizza México', 'Calle Guillermo Gonzalez Camarena 11, 3, Santa Fe, Zedec Sta Fé, Álvaro Obregón, 01210 Ciudad de México, CDMX, ', 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png', '4.2', ' restaurant food point_of_interest establishment', '171', '19.367851', '-99.2622048', 'pet+friendly', 0, 21, 3, 13, 1, '', 0),
(898, 'Hostaluk', 'Calle Federación 26, La Perla, 44360 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png', '5', ' lodging point_of_interest establishment', '2', '20.6792117', '-103.3392484', 'pet+friendly', 0, 22, 3, 13, 1, '', 0),
(899, 'The Black Sheep', 'Libertad 1840, Col Americana, Americana, 44160 Guadalajara, Jal., México', 'https://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png', '4.2', ' bar restaurant food point_of_interest establishment', '1622', '20.6732537', '-103.3650776', 'pet+friendly', 0, 22, 3, 13, 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id_banner` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `archivos` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `fotos` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL,
  `extra` int(11) NOT NULL,
  `url` varchar(111) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `id_tipo`, `nombre`, `extra`) VALUES
(10, 3, 'Plazas', 0),
(2, 3, 'Cines', 0),
(3, 1, 'Veterinario General', 0),
(4, 1, 'Veterinario de animales pequeños', 0),
(5, 1, 'Veterinario de animales grandes', 0),
(6, 1, 'Veterinarios especializados', 0),
(7, 2, 'Estetica canina', 0),
(8, 2, 'Spa para mascotas', 0),
(1, 0, 'Todas', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categoria_entrada`
--

CREATE TABLE `categoria_entrada` (
  `id_categoria_entrada` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categoria_entrada`
--

INSERT INTO `categoria_entrada` (`id_categoria_entrada`, `nombre`, `descripcion`, `extra`) VALUES
(1, 'General', '', 0),
(2, 'Salud', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categoria_marca`
--

CREATE TABLE `categoria_marca` (
  `id_categoria_marca` int(11) NOT NULL,
  `id_marca` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categoria_marca`
--

INSERT INTO `categoria_marca` (`id_categoria_marca`, `id_marca`, `nombre`, `extra`, `orden`) VALUES
(1, 8, 'Blue Buffalo', 0, 0),
(2, 6, 'Beneful', 0, 0),
(3, 9, 'Best Choice', 0, 0),
(4, 10, 'Campeón', 0, 0),
(5, 11, 'Choice Nutrition', 0, 0),
(6, 12, 'Chop!', 0, 0),
(7, 13, 'Criador', 0, 0),
(8, 14, 'Diamond', 0, 0),
(9, 15, 'Dog Chow', 0, 0),
(10, 16, 'Eukanuba', 0, 0),
(11, 17, 'FullTrust', 0, 0),
(12, 18, 'Ganador', 0, 0),
(13, 19, 'Golden Dog Nufit', 0, 0),
(14, 20, 'Hi Multipro', 0, 0),
(15, 21, 'Hills SC', 0, 0),
(16, 22, 'Hills PR', 0, 0),
(17, 23, 'Hills IB', 0, 0),
(18, 24, 'Kirkland Signature', 0, 0),
(19, 25, 'Hoger Hond', 0, 0),
(20, 26, 'IAMS', 0, 0),
(21, 27, 'Instinct', 0, 0),
(22, 28, 'L H &S', 0, 0),
(23, 29, 'Lideratto', 0, 0),
(24, 30, 'Mainstay Purina', 0, 0),
(25, 31, 'Maxi Bobby', 0, 0),
(26, 32, 'MIRA', 0, 0),
(27, 33, 'Members Marks', 0, 0),
(28, 34, 'Merrick', 0, 0),
(29, 35, 'Natural Gourmet', 0, 0),
(30, 36, 'NUCAN', 0, 0),
(31, 37, 'North Paw', 0, 0),
(32, 38, 'Nupec', 0, 0),
(33, 39, 'NutresCan', 0, 0),
(34, 40, 'Pal Perro', 0, 0),
(35, 41, 'Pedigree', 0, 0),
(36, 42, 'Purina One', 0, 0),
(37, 43, 'Prairie', 0, 0),
(38, 44, 'Pro Plan', 0, 0),
(39, 45, 'Canidar Pure', 0, 0),
(40, 46, 'Royal Canin', 0, 0),
(41, 47, 'Tier Holistic', 0, 0),
(42, 48, 'The Honest Kitchen', 0, 0),
(43, 49, 'Whole Earth Farms', 0, 0),
(44, 50, 'Whole Hearted', 0, 0),
(45, 51, 'X-Dog', 0, 0),
(46, 52, 'Zoopet', 0, 0),
(47, 53, 'Top Choice', 0, 0),
(48, 14, 'Natural', 0, 0),
(49, 14, 'Premium', 0, 0),
(50, 14, 'Taste of the Wild', 0, 0),
(51, 44, 'Cachorro', 0, 0),
(52, 44, 'Adulto', 0, 0),
(53, 44, 'Optistart', 0, 0),
(54, 44, 'Optipower', 0, 0),
(55, 44, 'Optihealth', 0, 0),
(56, 44, 'Exigent Optienrich', 0, 0),
(57, 44, 'Reduced Calorie', 0, 0),
(58, 44, 'Sensitive Digestion', 0, 0),
(59, 44, 'Sensitive Skin', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `codueno`
--

CREATE TABLE `codueno` (
  `id_codueno` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_mascota` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_nacimiento` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `ano` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `codueno`
--

INSERT INTO `codueno` (`id_codueno`, `id_usuario`, `id_mascota`, `nombre`, `apellido`, `fecha_nacimiento`, `email`, `telefono`, `extra`, `dia`, `mes`, `ano`) VALUES
(1, 2, 2, 'Pedro', 'Perez', '', 'dasd@dasd.com', '123213213', 0, 6, 6, 1998),
(2, 2, 3, 'fsdf', 'sdfdsf', '', 'fsdfdsf', '455345', 0, 4, 6, 2016),
(3, 2, 3, 'sdfdsf', 'vxcvcxv', '', 'fdsf', '5345435', 0, 7, 7, 2012),
(4, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(5, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(6, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(7, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(8, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(9, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(10, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(11, 1, 12, 'co', 'dueño', '', 'Héctor', '553579', 0, 12, 4, 1989),
(12, 1, 12, 'hey', 'hey', '', 'hey', 'hey', 0, 10, 10, 1987),
(13, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(14, 1, 12, 'Liz', 'Gutierrez ', '', 'Liz', 'cel', 0, 22, 1, 1996),
(15, 1, 12, 'Liz', 'Gutierrez ', '', 'Liz', 'cel', 0, 22, 1, 1996),
(16, 4, 38, 'Héctor', 'Aguilar', '', 'hector@aguitech.com', '5545249906', 0, 12, 4, 1989),
(17, 4, 38, 'Héctor', 'Aguilar', '', 'hector@aguitech.com', '5545249906', 0, 12, 4, 1989),
(18, 4, 38, 'Héctor', 'Aguilar', '', 'hector@aguitech.com', '5545249906', 0, 12, 4, 1989),
(19, 4, 38, 'Héctor', 'Aguilar', '', 'hector@aguitech.com', '5545249906', 0, 12, 4, 1989),
(20, 2, 1, 'testeo', 'testeo', '', 'srf@ffr.com', '434535', 0, 1, 1, 1),
(21, 2, 1, 'testeo', 'testeo', '', 'srf@ffr.com', '434535', 0, 1, 1, 1),
(22, 2, 1, 'testeo', 'testeo', '', 'srf@ffr.com', '434535', 0, 1, 1, 1),
(23, 2, 1, 'testeo', 'testeo', '', 'srf@ffr.com', '434535', 0, 1, 1, 1),
(24, 2, 1, 'h', 's', '', 'a', 'a', 0, 1, 1, 1),
(25, 2, 1, 'uno', 'dos', '', 'dsads@adasdsa.com', '1234567890', 0, 1, 5, 2017),
(26, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(27, 1, 12, '', '', '', '', '', 0, 0, 0, 0),
(28, 2, 1, 'codueno 1', 'apellido de codueno', '', 'mail@codueno.com', '34455', 0, 2, 2, 0),
(29, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(30, 39, 84, 'Luciano ', 'Formica ', '', '', '', 0, 1, 9, 2000),
(31, 0, 0, '', '', '', '', '', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dueno`
--

CREATE TABLE `dueno` (
  `id_dueno` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_mascota` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_nacimiento` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `ano` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dueno`
--

INSERT INTO `dueno` (`id_dueno`, `id_usuario`, `id_mascota`, `nombre`, `apellido`, `fecha_nacimiento`, `email`, `telefono`, `extra`, `dia`, `mes`, `ano`) VALUES
(3, 2, 1, 'Juan', 'PErez', '', 'pollito32@dasd.com', '321651231', 0, 9, 10, 2013),
(2, 2, 1, 'nombre dueno', 'apellido dueno', '', 'emailo', 'telefono', 0, 2, 3, 2001),
(4, 23, 15, 'Cindy M', 'Lozano', '', '', '', 0, 26, 7, 1986),
(5, 23, 15, 'Cindy M', 'Lozano', '', '', '', 0, 26, 7, 1986),
(6, 23, 15, 'Cindy M', 'Lozano', '', '', '', 0, 26, 7, 1986),
(7, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(8, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(9, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(10, 2, 2, 'hgg', 'gg', '', 'hgt', 'ghu', 0, 4, 6, 1985),
(11, 2, 2, 'hgg', 'gg', '', 'hgt', 'ghu', 0, 4, 6, 1985),
(12, 2, 2, 'hgg', 'gg', '', 'hgt', 'ghu', 0, 4, 6, 2019),
(13, 2, 2, 'hgg', 'gg', '', 'hgt', 'ghu', 0, 4, 6, 2019),
(14, 2, 2, 'hgg', 'gg', '', 'hgt', 'ghu', 0, 4, 6, 2019),
(15, 2, 3, 'fdds', 'fsdfdsf', '', 'fsdfsdf', 'fsdfsdf', 0, 4, 5, 2014),
(16, 2, 3, 'fdds', 'fsdfdsf', '', 'fsdfsdf', 'fsdfsdf', 0, 4, 5, 2014),
(17, 2, 3, 'fdds', 'fsdfdsf', '', 'fsdfsdf', 'fsdfsdf', 0, 4, 5, 2019),
(18, 23, 15, 'Cindy M', 'Lozano', '', 'lozano.cindy@gmail.com', '5545442990', 0, 26, 7, 2019),
(19, 23, 15, 'Cindy M', 'Lozano', '', 'lozano.cindy@gmail.com', '5545442990', 0, 26, 7, 2019),
(20, 23, 15, 'Cindy M', 'Lozano', '', 'lozano.cindy@gmail.com', '5545442990', 0, 26, 7, 2019),
(21, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(22, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(23, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(24, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(25, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(26, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(27, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(28, 1, 12, 'gen', 'gen', '', 'gl', 'ga', 0, 10, 10, 1989),
(29, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(30, 1, 12, 'hola', 'holaa', '', 'huijvgujg@gmail.com', '67834', 0, 0, 0, 0),
(31, 4, 38, 'hector', 'Aguilar', '', 'hector@aguitech.com', '5545249906', 0, 12, 4, 1989),
(32, 4, 38, 'hector', 'Aguilar', '', 'hector@aguitech.com', '5545249906', 0, 12, 4, 1989),
(33, 4, 38, 'hector', 'Aguilar', '', 'hector@aguitech.com', '5545249906', 0, 12, 4, 1989),
(34, 2, 1, '', '', '', '', '', 0, 0, 0, 0),
(35, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(36, 0, 12, '', '', '', '', '', 0, 0, 0, 0),
(37, 0, 12, '', '', '', '', '', 0, 0, 0, 0),
(38, 2, 1, 'asd', 'asda', '', 'dsadsadsa@asadsad.com', '1234567890', 0, 1, 1, 2015),
(39, 2, 1, 'asde', 'asda', '', 'dsadsadsa@asadsad.com', '1234567890', 0, 1, 1, 2015),
(40, 2, 1, 'asde', 'asda', '', 'dsadsadsa@asadsad.com', '1234567890', 0, 1, 1, 2015),
(41, 2, 1, 'asde', 'asda', '', 'dsadsadsa@asadsad.com', '1234567890', 0, 1, 3, 2015),
(42, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(43, 2, 1, 'asde de de', 'asda de de', '', 'dsadsadsa@asadsad.com', '1234567890', 0, 8, 3, 2015),
(44, 2, 1, 'asde de de', 'asda de de', '', 'este@este.com', '6655', 0, 8, 3, 2015),
(45, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(46, 2, 1, 'asde de de', 'asda de de', '', 'este@este.com', '6655', 0, 8, 3, 2015),
(47, 2, 1, '', 'asda de de', '', 'este@este.com', '6655', 0, 8, 3, 2015),
(48, 2, 1, '', 'asda de de', '', 'este@este.com', '6655', 0, 8, 3, 2015),
(49, 2, 1, '', 'asda de de', '', 'este@este.com', '6655', 0, 8, 3, 2015),
(50, 2, 1, '', 'asda de de', '', 'este@este.com', '6655', 0, 8, 3, 2015),
(51, 2, 1, '', 'asda de de', '', 'este@este.com', '6655', 0, 8, 3, 2015),
(52, 2, 1, 'fg', 'asda de de', '', 'este@este.com', '6655', 0, 8, 3, 2015),
(53, 2, 1, 'fg', 'asda de de', '', 'este@este.com', '6655', 0, 8, 3, 2015),
(54, 2, 1, 'fg', 'asda de de', '', 'este@este.com', '6655', 0, 8, 3, 2015),
(55, 39, 84, 'Aby', 'Barahona', '', 'abrilbarahona@gmail.com', '2615063114', 0, 3, 7, 2003),
(56, 39, 84, 'Aby', 'Barahona', '', 'abrilbarahona@gmail.com', '2615063114', 0, 3, 7, 2003),
(57, 39, 84, 'Aby', 'Barahona', '', 'abrilbarahona@gmail.com', '2615063114', 0, 3, 7, 2003),
(58, 39, 84, 'Aby', 'Barahona', '', 'abrilbarahona@gmail.com', '2615063114', 0, 3, 7, 2003),
(59, 39, 84, 'Aby', 'Barahona', '', 'abrilbarahona@gmail.com', '2615063114', 0, 3, 7, 2003),
(60, 49, 86, 'manuel', 'gil', '', 'fgil@corefix.com.mx', '12341234', 0, 5, 1, 1991),
(61, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(62, 39, 84, '', '', '', '', '', 0, 0, 0, 0),
(63, 39, 84, 'Aby', 'Barahona', '', 'abrilbarahona@gmail.com', '2615063114', 0, 3, 7, 2003),
(64, 39, 84, 'Aby', 'Barahona', '', 'abrilbarahona@gmail.com', '2615063114', 0, 3, 7, 2003);

-- --------------------------------------------------------

--
-- Table structure for table `entrada`
--

CREATE TABLE `entrada` (
  `id_entrada` bigint(20) NOT NULL,
  `id_categoria_entrada` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `introduccion` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` varchar(8000) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `archivos` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `fotos` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL,
  `busqueda_tipo` int(12) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `entrada`
--

INSERT INTO `entrada` (`id_entrada`, `id_categoria_entrada`, `nombre`, `introduccion`, `contenido`, `imagen`, `archivos`, `fotos`, `extra`, `busqueda_tipo`) VALUES
(17, 1, 'Consejos para después de  viajar a la playa. ', 'Para el regreso a casa debes seguir estos consejos:', 'Después del viaje\r\n\r\n1.- Seguramente al llegar a casa lo único que hará tu perro es correr a su lugar de descanso y quedarse ahí por un par de horas. Esto es normal, pues todo su día estuvo lleno de nuevas cosas que lo mantuvieron activo y alerta todo el tiempo.\r\n\r\n2.- No olvides lavar o limpiar todo lo que llevaste a la playa para tu mascota: juguetes, platos, etc.\r\n\r\n3.- Luego de su descanso evalúa su comportamiento, es decir, que coma su comida, que haga sus necesidades, todo tal cual lo hacía regularmente. Si esto ocurre, tu mascota habrá tenido un lindo día de vacaciones que de seguro querrá repetir!\r\n\r\nNota: Si vas a viajar por más de un día, ten en cuenta lo siguiente\r\n\r\n*Averigua si el hotel al que llegarás recibe mascotas, no intentes meterlo a escondidas pues podrías generar tremendo problema a los demás huéspedes.\r\n*Si vas a quedarte una noche en la playa, recuerda llevarle su lugar de descanso, solo así tu perro  dormirá y no pasará toda la noche intranquilo.', 'https://honeywell-show.info/uploaded/1562033764.jpg', '', 'http://honeywell-show.info/uploaded/1562033764.jpg', 0, 1),
(14, 1, 'Consejos para antes de viajar con tu perro a la playa.', '\r\nSi bien es cierto, no es una actividad muy común en nuestra sociedad, ¿por qué no llevar a tu mejor amigo a la playa? A continuación algunos tips para considerar antes del paseo.', 'Antes del viaje:\r\n\r\n1.- Debes tener en cuenta que muchos perros sufren de ansiedad al salir a la calle, por tal motivo debes visitar a tu veterinario y preguntar por el tipo de medicación (leve) que podrías darle para que tenga un viaje tranquilo. Se debe tener presente que si un perro jamás ha viajado en vehículo vomitará. Para ello también deberás consultar el medicamento apropiado considerando el tamaño de tu mascota y tiempo de viaje.\r\n\r\n2.- Chequear como está su control de pulgas y garrapatas, nunca sabrás si el lugar al que va podría agarrar alguna de ellas. Por eso es bueno que se le aplique algún producto anti-pulgas y garrapatas con previa precaución.\r\n\r\n3.- No olvides llevar su collar de identificación con tus datos de contacto.\r\n\r\n4.- Prepara un listado de todas las cosas que debes empacar para tu mascota. Por ejemplo:\r\n\r\n*Comida\r\n*Agua\r\n*Plato para sus alimentos\r\n*Toalla para secarlo\r\n*Juguetes\r\n*Medicina (primeros auxilios y propia de la mascota)\r\n*Carnet de vacunación al día\r\n*Fundas para recolección de excrementos.\r\n\r\n5.- Prepara el lugar en el que tu mascota viajará. Es recomendable que viaje en un Kennel, siempre y cuando ya haya tenido esa experiencia, pues de caso contrario resultará una actividad totalmente estresante y terminará en una crisis para tu mascota.\r\n\r\n6.- Si tu mascota ya está acostumbrada a viajar, puedes colocarla en la parte de atrás de tu vehículo con algún elemento que le genere confianza a tu mascota. En este punto debes tener precaución con la limpieza del lugar.', 'https://honeywell-show.info/uploaded/1562032969.jpg', '', 'http://honeywell-show.info/uploaded/1562032969.jpg', 0, 1),
(16, 1, 'Tips durante el viaje con tu perro a la playa', 'Llevar a una mascota de viaje debe ser una experiencia agradable y natural, no una carga de stress e intolerancia!', '\r\nDurante el viaje:\r\n\r\n1.- Es recomendable que viajes en un vehículo propio, pues deberás hacer paradas constantes para que tu perro pueda hacer sus necesidades! Recuerda que el simple hecho de salir de casa los pone un poquito nerviosos y querrán hacer sus necesidades cuanto antes.\r\n\r\n2.- Al llegar a la playa recuerda que lo primero que tu perro te pedirá de una forma u otra es agua, pues al fin salió de la tensión del viaje y antes de explorar el nuevo lugar necesitará sentirse relajado.\r\n\r\n3.- Si tu perrito es algo agresivo, procura tenerlo siempre con su collar, caso contrario tendrás algunos inconvenientes durante tu viaje.  Los niños siempre tratarán de acariciar a todas las mascotas que ven por el camino, es algo natural para ellos, sin embargo será un total problema para ti si tu perro llega a morder a uno, y por este motivo debes tener a la mano tu carnet con todas sus vacunas al día.\r\n\r\n4.- Busca un espacio de la playa un poco tranquilo, recuerda que al ver tantas personas adultas y niños podría alterar a tu perrito.\r\n\r\n5.- Una vez instalado en tu espacio, con parasol y demás, inicia actividades con tu mascota, por ejemplo, jugar con su pelota favorita, hacer caminatas largas, correr, etc.\r\n\r\n6.- Recuerda que tu perro es como un niño, no intentes meterlo al mar apenas llegues. Deja que se familiarice con el lugar, que huela, que admire el paisaje y que se cree confianza para que entienda que el mar también es bueno para él. Podrías hacer el siguiente ejercicio, que vea que ingresas al mar y luego poco a poco que moje sus patitas hasta que se pueda bañar por completo. Atención, no pretendas que tu perro vaya al fondo y regrese, un baño en la orilla será suficiente.  Tampoco uses la correa al momento de acercarlo al mar.\r\n\r\n7.- No olvides secar a tu mascota apenas salga del agua, si hay sol y está jugando quizás no sea necesario. Pero si está algo nervioso y temblando, deberás  secarlo y que se quede junto a ti bajo tu parasol.\r\n\r\n8.- Al transcurrir de las h', 'https://honeywell-show.info/uploaded/1562033506.jpg', '', 'http://honeywell-show.info/uploaded/1562033506.jpg', 0, 1),
(18, 2, '8 Tips para cuidar un gatito recién nacido.', 'Si por alguna circunstancia tienes en casa un gato recién nacido que no tiene a su mamá, es fundamental que le ofrezcas mucha atención y todos los cuidados posibles para que crezca sano y fuerte.', '1.- Un gatito recién nacido no regula su temperatura fácilmente, aunque esto lo compensa juntándose con su mamá y con las demás crías. Pero si ellos no están, tú serás el encargado de mantener la temperatura de tu minino, para ello deberás usar mantas para que el gatito que acaba de nacer esté calentito, aunque es muy importante que estas mantas no pesen demasiado.\r\n2.- Puedes colocar un peluche en la cuna de tu cachorro, para que haga las funciones de madre y se pueda apoyar sobre él. Esto le dará confianza y calidez, por lo que se sentirá más reconfortado en sus primeros días de vida. \r\n3.- Si dispones de él, también puedes poner en su camita un reloj o despertador de aquellos antiguos que hacen un suave TIC TAC. Este sonido le recuerda al cachorro gatuno el latir del corazón de la madre. Esto hará que se encuentre mejor y no se sienta solo.\r\n4.-Es esencial que sepas que en las cuatro primeras semanas de vida, los gatitos únicamente toman líquidos. Tendrás que darle al minino leche apropiada para su edad. Bajo ningún concepto le des leche de vaca ya que le puede sentar muy mal. Podrás encontrar leche para gatitos en cualquier tienda veterinaria o en tiendas de mascotas online.\r\n5.- A un gatito recién nacido, como norma general, tienes que darle trece mililitros por cada cien gramos de peso, aunque el propio minino, cuando no tenga hambre, dejará de tomar.\r\n6.- En lo referente a la asiduidad de las tomas de la leche, si el felino se halla en condiciones saludables, será suficiente alimentarlo cuatro veces al día, si no es así, tendrás que darle su toma de leche cada dos horas.\r\n7.- Para suministrar la leche al felino puedes usar una jeringa, un gotero o un biberón. Es imprescindible que sea cual sea el utensilio que utilices, extremes las medidas de higiene y lo limpies concienzudamente después de usarlo, de este modo podrás alimentar a tu gato recién nacido a la vez que cuidas su salud.\r\n8.-  Un gatito recién nacido precisa de la estimulación de su mamá para orinar y defecar, pero a falta de ella, tú tienes que asumir dicha labor. Para hacerlo, puedes utilizar un algodón que deberás pasar con mucha suavidad por los genitales y el ano de tu pequeña mascota con el fin de que haga sus necesidades.', 'https://honeywell-show.info/uploaded/1562078256.jpg', '', 'http://honeywell-show.info/uploaded/1562078256.jpg', 0, 1),
(6, 2, '7 cuidados para los perros en verano', 'Los animales sufren los cambios de temperatura igual que las personas. Por lo tanto, los perros en verano necesitan de ciertos cuidados adicionales. Presta atenciÃ³n a estos consejos.', 'Los animales sufren los cambios de temperatura igual que las personas. Por lo tanto, los perros en verano necesitan de ciertos cuidados adicionales. Presta atenciÃ³n a estos consejos:\r\n\r\n1. Salid en las horas de menos calor\r\nDurante el mediodÃ­a â€“de 12:00 a 16:00 horasâ€“ serÃ­a bueno que se queden todos en casa al resguardo del calor y el sol. Si estÃ¡is en la playa, podÃ©is regresar al alojamiento o quedaros a la sombra y con mucha agua fresca disponible.\r\n\r\nEn ese periodo mejor las siestas que las caminatasâ€¦ Â¡Y mucho menos el ejercicio o los juegos al aire libre! Ten en cuenta que un golpe de calor puede ser fatal para tu mascota; incluso puede sufrir quemaduras en sus almohadillas, nariz o piel.\r\n\r\n2. No le cortes el pelo\r\nCuando comienzan los dÃ­as mÃ¡s agradables probablemente a tu mascota se le caiga el pelo. Si eso no sucede, te recomendamos que no se lo cortes al ras de la piel, ya que esto puede ser muy peligroso. Claro, porque no tendrÃ¡ ninguna protecciÃ³n frente al sol.\r\n\r\nEvita los cortes de pelo en ciertas razas, como por ejemplo chow chow, pastor alemÃ¡n, labrador o border collie. Estos perros tienen dos capas de pelo, que mudan segÃºn la Ã©poca del aÃ±o. Lo que sÃ­ puedes hacer es cepillarlos a diario para eliminar suciedad y pelo muerto.\r\n\r\n3. No lo dejes en el coche\r\nÂ¿SabÃ­as que dejar a tu perro en el coche es lo mismo que condenarlo a una muerte horrible? Aunque solo sean cinco minutos para hacer una compra, en ese lapso el animal sufre mucho y puede fallecer.\r\n\r\nNo se puede hacer en ningÃºn momento del aÃ±o, mucho menos en verano, ya que el coche se vuelve un â€˜hornoâ€™ sin oxÃ­geno en dos minutos. Es preferible que lo dejes atado en la puerta de la tienda o que alguien se quede cuidÃ¡ndole en el coche con las ventanas abiertas.\r\n\r\n4. Que siempre tenga agua\r\nAunque estÃ© en casa, el animal puede sentir mucha sed y necesitar refrescarse varias veces al dÃ­a. Antes de salir del trabajo, o apenas te levantas, ponle agua nueva en el recipien', 'https://honeywell-show.info/uploaded/1562012394.jpg', '', 'http://honeywell-show.info/uploaded/1562012394.jpg', 0, 1),
(8, 2, '10 consejos para refrescar a tu perro en verano', 'El calor intenso puede reducir el apetito y las ganas de moverse, pero también puede tener consecuencias más graves, como el temido golpe de calor. En este artículo encontrarás algunas sugerencias para proteger a tu perro ', '.\r\n\r\n1. Mantenlo hidratado\r\nDéjale agua fresca y limpia a disposición las 24 horas del día y comprueba periódicamente que quede agua en el bebedero y que el bebedero no esté en pleno sol: en verano es más fácil que el agua evapore o simplemente se caliente y el perro deje de beber por eso.\r\n\r\nTambién puedes colocar más bebederos en casa, sobre todo cerca de donde descansa el perro. De esta manera, incluso si el perro no se quisiera mover siempre tendría un bebedero cerca. El agua no es importante solo en casa: no olvides llevarte agua y un bebedero portátil cuando sales de paseos, vas de excursión o haces un viaje en coche con tu perro.\r\n\r\n2. Prepárale helados caseros\r\nPrueba a poner algún cubito de hielo en el bebedero. El hielo no es perjudicial para los perros y si para el perro el agua fuera demasiado fría, dejarle a disposición otro bebedero sin cubitos le permitirá escoger el agua que prefiere. Con el hielo también se pueden preparar juguetes caseros.\r\n\r\nPara hacerlo solo se necesita un molde para helado o un contenedor de plástico, un poco de agua y unos granos de pienso. Se mezcla el pienso con el agua y cuando el agua se ha congelado, se saca el bloque de hielo, se pasa debajo del agua para que la lengua no se le enganche, y se da al perro. Eso sí, para este tipo de juego lo ideal es tener un espacio exterior, como una terraza: este juguete casero suele tener mucho éxito entre los perros, pero habrá que limpiar después de cada sesión de juego.\r\n\r\n3. Protégelo del sol\r\nEs muy importante que el perro tenga un lugar fresco y protegido del sol donde descansar, sobre todo en las horas más cálidas del día. Esto se aplica tanto si vive dentro de casa como si solo vive en el exterior.\r\n\r\n4. Refresca el ambiente\r\nUtilizar un ventilador o el aire acondicionado puede ser una buena medida para bajar la temperatura ambiental.\r\n\r\n5. Mójalo\r\nMojarle, sobre todo en la cabeza y en el tronco, puede ayudar a bajar la temperatura del perro. Los perros no sudan como las persona', 'https://honeywell-show.info/uploaded/1562013443.jpg', '', 'http://honeywell-show.info/uploaded/1562013443.jpg', 0, 1),
(10, 2, 'Cómo cuidar a tu perro en verano', 'En verano debido a las altas temperaturas tenemos que prestar más atención a nuestro perro.', 'Los 5 sentidos de tu perro en verano\r\nOído\r\nSécale bien los oídos después de cualquier baño, la humedad puede causarle otitis.\r\n\r\nTen precaución con las espigas. Si ves que el tu perro sacude la cabeza constantemente después de haber estado en zona de campos, debes llevarlo cuanto antes al veterinario.\r\n\r\nOlfato\r\nCuidado con las espigas, las ramas secas o el polvo que pueden inhalar. Pueden provocarle estornudos en cadena y dañarle las vías respiratorias.\r\n\r\nVista\r\nSi se les recorta el pelo de los ojos, tanto la arena como la luz del sol pueden ser perjudiciales para su vista. Cuidado con el agua del mar y de la piscina, puede irritarle los ojos.\r\n\r\nGusto\r\nEn verano tienen más sensibilidad. Ten mucho cuidado y evita que coma piedras, plantas, plásticos, restos de alimentos o excrementos.\r\n\r\nTacto\r\nIntenta sacar a pasear a tu perro en las horas de menos sol, ya sea a primera hora de la mañana o por la tarde. La arena y el asfalto podrían quemarle las almohadillas de las patas.\r\n\r\nCuida a tu compañero durante los días de calor\r\nAlimentación\r\nIntenta repartir sus raciones de alimento a lo largo de la jornada, haciendo coincidir sus comidas con las horas de menos calor.\r\n\r\nSi es posible, evita sacarlo a pasear después de sus comidas.\r\n\r\nHidratación\r\nProcura que tenga agua fresca y limpia las 24 horas del día.\r\n\r\nAñade cubitos de hielo en el bebedero.\r\n\r\nUtiliza un ventilador o aire acondicionado para bajar la temperatura.\r\n\r\nMójalo en la cabeza y en el tronco.\r\n\r\nProtección contra parásitos y mosquitos\r\nProtege a tu perro especialmente en verano contra parásitos y el mosquito de Leishmania con una pipeta mensual.\r\n\r\nProcura estar al día con la cartilla de vacunación.\r\n\r\nUtiliza el collar antipulgas y sustitúyelo cuando esté caducado.\r\n\r\nIntenta cepillarlo dos o tres veces por semana y bañarlo por lo menos una vez al mes con productos especiales para perros.\r\n\r\nDisfruta con tu perro durante las vacaciones\r\nVIAJES\r\nCOCHE: Acuérdate del transportín y de sus juguetes para e', 'https://honeywell-show.info/uploaded/1562017390.jpg', '', 'http://honeywell-show.info/uploaded/1562017390.jpg', 0, 1),
(20, 1, 'Consejos para convivencia perro y niño.', 'Está claro que los perros son una de las mascotas preferidas por los niños. Pero los humanos adultos tienen que tener en cuenta algunos consejos para que ninguno de nosotros sufra ningún percance. ', '-No los dejes solos. Siempre que estemos juntos tendrá que haber un adulto presente, ya que a veces se nos puede cruzar algún cable y podemos intentar morder al niño. Tampoco dejes que el pequeño de la casa nos moleste mientras comemos, dormimos o estamos enfermos.\r\n-Convéncenos a los dos de que los juegos basados en peleas o demasiado violentos no son buena idea. \r\n-Elige una raza que sea compatible con los niños. Entérate de cuáles son los cuidados que necesita. Lo mejor es que te decantes por una raza que requiera mucha actividad, ya que tendrá que aguantar el ritmo del niño de la casa.\r\n-Socialízalo. Los perros necesitamos tener contacto con otros perros, con los ruidos de la calle, con otras personas... Si no, cuando estemos ante ellos, reaccionaremos de forma violenta.\r\n-Avisa a tus niños de no acercarse a perros desconocidos, ya que no sabemos cómo van a reaccionar. Tampoco deben salir corriendo, ya que los canes olemos el miedo.\r\n', 'https://honeywell-show.info/uploaded/1562087533.jpg', '', 'http://honeywell-show.info/uploaded/1562087533.jpg', 0, 1),
(21, 2, 'Tips para bañar por primera vez a tu gato', '\r\nSigue estas recomendaciones para que el baño de tu gato sea una experiencia placentera para ambos.', 'Ten a la mano los artículos de baño:\r\nCepillo, jabón, champú o acondicionador (esto depende del pelo que tenga tu gato) , aceite mineral, alfombra de goma, esponja de ducha, bolitas de algodón y dos toallas.Hazlo sentir seguro\r\nColoca una alfombra de goma en la bañera: tu gato se sentirá mucho más seguro si no se resbala. Prepara el champú y/o acondicionador adecuado; debes tomar en cuenta que no todas las razas ni tipos de cabello requieren los mismos cuidados, por lo que te aconsejamos que preguntes a tu veterinario de cabecera qué tipo de pelo tiene tu mascota.\r\nPrepáralo antes de bañarlo:\r\nAntes de abrir el agua, cepíllalo para eliminar el pelo muerto y desenreda los nudos que pueda tener. Aplica una gota de aceite mineral en la esquina de cada ojo del minino para evitar que le entre jabón. Coloca unas bolitas de algodón en sus oídos para impedir la entrada del agua, eso sí, no los introduzcas muy adentro porque podrías lastimarlo.\r\nObserva el estrés de tu minino\r\nSi ves que tu minino se está poniendo nervioso y agresivo, sácalo de la bañera y deja que se marche. Vuelve a intentarlo en una o dos semanas. Si, por el contrario, no presenta signos de estrés, sigue adelante con el baño.\r\nLa temperatura del agua ideal\r\nAbre el agua y regúlala para que esté templada (unos 38°C) . Con suavidad ve mojando a tu gato asegurándote de que el agua llegue a todos los rincones de su cuerpo, excepto en la cara porque el agua caliente puede crear infección en sus ojos y oídos.', 'https://honeywell-show.info/uploaded/1562088023.jpg', '', 'http://honeywell-show.info/uploaded/1562088023.jpg', 0, 1),
(22, 2, 'Señales de que tu gato es agresivo', 'Las Conductas Agresivas a Menudo Muestran Señales Sutiles de Advertencia', 'Si observas uno o más de los siguientes comportamientos en tu gato, podría estar tomando actitudes agresivas:\r\n\r\nActitudes temerosas\r\nAzotar la cola\r\nFrotarse la cabeza en exceso\r\nRepetición del comportamiento defensivo hacia el propietario\r\nOrinar o defecar fuera de su caja de arena\r\nTu gatito puede comenzar a presentar estas conductas en repetidas ocasiones de forma diaria o semanal. O puede pasar gran parte de su tiempo escondiéndose y al salir, tener conductas agresivas como resoplar, golpear, gruñir y/o morder.\r\n\r\nEvitar las Situaciones Estimulantes y Crear un Espacio Seguro\r\nEn los gatos, la agresividad hacia los extraños, por lo general es provocada por miedo, territorialismo, acariciar y/o jugar. El tratamiento debe comprender, evitar los detonadores, proporcionarle un espacio seguro a tu gato, enriquecer su entorno y la desensibilización/contracondicionamiento.', 'https://honeywell-show.info/uploaded/1562088494.jpg', '', 'http://honeywell-show.info/uploaded/1562088494.jpg', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `estado`
--

CREATE TABLE `estado` (
  `id_estado` int(11) NOT NULL,
  `nombre` varchar(121) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `longitud` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `zoom` float NOT NULL,
  `extra` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `estado`
--

INSERT INTO `estado` (`id_estado`, `nombre`, `latitud`, `longitud`, `zoom`, `extra`) VALUES
(1, 'CDMX', '19.41036036043098', '-99.16975378990173', 11, 1),
(2, 'Estado de Mexico', '19.29633964882271', '-99.65315222740173', 8, 1),
(6, 'Puebla', '18.69204554320027', '-98.18527579307556', 9, 0),
(5, 'Michoacan', '19.29633964882271', '-101.41096472740173', 8, 0),
(8, 'Puebla', '19.041450214461733', '-98.20845007896423', 11, 0),
(9, 'Guadalajara', '20.658586577716097', '-103.34043860435486', 11, 1),
(10, 'Querétaro', '20.582432020069202', '-100.37927985191345', 12, 0),
(11, 'Estado de México', '19.28240552536914', '-99.65383887290955', 11, 0),
(12, 'Verácruz', '19.17315988211702', '-96.14782691001892', 12, 0),
(13, 'Oaxaca', '16.773747085921038', '-96.43965125083923', 8, 0),
(14, 'Nuevo León', '25.699178395913346', '-100.28898596763611', 10, 1),
(15, 'Colima', '19.25032013042258', '-103.71740698814392', 11, 0),
(16, 'Morelos', '18.93301949386411', '-99.20820593833923', 11, 0),
(17, 'Tlaxcala', '19.316104625744448', '-98.25479865074158', 12, 0),
(18, 'Aguascalientes', '21.89090953833516', '-102.30360388755798', 11, 0),
(19, 'Sonora', '29.08086983820096', '-110.97181677818298', 11, 0),
(20, 'Michoacán de Ocampo ', '19.708637161966553', '-101.2005078792572', 12, 0),
(21, 'Nayarit', '21.509715429321197', '-104.89363074302673', 12, 0),
(22, 'Baja California Sur', '24.150610824126357', '-110.3133237361908', 11, 0),
(23, 'Chiapas', '16.7586255946944', '-93.11010718345642', 12, 0),
(24, 'Yucatán', '20.976927902495934', '-89.61096167564392', 11, 0),
(25, 'Guanajuato', '21.131362083064523', '-101.68562293052673', 12, 0),
(26, 'Tabasco', '17.995815370836027', '-92.93432593345642', 11, 0),
(27, 'Baja California', '32.6296350887339', '-115.4768979549408', 11, 0),
(28, 'Campeche', '19.833670749495898', '-90.53346991539001', 12, 0),
(29, 'Chihuahua', '28.638264898090735', '-106.09526038169861', 11, 0),
(30, 'Coahuila de Zaragoza', '25.44120130731177', '-100.99657416343689', 11, 0),
(31, 'Durango', '24.033393152890905', '-104.66051459312439', 12, 0),
(32, 'San Luis PotosÃ­', '22.164613619317688', '-101.01545691490173', 11, 0),
(33, 'Sinaloa', '24.811141676473213', '-107.40846991539001', 12, 0),
(34, 'Guerrero', '17.5564208637123', '-99.5195996761322', 12, 0),
(35, 'Hidalgo', '20.10214317759409', '-98.7721860408783', 12, 0),
(36, 'Zacatecas', '22.77248175488474', '-102.61053442955017', 11, 0),
(37, 'Quintana Roo', '18.50250267139408', '-88.30049872398376', 12, 0),
(38, 'Tamaulipas', '23.73988156280719', '-99.15224432945251', 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `foto`
--

CREATE TABLE `foto` (
  `id_foto` bigint(20) NOT NULL,
  `id_categoria_entrada` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `introduccion` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `archivos` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `fotos` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `likes` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `busqueda_tipo` int(12) NOT NULL DEFAULT '2',
  `fecha_nice` varchar(211) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `foto`
--

INSERT INTO `foto` (`id_foto`, `id_categoria_entrada`, `nombre`, `introduccion`, `contenido`, `imagen`, `archivos`, `fotos`, `extra`, `fecha_alta`, `likes`, `estatus`, `busqueda_tipo`, `fecha_nice`) VALUES
(1, 0, 'Perrito dormido en el coche', '', '', 'https://honeywell-show.info/uploaded/1562010077.jpg', '', 'http://honeywell-show.info/uploaded/1562010077.jpg', 0, '2019-07-01 14:41:26', 3, 0, 2, 'Hace 8 mess'),
(2, 0, 'Perrito en el estadio de beisball', '', '', 'https://honeywell-show.info/uploaded/1562010106.jpg', '', 'http://honeywell-show.info/uploaded/1562010106.jpg', 0, '2019-07-01 14:41:56', 2, 0, 2, 'Hace 8 mess'),
(3, 0, 'Lobito en el bosque', '', '', 'https://honeywell-show.info/uploaded/1562011296.jpg', '', 'http://honeywell-show.info/uploaded/1562011296.jpg', 0, '2019-07-01 15:01:52', 2, 0, 2, 'Hace 8 mess'),
(4, 0, 'Un lobo perdido en la ciudad', '', '', 'https://honeywell-show.info/uploaded/1562011319.jpg', '', 'http://honeywell-show.info/uploaded/1562011319.jpg', 0, '2019-07-01 15:02:08', 2, 0, 2, 'Hace 8 mess'),
(5, 0, 'Pirrito de colores', '', '', 'https://honeywell-show.info/uploaded/1562011334.jpg', '', 'http://honeywell-show.info/uploaded/1562011334.jpg', 0, '2019-07-01 15:02:21', 2, 0, 2, 'Hace 8 mess'),
(6, 0, 'Gato y mariposa', '', '', 'https://honeywell-show.info/uploaded/1562011349.jpg', '', 'http://honeywell-show.info/uploaded/1562011349.jpg', 0, '2019-07-01 15:02:37', 2, 0, 2, 'Hace 8 mess'),
(7, 0, 'Las botas con gato', '', '', 'https://honeywell-show.info/uploaded/1562011369.jpg', '', 'http://honeywell-show.info/uploaded/1562011369.jpg', 0, '2019-07-01 15:03:00', 2, 0, 2, 'Hace 8 mess'),
(8, 0, 'Vuelto loco', '', '', 'https://honeywell-show.info/uploaded/1562011388.jpg', '', 'http://honeywell-show.info/uploaded/1562011388.jpg', 0, '2019-07-01 15:03:15', 2, 0, 2, 'Hace 8 mess'),
(9, 0, 'Gato galan', '', '', 'https://honeywell-show.info/uploaded/1562011400.jpg', '', 'http://honeywell-show.info/uploaded/1562011400.jpg', 0, '2019-07-01 15:03:32', 2, 0, 2, 'Hace 8 mess'),
(10, 0, 'Gato con estilo', '', '', 'https://honeywell-show.info/uploaded/1562011416.jpg', '', 'http://honeywell-show.info/uploaded/1562011416.jpg', 0, '2019-07-01 15:03:51', 2, 0, 2, 'Hace 8 mess'),
(11, 0, 'Gato con lentes de sol', '', '', 'https://honeywell-show.info/uploaded/1562011439.jpg', '', 'http://honeywell-show.info/uploaded/1562011439.jpg', 0, '2019-07-01 15:04:05', 2, 0, 2, 'Hace 8 mess'),
(12, 0, 'El gato volador', '', '', 'https://honeywell-show.info/uploaded/1562011451.jpg', '', 'http://honeywell-show.info/uploaded/1562011451.jpg', 0, '2019-07-01 15:04:24', 2, 0, 2, 'Hace 8 mess'),
(14, 0, 'Perrito Fotografo', '', '', 'https://honeywell-show.info/uploaded/1562025951.jpg', '', 'http://honeywell-show.info/uploaded/1562025951.jpg', 0, '2019-07-01 19:06:53', 2, 0, 2, 'Hace 8 mess'),
(15, 0, 'Cachorro tomando el Sol', '', '', 'https://honeywell-show.info/uploaded/1562026027.jpg', '', 'http://honeywell-show.info/uploaded/1562026027.jpg', 0, '2019-07-01 19:08:05', 2, 0, 2, 'Hace 8 mess'),
(16, 0, 'Perrito en piscina', '', '', 'https://honeywell-show.info/uploaded/1562026135.jpg', '', 'http://honeywell-show.info/uploaded/1562026135.jpg', 0, '2019-07-01 19:09:37', 2, 0, 2, 'Hace 8 mess'),
(17, 0, 'Super Perro', '', '', 'https://honeywell-show.info/uploaded/1562026208.jpg', '', 'http://honeywell-show.info/uploaded/1562026208.jpg', 0, '2019-07-01 19:10:36', 2, 0, 2, 'Hace 8 mess'),
(18, 0, 'Cachorro de Otoño', '', '', 'https://honeywell-show.info/uploaded/1562026245.jpg', '', 'http://honeywell-show.info/uploaded/1562026245.jpg', 0, '2019-07-01 19:11:12', 2, 0, 2, 'Hace 8 mess'),
(19, 0, 'Dalmata Pintor', '', '', 'https://honeywell-show.info/uploaded/1562026282.jpg', '', 'http://honeywell-show.info/uploaded/1562026282.jpg', 0, '2019-07-01 19:11:54', 2, 0, 2, 'Hace 8 mess'),
(20, 0, 'Perrita dentro de Sandalia', '', '', 'https://honeywell-show.info/uploaded/1562026329.jpg', '', 'http://honeywell-show.info/uploaded/1562026329.jpg', 0, '2019-07-01 19:12:55', 2, 0, 2, 'Hace 8 mess'),
(21, 0, 'Perrito  Intelectual', '', '', 'https://honeywell-show.info/uploaded/1562026386.jpg', '', 'http://honeywell-show.info/uploaded/1562026386.jpg', 0, '2019-07-01 19:13:45', 2, 0, 2, 'Hace 8 mess'),
(22, 0, 'Perro Surfing', '', '', 'https://honeywell-show.info/uploaded/1562026445.jpg', '', 'http://honeywell-show.info/uploaded/1562026445.jpg', 0, '2019-07-01 19:14:58', 2, 0, 2, 'Hace 8 mess'),
(36, 0, 'Perro en puesta de sol', '', '', 'https://honeywell-show.info/uploaded/1562076932.jpg', '', 'http://honeywell-show.info/uploaded/1562076932.jpg', 0, '2019-07-02 09:16:07', 2, 0, 2, 'Hace 8 mess'),
(24, 0, 'Perrito en taza de tè', '', '', 'https://honeywell-show.info/uploaded/1562026645.jpg', '', 'http://honeywell-show.info/uploaded/1562026645.jpg', 0, '2019-07-01 19:17:55', 2, 0, 2, 'Hace 8 mess'),
(25, 0, 'Siames descansando', '', '', 'https://honeywell-show.info/uploaded/1562026723.jpg', '', 'http://honeywell-show.info/uploaded/1562026723.jpg', 0, '2019-07-01 19:19:10', 2, 0, 2, 'Hace 8 mess'),
(26, 0, 'Gatito comenzal', '', '', 'https://honeywell-show.info/uploaded/1562026807.jpg', '', 'http://honeywell-show.info/uploaded/1562026807.jpg', 0, '2019-07-01 19:20:44', 2, 0, 2, 'Hace 8 mess'),
(27, 0, 'Gato sediento', '', '', 'https://honeywell-show.info/uploaded/1562026851.jpg', '', 'http://honeywell-show.info/uploaded/1562026851.jpg', 0, '2019-07-01 19:21:13', 2, 0, 2, 'Hace 8 mess'),
(28, 0, 'Gatito hambriento', '', '', 'https://honeywell-show.info/uploaded/1562026922.jpg', '', 'http://honeywell-show.info/uploaded/1562026922.jpg', 0, '2019-07-01 19:23:29', 2, 0, 2, 'Hace 8 mess'),
(29, 0, 'Gatito escondido', '', '', 'https://honeywell-show.info/uploaded/1562027027.jpg', '', 'http://honeywell-show.info/uploaded/1562027027.jpg', 0, '2019-07-01 19:24:16', 2, 0, 2, 'Hace 8 mess'),
(30, 0, 'Gato estudioso', '', '', 'https://honeywell-show.info/uploaded/1562027069.jpg', '', 'http://honeywell-show.info/uploaded/1562027069.jpg', 0, '2019-07-01 19:25:16', 2, 0, 2, 'Hace 8 mess'),
(31, 0, 'Gato Taco', '', '', 'https://honeywell-show.info/uploaded/1562027121.jpg', '', 'http://honeywell-show.info/uploaded/1562027121.jpg', 0, '2019-07-01 19:25:45', 2, 0, 2, 'Hace 8 mess'),
(32, 0, 'Gato en caja', '', '', 'https://honeywell-show.info/uploaded/1562027172.jpg', '', 'http://honeywell-show.info/uploaded/1562027172.jpg', 0, '2019-07-01 19:26:51', 2, 0, 2, 'Hace 8 mess'),
(33, 0, 'gatito tomando leche', '', '', 'https://honeywell-show.info/uploaded/1562027224.jpg', '', 'http://honeywell-show.info/uploaded/1562027224.jpg', 0, '2019-07-01 19:28:28', 2, 0, 2, 'Hace 8 mess'),
(34, 0, 'Gatito con biberon', '', '', 'https://honeywell-show.info/uploaded/1562027314.jpg', '', 'http://honeywell-show.info/uploaded/1562027314.jpg', 0, '2019-07-01 19:28:54', 2, 0, 2, 'Hace 8 mess'),
(35, 0, 'Gato con biberon', '', '', 'https://honeywell-show.info/uploaded/1562027346.jpg', '', 'http://honeywell-show.info/uploaded/1562027346.jpg', 0, '2019-07-01 19:29:41', 2, 0, 2, 'Hace 8 mess'),
(37, 0, 'Perrito sediento', '', '', 'https://honeywell-show.info/uploaded/1562077001.jpg', '', 'http://honeywell-show.info/uploaded/1562077001.jpg', 0, '2019-07-02 09:17:19', 2, 0, 2, 'Hace 8 mess'),
(38, 0, 'Perrito puesta de Sol', '', '', 'https://honeywell-show.info/uploaded/1562077059.jpg', '', 'http://honeywell-show.info/uploaded/1562077059.jpg', 0, '2019-07-02 09:18:08', 2, 0, 2, 'Hace 8 mess'),
(39, 0, 'Perro con sombrero', '', '', 'https://honeywell-show.info/uploaded/1562077103.jpg', '', 'http://honeywell-show.info/uploaded/1562077103.jpg', 0, '2019-07-02 09:19:38', 2, 0, 2, 'Hace 8 mess'),
(40, 0, 'Gatito caritas', '', '', 'https://honeywell-show.info/uploaded/1562077194.jpg', '', 'http://honeywell-show.info/uploaded/1562077194.jpg', 0, '2019-07-02 09:20:29', 2, 0, 2, 'Hace 8 mess'),
(41, 0, 'Gato jugando play station', '', '', 'https://honeywell-show.info/uploaded/1562077241.jpg', '', 'http://honeywell-show.info/uploaded/1562077241.jpg', 0, '2019-07-02 09:21:15', 2, 0, 2, 'Hace 8 mess'),
(42, 0, 'Gato en la nieve', '', '', 'https://honeywell-show.info/uploaded/1562077288.jpg', '', 'http://honeywell-show.info/uploaded/1562077288.jpg', 0, '2019-07-02 09:22:24', 2, 0, 2, 'Hace 8 mess'),
(43, 0, 'Perrito jugando disco', '', '', 'https://honeywell-show.info/uploaded/1562077360.jpg', '', 'http://honeywell-show.info/uploaded/1562077360.jpg', 0, '2019-07-02 09:23:08', 2, 0, 2, 'Hace 8 mess'),
(44, 0, 'Cachorrita fashion', '', '', 'https://honeywell-show.info/uploaded/1562077406.jpg', '', 'http://honeywell-show.info/uploaded/1562077406.jpg', 0, '2019-07-02 09:23:49', 2, 0, 2, 'Hace 8 mess'),
(45, 0, 'Cachorro atrapando disco', '', '', 'https://honeywell-show.info/uploaded/1562077445.jpg', '', 'http://honeywell-show.info/uploaded/1562077445.jpg', 0, '2019-07-02 09:24:29', 2, 0, 2, 'Hace 8 mess'),
(46, 0, 'Gato y su tablet', '', '', 'https://honeywell-show.info/uploaded/1562077486.jpg', '', 'http://honeywell-show.info/uploaded/1562077486.jpg', 0, '2019-07-02 09:25:16', 2, 0, 2, 'Hace 8 mess'),
(47, 0, 'Gatito lentes azules', '', '', 'https://honeywell-show.info/uploaded/1562077620.jpg', '', 'http://honeywell-show.info/uploaded/1562077620.jpg', 0, '2019-07-02 09:27:35', 2, 0, 2, 'Hace 8 mess'),
(48, 0, 'Gato atrapado en la nieve', '', '', 'https://honeywell-show.info/uploaded/1562077673.jpg', '', 'http://honeywell-show.info/uploaded/1562077673.jpg', 0, '2019-07-02 09:28:12', 2, 0, 2, 'Hace 8 mess'),
(49, 0, 'Perrito tomando siesta', '', '', 'https://honeywell-show.info/uploaded/1562077808.jpg', '', 'http://honeywell-show.info/uploaded/1562077808.jpg', 0, '2019-07-02 09:30:31', 2, 0, 2, 'Hace 8 mess'),
(50, 0, 'Perro travieso', '', '', 'https://honeywell-show.info/uploaded/1562077848.jpg', '', 'http://honeywell-show.info/uploaded/1562077848.jpg', 0, '2019-07-02 09:31:05', 2, 0, 2, 'Hace 8 mess'),
(51, 0, 'Cachorrito en el jardin', '', '', 'https://honeywell-show.info/uploaded/1562077877.jpg', '', 'http://honeywell-show.info/uploaded/1562077877.jpg', 0, '2019-07-02 09:32:59', 2, 0, 2, 'Hace 8 mess'),
(52, 0, 'Volando alto', '', '', 'https://honeywell-show.info/uploaded/1562078013.jpg', '', 'http://honeywell-show.info/uploaded/1562078013.jpg', 0, '2019-07-02 09:34:02', 2, 0, 2, 'Hace 8 mess'),
(53, 0, 'Jugando escondidas', '', '', 'https://honeywell-show.info/uploaded/1562078054.jpg', '', 'http://honeywell-show.info/uploaded/1562078054.jpg', 0, '2019-07-02 09:34:32', 2, 0, 2, 'Hace 8 mess'),
(54, 0, 'gatito y bebe', '', '', 'https://honeywell-show.info/uploaded/1562086475.jpg', '', 'http://honeywell-show.info/uploaded/1562086475.jpg', 0, '2019-07-02 11:55:16', 2, 0, 2, 'Hace 8 mess'),
(55, 0, 'gatito en taza', '', '', 'https://honeywell-show.info/uploaded/1562086535.jpg', '', 'http://honeywell-show.info/uploaded/1562086535.jpg', 0, '2019-07-02 11:55:51', 2, 0, 2, 'Hace 8 mess'),
(56, 0, 'gato sentado', '', '', 'https://honeywell-show.info/uploaded/1562086559.jpg', '', 'http://honeywell-show.info/uploaded/1562086559.jpg', 0, '2019-07-02 11:56:17', 2, 0, 2, 'Hace 8 mess'),
(57, 0, 'Esperando a mi dueño', '', '', 'https://honeywell-show.info/uploaded/1562086643.jpg', '', 'http://honeywell-show.info/uploaded/1562086643.jpg', 0, '2019-07-02 11:57:45', 2, 0, 2, 'Hace 8 mess'),
(58, 0, 'Ambos dormidos', '', '', 'https://honeywell-show.info/uploaded/1562086681.jpg', '', 'http://honeywell-show.info/uploaded/1562086681.jpg', 0, '2019-07-02 11:58:39', 2, 0, 2, 'Hace 8 mess'),
(59, 0, 'Ojos turqueza', '', '', 'https://honeywell-show.info/uploaded/1562086741.jpg', '', 'http://honeywell-show.info/uploaded/1562086741.jpg', 0, '2019-07-02 11:59:23', 2, 0, 2, 'Hace 8 mess'),
(60, 0, 'Amamantando gatitos', '', '', 'https://honeywell-show.info/uploaded/1562086782.jpg', '', 'http://honeywell-show.info/uploaded/1562086782.jpg', 0, '2019-07-02 12:00:03', 3, 0, 2, 'Hace 8 mess'),
(61, 0, 'Ojos amarillos', '', '', 'https://honeywell-show.info/uploaded/1562086809.jpg', '', 'http://honeywell-show.info/uploaded/1562086809.jpg', 0, '2019-07-02 12:00:30', 2, 0, 2, 'Hace 8 mess'),
(62, 0, 'Perro y Bebé', '', '', 'https://honeywell-show.info/uploaded/1562086986.jpg', '', 'http://honeywell-show.info/uploaded/1562086986.jpg', 0, '2019-07-02 12:03:40', 3, 0, 2, 'Hace 8 mess'),
(63, 0, 'Cachorro con chupón', '', '', 'https://honeywell-show.info/uploaded/1562087039.jpg', '', 'http://honeywell-show.info/uploaded/1562087039.jpg', 0, '2019-07-02 12:04:26', 2, 0, 2, 'Hace 8 mess'),
(64, 0, 'Alimentando cachorros', '', '', 'https://honeywell-show.info/uploaded/1562087082.jpg', '', 'http://honeywell-show.info/uploaded/1562087082.jpg', 0, '2019-07-02 12:05:00', 3, 0, 2, 'Hace 8 mess'),
(65, 0, 'Cuidando niña', '', '', 'https://honeywell-show.info/uploaded/1562087111.jpg', '', 'http://honeywell-show.info/uploaded/1562087111.jpg', 0, '2019-07-02 12:05:29', 2, 0, 2, 'Hace 8 mess'),
(66, 0, 'Cachorros en canasta', '', '', 'https://honeywell-show.info/uploaded/1562087134.jpg', '', 'http://honeywell-show.info/uploaded/1562087134.jpg', 0, '2019-07-02 12:05:54', 2, 0, 2, 'Hace 8 mess'),
(67, 0, 'En pijama', '', '', 'https://honeywell-show.info/uploaded/1562087163.jpg', '', 'http://honeywell-show.info/uploaded/1562087163.jpg', 0, '2019-07-02 12:06:16', 2, 0, 2, 'Hace 8 mess'),
(68, 0, 'Volando ando', '', '', 'https://honeywell-show.info/uploaded/1562171080.jpg', '', 'http://honeywell-show.info/uploaded/1562171080.jpg', 0, '2019-07-03 11:25:07', 31, 0, 2, 'Hace 8 mess'),
(69, 0, 'Lobito en la nieve', '', '', 'https://honeywell-show.info/uploaded/1562186266.jpg', '', 'http://honeywell-show.info/uploaded/1562186266.jpg', 0, '2019-07-03 15:38:33', 5, 0, 2, 'Hace 8 mess'),
(70, 0, 'Tarde de soccer', '', '', 'https://honeywell-show.info/uploaded/1562186356.jpg', '', 'http://honeywell-show.info/uploaded/1562186356.jpg', 0, '2019-07-03 15:39:36', 29, 0, 2, 'Hace 8 mess'),
(71, 0, 'Gato en equilibrio', '', '', 'https://honeywell-show.info/uploaded/1562186424.jpg', '', 'http://honeywell-show.info/uploaded/1562186424.jpg', 0, '2019-07-03 15:41:35', 10, 0, 2, 'Hace 8 mess'),
(72, 0, 'Gato intelectual', '', '', 'https://honeywell-show.info/uploaded/1562186506.jpg', '', 'http://honeywell-show.info/uploaded/1562186506.jpg', 0, '2019-07-03 15:42:15', 26, 0, 2, 'Hace 8 mess'),
(73, 0, 'Madre e hijo', '', '', 'https://honeywell-show.info/uploaded/1562186582.jpg', '', 'http://honeywell-show.info/uploaded/1562186582.jpg', 0, '2019-07-03 15:43:14', 16, 0, 2, 'Hace 8 mess'),
(74, 0, 'Practicando nataciòn', '', '', 'https://honeywell-show.info/uploaded/1562186601.jpg', '', 'http://honeywell-show.info/uploaded/1562186601.jpg', 0, '2019-07-03 15:43:39', 12, 0, 2, 'Hace 8 mess'),
(75, 0, 'Gato en caja', '', '', 'https://honeywell-show.info/uploaded/1562186658.jpg', '', 'http://honeywell-show.info/uploaded/1562186658.jpg', 0, '2019-07-03 15:44:45', 30, 0, 2, 'Hace 8 mess'),
(76, 0, 'Gato heroico', '', '', 'https://honeywell-show.info/uploaded/1562186692.jpg', '', 'http://honeywell-show.info/uploaded/1562186692.jpg', 0, '2019-07-03 15:45:05', 7, 0, 2, 'Hace 8 mess'),
(77, 0, 'El rey', '', '', 'https://honeywell-show.info/uploaded/1562186715.jpg', '', 'http://honeywell-show.info/uploaded/1562186715.jpg', 0, '2019-07-03 15:45:31', 29, 0, 2, 'Hace 8 mess'),
(78, 0, 'Rescatista', '', '', 'https://honeywell-show.info/uploaded/1562186743.jpg', '', 'http://honeywell-show.info/uploaded/1562186743.jpg', 0, '2019-07-03 15:45:57', 29, 0, 2, 'Hace 8 mess'),
(79, 0, 'Gato dentro la taza de baño', '', '', 'https://honeywell-show.info/uploaded/1562186822.jpg', '', 'http://honeywell-show.info/uploaded/1562186822.jpg', 0, '2019-07-03 15:47:53', 49, 0, 2, 'Hace 8 mess');

-- --------------------------------------------------------

--
-- Table structure for table `frecuencia`
--

CREATE TABLE `frecuencia` (
  `id_frecuencia` int(11) NOT NULL,
  `nombre` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `frecuencia`
--

INSERT INTO `frecuencia` (`id_frecuencia`, `nombre`, `extra`, `estatus`) VALUES
(1, '1 vez al dìa', 0, 0),
(3, '1-2 veces al día', 0, 0),
(4, '2-3 veces al día', 0, 0),
(5, '3-4 veces al día', 0, 0),
(6, '4-5 veces al día', 0, 0),
(7, '2 veces por semana', 0, 0),
(8, '2-3 veces por semana', 0, 0),
(9, '3-4 veces por semana', 0, 0),
(10, '4-5 veces por semana', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gps_test`
--

CREATE TABLE `gps_test` (
  `id_gps` bigint(20) NOT NULL,
  `id_unico` bigint(20) NOT NULL,
  `latitud` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `lontitud` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `datos` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `extra` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `fechahora` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gps_test`
--

INSERT INTO `gps_test` (`id_gps`, `id_unico`, `latitud`, `lontitud`, `datos`, `extra`, `fechahora`) VALUES
(1, 3, '19.234', '19.256', '1,3,5,6,6', 'vo,213;ca,23', '2019-12-19 12:42:12'),
(2, 3, '19.234', '19.256', '1,3,5,6,6', 'vo,213;ca,23', '2019-12-19 12:44:14'),
(3, 3, '19.234', '19.256', '1,3,5,6,6', 'vo,213;ca,23', '2019-12-19 12:45:39'),
(4, 0, '', '', '', '', '2019-12-22 11:50:43'),
(5, 0, '', '', '', '', '2020-01-03 01:32:56'),
(6, 0, '', '', '', '', '2020-01-03 05:24:02'),
(7, 0, '', '', '', '', '2020-01-29 01:09:34'),
(8, 0, '', '', '', '', '2020-03-02 15:06:00'),
(9, 0, '', '', '', '', '2020-03-05 16:05:19');

-- --------------------------------------------------------

--
-- Table structure for table `lugar`
--

CREATE TABLE `lugar` (
  `id_lugar` bigint(20) NOT NULL,
  `id_estado` int(11) NOT NULL,
  `id_municipio` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `latitud` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `longitud` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `zoom` double NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(44) COLLATE utf8_unicode_ci NOT NULL,
  `horario` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `liga_mapa` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `icono` varchar(22) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(22) COLLATE utf8_unicode_ci NOT NULL,
  `garantia` int(11) NOT NULL,
  `busqueda_tipo` int(11) NOT NULL DEFAULT '3',
  `apimap` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lugar`
--

INSERT INTO `lugar` (`id_lugar`, `id_estado`, `id_municipio`, `id_tipo`, `id_categoria`, `latitud`, `longitud`, `zoom`, `nombre`, `web`, `direccion`, `telefono`, `horario`, `liga_mapa`, `icono`, `color`, `garantia`, `busqueda_tipo`, `apimap`) VALUES
(5, 1, 0, 3, 1, '19.39583889454184', '-99.28847908973694', 17, 'Plaza Parque Interlomas', 'https://parqueinterlomas.com/', 'Av. JesÃºs del Monte No. 41, Col. JesÃºs del Monte. Naucalpan de JuÃ¡rez.', '55 5247 3838', '10:00â€“23:00', '', '', '', 0, 3, 0),
(3, 1, 4, 3, 2, '19.441673232380918', '-99.2041826248169', 17, 'Cinépolis VIP Plaza Carso', 'https://www.cinepolis.com/', 'Calle Lago Zurich 245, Amp Granada, 11529 Ciudad de Mexico, CDMX, Mexico', '55 2122 6060', '14:00 a 24:00 horas', '', '', '', 0, 3, 0),
(6, 1, 4, 3, 1, '19.43972315222894', '-99.19985890388489', 17, 'Miyana', 'http://miyanacomercial.com.mx/', 'Av. EjÃ©rcito Nacional 769, Granada, 11570 Ciudad de MÃ©xico, CDMX', '5555451695 ', ' ', '', '', '', 1, 3, 0),
(7, 1, 7, 3, 1, '19.377409440570677', '-99.2541253566742', 16, 'Patio Santa Fe', 'https://www.facebook.com/patiosantafemx/?rf=145824538957108', 'ProlongaciÃ³n Paseo de la Reforma No. 400, Col. ProlongaciÃ³n Reforma. Lomas de Santa Fe.', '55 5292 5007', ' 11:00am-20:00pm', '', '', '', 0, 3, 0),
(8, 1, 6, 3, 1, '19.454534069083238', '-99.21878457069397', 16, 'Toreo Parque Central', 'https://www.facebook.com/pg/toreoparquecentral/about/?ref=page_internal', 'Boulevard Manuel Ãvila Camacho No 5. Fraccionamiento Lomas de Sotelo Naucalpan de JuÃ¡rez, Estado de MÃ©xico.', '55 6706 3163', ' 11:00am-21:00pm', '', '', '', 0, 3, 0),
(9, 1, 4, 3, 1, '19.43958151125345', '-99.20307755470276', 16, 'Antara Fashion Hall, Polanco', 'http://www.antara.com.mx/', 'Av EjÃ©rcito Nacional 843, Granada, 11520 Ciudad de MÃ©xico, CDMX', '5545938870', ' 11:00am-23:00pm', '', '', '', 0, 3, 0),
(10, 11, 0, 3, 1, '19.52543370771536', '-99.22768950462341', 16, 'Plaza Mundo E', 'https://www.mundoe.com.mx/', 'Perif. Blvd. Manuel Ãvila Camacho 1007, Hab Jardines de Santa Monica, 54050 Tlalnepantla de Baz, MÃ©x., MÃ©xico', '55 4428 0250', ' ', '', '', '', 0, 3, 0),
(11, 1, 4, 3, 1, '19.387980571390102', '-99.24788653850555', 18, 'Plaza Lilas Lomas', 'https://twitter.com/plilas?lang=es', 'Calle Paseo de Las Lilas 92, Bosques de las Lomas, 11910 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 5570 1076', ' 06:00amâ€“23:00pm', '', '', '', 0, 3, 0),
(12, 11, 10, 3, 1, '19.601052661731554', '-99.19970870018005', 16, 'Premium Outlets Punta Norte', 'https://www.premiumoutlets.com.mx/puntanorte', 'Hda. de Sierra Vieja 2, Hacienda del Parque, 54769 CuautitlÃ¡n Izcalli, MÃ©x., MÃ©xico', '5558862585', ' 11:00am-21:00pm', '', '', '', 0, 3, 0),
(13, 11, 0, 3, 1, '19.495565584188157', '-99.24674391746521', 17, 'Gran Terraza', 'http://granterrazalomasverdes.com/', 'Colina de la Paz #25, Fracc. Bulevares, Naucalpan Edo de Mex.', '55 1663 0252', ' 11:00am-21:00pm', '', '', '', 0, 3, 0),
(14, 1, 11, 3, 1, '19.365668558314212', '-99.26487565040588', 17, 'Garden Santa Fe', 'www.gardensantafe.com.mx', 'Calle Guillermo Gonzalez Camarena 1205, Santa Fe, Zedec Sta FÃ©, 01219 Ciudad de MÃ©xico, CDMX', ' 55 5292 1731', ' ', '', '', '', 0, 3, 0),
(15, 14, 13, 3, 1, '25.578446647956874', '-100.24595260620117', 17, 'Esfera City Hall', 'https://www.facebook.com/EsferaFashion/', 'Av. la Rioja 245, Residencial la Rioja, 64985 Monterrey, N.L', '81 2134 0200', ' 11:00am-21:00pm\r\n', '', '', '', 0, 3, 0),
(16, 14, 0, 3, 1, '25.65316635936579', '-100.27530670166016', 19, 'Nuevo Sur', 'https://nuevosur.com/', 'Av. RevoluciÃ³n 2703, Ladrillera, 64830 Monterrey, N.L.', ' 81 2222 2291', ' 24hrs', '', '', '', 0, 3, 0),
(17, 14, 0, 3, 1, '25.64935822126469', '-100.33776998519897', 18, 'Plaza San Agustín (ciertas áreas)', 'https://plazafiestamall.com/', 'Real San AgustÃ­n # 222. Col. Residencial San AgustÃ­n San Pedro Garza GarcÃ­a, N.L.. MÃ©xico', '81 12 53 44 40', ' 10:00am-20.00pm', '', '', '', 0, 3, 0),
(18, 14, 0, 3, 1, '25.57639503079409', '-100.24795889854431', 19, 'Pueblo Serena', 'https://www.puebloserena.com/', 'Carr Nacional 500, Sin Nombre de Col 50, 64989 Monterrey, N.L.', '81 9688 6750', '24hrs', '', '', '', 0, 3, 0),
(19, 9, 12, 3, 1, '20.71047915299651', '-103.4128475189209', 17, 'Plaza Andares', 'https://andares.com/', 'Blvrd Puerta de Hierro 2085, Puerta de Hierro, 45116 Gadalajara, Jal.', '33 3648 2280', ' 11:00 am-10:00 pm', '', '', '', 0, 3, 0),
(20, 1, 1, 3, 2, '19.409075237691674', '-99.16061013936996', 19, 'Cine Tonalá', 'http://cinetonala.mx/', 'TonalÃ¡ 261, Roma Sur, 06760 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 5264 4101', ' ', '', '', '', 0, 3, 0),
(21, 1, 4, 3, 2, '19.439642214543802', '-99.19999301433563', 18, 'Cinépolis VIP Miyana', 'https://www.cinepolis.com/vip/cartelera/cdmx-poniente/cinepolis-vip-miyana-polanco', 'Av EjÃ©rcito Nacional 769, Granada, 11520 Ciudad de MÃ©xico, CDMX, MÃ©xico', '5552 54 0031', ' ', '', '', '', 0, 3, 0),
(22, 1, 11, 3, 2, '19.368037045771608', '-99.25865292549133', 16, 'Cinépolis VIP Samara', 'https://www.cinepolis.com', 'Antonio Dovali Jaime 70, Santa Fe, Zedec Sta FÃ©, 01210 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 2122 6060', ' ', '', '', '', 0, 3, 0),
(23, 1, 4, 3, 2, '19.44160747130479', '-99.20409344136715', 17, 'Cinépolis VIP Plaza Carso', 'https://www.cinepolis.com', 'Calle Lago Zurich 245, Amp Granada, 11529 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 2122 6060', ' ', '', '', '', 0, 3, 0),
(24, 1, 7, 3, 2, '19.36096184648627', '-99.2744243144989', 16, 'Cinemex Santa Fe', 'https://cinemex.com/', 'Vasco de Quiroga 3800, Lomas de Santa Fe, Cuajimalpa, 01219 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 5257 6969', ' ', '', '', '', 0, 3, 0),
(25, 2, 0, 3, 2, '19.49622803170002', '-99.24737691879272', 18, 'Cinemex Gran Terraza Lomas Verdes', 'https://cinemex.com/', 'Gran Terraza Lomas Verdes, Colina de La Paz 25, Boulevares, 53140 Naucalpan de JuÃ¡rez, MÃ©x., MÃ©xico', '55 5257 6969', ' ', '', '', '', 0, 3, 0),
(26, 1, 3, 3, 2, '19.360749282100254', '-99.16488289833069', 17, 'Cineteca Nacional ', 'cinetecanacional.net', 'Av. MÃ©xico CoyoacÃ¡n 389, Xoco, 03330 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 4155 1200', ' ', '', '', '', 0, 3, 0),
(27, 1, 19, 3, 2, '19.488136884049236', '-99.1524589061737', 16, 'Cinépolis Vía Vallejo', 'https://www.cinepolis.com', 'Calzada Vallejo No. 1090 Int, Nueva Industrial Vallejo, 02300 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 2122 6060', ' ', '', '', '', 0, 3, 0),
(28, 1, 20, 3, 2, '19.305270636032244', '-99.19020295143127', 16, 'Cinépolis Perisur', 'https://www.cinepolis.com', 'Anillo Perif. Blvd. Adolfo LÃ³pez Mateos 4690, Insurgentes Cuicuilco, 04500 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 2122 6060', ' ', '', '', '', 0, 3, 0),
(29, 1, 3, 3, 2, '19.373143347147035', '-99.17294561862946', 19, 'Cinemex Platino Felix Cuevas', 'https://cinemex.com/', 'FÃ©lix Cuevas 374, Col del Valle Centro, 03100 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 5335 0383', ' ', '', '', '', 0, 3, 0),
(30, 1, 21, 3, 2, '19.339461074225518', '-99.19379711151123', 17, 'Cinemex Plaza Loreto', 'https://cinemex.com/', 'Altamirano No. 46, TizapÃ¡n San Ãngel, TizapÃ¡n, 01000 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 5257 6969', ' ', '', '', '', 0, 3, 0),
(31, 1, 4, 3, 2, '19.437982983106192', '-99.20713305473328', 17, 'Cinemex Pabellón Polanco', 'https://cinemex.com/', 'Av EjÃ©rcito Nacional 980, Polanco, Polanco I Secc, 11510 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 5257 6969', ' ', '', '', '', 0, 3, 0),
(32, 1, 0, 3, 2, '19.39924927860235', '-99.28302347660065', 18, 'Cinemex Interlomas', 'https://cinemex.com/', '13, Blvrd. Interlomas 5, Bosque de las Palmas, 52787 Naucalpan de JuÃ¡rez, MÃ©x., MÃ©xico', '55 2122 6060', ' ', '', '', '', 0, 3, 0),
(33, 1, 0, 3, 2, '19.416271667723002', '-99.17204976081848', 21, 'Cineclub Condesa', 'https://www.facebook.com/CineclubCondesadf/', 'Av. Veracruz #102 Col. Condesa, CDMX', '55 5256 3107', ' ', '', '', '', 0, 3, 0),
(34, 1, 0, 3, 2, '19.335743198172835', '-99.19867739081383', 20, 'The Movie Company Escenaria', 'http://themoviecompany.mx/', 'Av. San JerÃ³nimo 263, TizapÃ¡n San Ãngel, TizapÃ¡n, 01090 Ciudad de MÃ©xico, CDMX, MÃ©xico', '55 5550 0859', ' ', '', '', '', 0, 3, 0),
(35, 1, 7, 3, 2, '19.31418088559723', '-99.22023296356201', 17, 'Artz Pedregal', 'https://artzpedregal.mx/', 'Periferico Sur # 3720, Jardines del Pedregal, 01900 Ciudad de MÃ©xico, CDMX', '55 4172 8309', ' ', '', '', '', 0, 3, 0),
(36, 1, 4, 3, 2, '19.44430365357838', '-99.20258939266205', 19, 'Autocinema Coyote Polanco', 'https://www.autocinemacoyote.com/', 'LAGO ZURICH 200 GRANADA 11529, CDMX', '6552 5054', ' ', '', '', '', 0, 3, 0),
(37, 1, 21, 3, 2, '19.35999265185833', '-99.18392390012741', 19, 'Autocinema Coyote Insurgentes Sur', 'https://www.autocinemacoyote.com/', 'AV DE LOS INSURGENTES SUR 1729 GUADALUPE INN 01020, CDMX', '6552 5054', ' ', '', '', '', 0, 3, 0),
(38, 9, 12, 3, 2, '20.712165112972585', '-103.41114699840546', 18, 'Cinépolis VIP Andares', 'https://www.cinepolis.com/', 'Blvrd Puerta de Hierro 2085, Puerta de Hierro, 45116 Gadalajara, Jal.', '55 2122 6060', ' ', '', '', '', 0, 3, 0),
(39, 9, 12, 3, 2, '20.67336321430796', '-103.40526223182678', 17, 'Cinépolis La Gran Plaza', 'https://www.cinepolis.com/', 'Av. Ignacio L. Vallarta 3959, Don Bosco Vallarta, 45049 Guadalajara, Jal.', '55 2122 6060', ' ', '', '', '', 0, 3, 0),
(40, 9, 22, 3, 2, '20.674045795933086', '-103.38100969791412', 19, 'Cinépolis Centro Magno', 'https://www.cinepolis.com/', 'Av. Ignacio L. Vallarta 2425, Arcos Vallarta, 44130 Guadalajara, Jal. ', '55 2122 6060', ' ', '', '', '', 0, 3, 0),
(41, 9, 22, 3, 2, '20.675631782566867', '-103.39319229125977', 17, 'Cinemex Sania', 'https://m.cinemex.com/cine/321/cinemex-sania', 'Av. Ignacio L. Vallarta 3300, Vallarta Nte., 44690 Guadalajara, Jal.', '55 5257 6969', ' ', '', '', '', 0, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `marca`
--

CREATE TABLE `marca` (
  `id_marca` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `marca`
--

INSERT INTO `marca` (`id_marca`, `nombre`, `descripcion`, `website`, `orden`) VALUES
(8, 'Blue Buffalo', 'Sabemos que las mascotas son miembros de la familia. Es por eso que los alimentos de BLUE se preparan con los ingredientes naturales con los que nos da gusto alimentar a nuestra familia.', 'https://bluebuffalo.com/es/para-perros/', 2),
(6, 'Beneful', 'Beneful es una marca de productos alimenticios para perros de Nestlé Purina Petcare global que incluye alimentos húmedos para perros, alimentos secos para perros y golosinas para perros. A partir de 2', 'https://www.purinalatam.com/mx/beneful/', 0),
(9, 'Best Choice', 'Sin descripción', '', 0),
(10, 'Campeón', 'Somos una compañía dedicada a enriquecer la vida de las mascotas y sus dueños', 'https://www.purina-latam.com/mx/purina', 0),
(11, 'Choice Nutrition', 'Sin descripción ', '', 0),
(12, 'Chop!', 'Chop! Llega al mercado en febrero del 2012, siendo una marca de croquetas 100% mexicana y con la calidad de la materia prima que nos proporciona nuestra empresa madre: PROAN.\r\nTeniendo claro que querí', 'https://www.croquetaschop.com/', 0),
(13, 'Criador', 'NUESTRA LÍNEA DE PRODUCTOS PREMIUM TE OFRECE ALIMENTO ADICIONADO CON\r\nPROBIÓTICOS, PREBIÓTICOS, ARÁNDANO Y UN ALTO NIVEL DE PROTEÍNA PARA LOS MÁS EXIGENTES DEL HOGAR', 'https://www.cedacan.com/', 0),
(14, 'Diamond', 'Más de 40 años siendo especialistas en alimentos Super Premium para mascotas.Como hecho en casa: proceso de elaboración con cariño y dedicación para lograr alimentos para perros y gatos de la más alta', 'http://www.diamondpet.mx/new/', 0),
(15, 'Dog Chow', '-', 'https://www.purina-latam.com/mx/dogchow', 0),
(16, 'Eukanuba', '-', 'https://www.eukanuba.com.mx/3-productos-eukanuba', 0),
(17, 'FullTrust', '-', 'https://fulltrust.com.mx/', 0),
(18, 'Ganador', '-', 'https://www.ganador.com.mx/', 0),
(19, 'Golden Dog Nufit', '-', '', 0),
(20, 'Hi Multipro', '-', '', 0),
(21, 'Hills SC', '-', 'https://www.hillspet.com.mx/', 0),
(22, 'Hills PR', '-', 'https://www.hillspet.com.mx/dog-food', 0),
(23, 'Hills IB', '-', 'https://www.hillspet.com.mx/dog-food', 0),
(24, 'Kirkland Signature', '-', 'https://www.costco.com.mx/Jardin-Flores-y-Mascotas/Mascotas/c/cos_9.10?q=%3Aprice-desc%3Abrand%3AKirkland%2BSignature&text=', 0),
(25, 'Hoger Hond', '-', 'http://www.hogerhondmexico.com.mx/', 0),
(26, 'IAMS', '-', 'https://www.iams.mx/', 0),
(27, 'Instinct', '-', 'http://www.instinctpetfood.mx/products/4-croquetas/mascotas', 0),
(28, 'L H &S', '-', '', 0),
(29, 'Lideratto', '-', '', 0),
(30, 'Mainstay Purina', '-', 'https://www.purina-latam.com/mx/purina/', 0),
(31, 'Maxi Bobby', '-', '', 0),
(32, 'MIRA', '-', '', 0),
(33, 'Members Marks', '-', 'https://www.sams.com.mx/search/Ntt=croquetas', 0),
(34, 'Merrick', '-', '', 0),
(35, 'Natural Gourmet', '-', 'http://grandpet.com/es/natural-gourmet/', 0),
(36, 'NUCAN', '-', '', 0),
(37, 'North Paw', '-', 'https://northpaw.com.mx/', 0),
(38, 'Nupec', '-', 'http://nupec.com/', 0),
(39, 'NutresCan', '-', 'http://grandpet.com/es/nutrescan/', 0),
(40, 'Pal Perro', '-', '', 0),
(41, 'Pedigree', '-', 'https://pedigree.com.mx/productos', 0),
(42, 'Purina One', '-', 'https://www.purina-latam.com/mx/one/productos', 0),
(43, 'Prairie', '-', 'http://www.prairiepetfood.mx/productos/1-croquetas-prairie-para-perros', 0),
(44, 'Pro Plan', '-', 'https://www.purina-latam.com/mx/proplan/', 0),
(45, 'Canidar Pure', '-', '', 0),
(46, 'Royal Canin', '-', 'https://www.royalcanin.com.mx/', 0),
(47, 'Tier Holistic', '-', 'https://www.facebook.com/bln.mx/', 0),
(48, 'The Honest Kitchen', '-', 'https://www.thehonestkitchen.com/dog-food', 0),
(49, 'Whole Earth Farms', '-', 'https://www.wholeearthfarms.com.ar/productos', 0),
(50, 'Whole Hearted', '-', '', 0),
(51, 'X-Dog', '-', 'https://www.cedacan.com/', 0),
(52, 'Zoopet', '-', '', 0),
(53, 'Top Choice', '-', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mascota`
--

CREATE TABLE `mascota` (
  `id_mascota` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `icono` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `sexo` int(11) NOT NULL,
  `peso` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `edad` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `altura` float NOT NULL,
  `raza` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `dia` int(20) NOT NULL,
  `mes` int(20) NOT NULL,
  `anio` int(20) NOT NULL,
  `color_ojos` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `color_pelo` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `estilo_pelo` int(11) NOT NULL,
  `tipo_pelo` int(11) NOT NULL,
  `rasgos_a` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `rasgos_b` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `rasgos_c` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `discapacidades` int(11) NOT NULL,
  `discapacidades_cuales` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entrenamiento` int(20) NOT NULL,
  `personalidad` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_sangre` int(10) NOT NULL,
  `esterilizado` int(10) NOT NULL,
  `chip` int(11) NOT NULL,
  `chip_numero` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alergias` int(11) NOT NULL,
  `alergias_cual` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `enfermedades` int(11) NOT NULL,
  `enfermedades_cual` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `ejercicio` int(11) NOT NULL,
  `ejercicio_horas` int(11) NOT NULL,
  `ejercicio_minutos` int(11) NOT NULL,
  `cirugia` int(11) NOT NULL,
  `cirugia_cual` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `dueno_nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `dueno_apellido` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `dueno_fecha` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `dueno_email` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `dueno_telefono` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `dueno_fecha_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mascota`
--

INSERT INTO `mascota` (`id_mascota`, `id_usuario`, `tipo`, `icono`, `foto`, `color`, `nombre`, `sexo`, `peso`, `edad`, `altura`, `raza`, `dia`, `mes`, `anio`, `color_ojos`, `color_pelo`, `estilo_pelo`, `tipo_pelo`, `rasgos_a`, `rasgos_b`, `rasgos_c`, `discapacidades`, `discapacidades_cuales`, `entrenamiento`, `personalidad`, `tipo_sangre`, `esterilizado`, `chip`, `chip_numero`, `alergias`, `alergias_cual`, `enfermedades`, `enfermedades_cual`, `ejercicio`, `ejercicio_horas`, `ejercicio_minutos`, `cirugia`, `cirugia_cual`, `dueno_nombre`, `dueno_apellido`, `dueno_fecha`, `dueno_email`, `dueno_telefono`, `dueno_fecha_date`) VALUES
(1, 2, 1, '', '', '', 'blacky', 0, '45kg', '344 años', 56, 'Callejero Fino', 0, 0, 0, 'negros', 'negro con blanco', 3, 2, '', '2', '3', 0, 'ninguna discapacidad', 1, 'Intenso ', 0, 1, 0, 'No', 0, 'alergia x', 0, 'pereza', 0, 0, 0, 1, 'perforaciones en las orejas', '', '', '', '', '', '2019-11-29'),
(2, 2, 1, '', '', '', 'michi', 0, '3900', '67', 90, 'pug', 0, 0, 0, '', '', 0, 0, '', '2', '3', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(3, 2, 1, '', '', '', 'Babsy', 0, '12 kg', '14 años', 17, 'Labradora', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(4, 2, 1, '', '', '', 'PErrito fernandez', 0, '33 kg', '11 años', 12, 'RAsota', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(5, 2, 1, '', '', '', 'Nombre', 0, 'kg', 'años', 0, 'ej. Labrador', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(6, 2, 1, '', '', '', 'Martina', 0, '2.5 kg', '3 años', 20, 'Chihuhua', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(7, 2, 1, '', '', '', 'Perrazo', 0, '12 kg', '3 años', 4, 'ej. Labrador', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(8, 7, 1, '', '', '', 'Nombre', 0, '4 kg', '2 años', 0, 'maltés', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(9, 9, 1, '', '', '', 'Martina Fey', 0, 'kg', 'años', 0, 'Chihuahua', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(10, 18, 1, '', '', '', 'Kahla', 0, '50 kg', '10 años', 100, 'Husky', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(11, 2, 1, '', '', '', 'Nombre', 0, 'kg', 'años', 0, 'ej. Labrador', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(12, 2, 1, '', '', '', 'Nombre', 0, 'kg', 'años', 0, 'ej. Labrador', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(13, 2, 1, '', '', '', 'riki', 0, '23 kg', '7 años', 22, 'ej. Labrador', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(14, 11, 1, '', '', '', 'Gatillo ', 0, '33  kg', '33  años', 33, 'ej. Labrador', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(39, 4, 0, '', '', '', 'greco', 0, '3 kg', '4', 30, 'maltés', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(15, 23, 1, '', '', '', 'galga', 1, '2.800 kg', '1.3 años', 50, 'galgo italiano', 0, 0, 0, 'café', 'gris con mancha blan', 0, 0, '', '', '', 0, '', 0, '', 0, 1, 0, '', 0, '', 0, '', 0, 0, 0, 1, 'pata rota izquierda', '', '', '', '', '', '0000-00-00'),
(16, 2, 1, '', '', '', 'sdd', 0, 'ds kg', '23 años', 43, 'adasdsad', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(17, 4, 1, '', '', '', 'gdfgfdg', 0, '3 kg', '21', 34, 'Mordedor', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(18, 2, 1, '', '', '', 'greci', 0, '5 kg', '4 años', 40, 'Maltes', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(21, 1, 0, '', '', '', 'dime', 0, '', '', 0, '', 10, 10, 1879, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(22, 1, 0, '', '', '', 'dime', 0, '', '', 0, '', 10, 10, 1879, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(23, 1, 0, '', '', '', 'dime', 0, '', '', 0, '', 10, 10, 1879, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(24, 1, 0, '', '', '', 'dime', 0, '', '', 0, '', 10, 10, 1879, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(25, 1, 0, '', '', '', 'dime', 0, '', '', 0, '', 10, 10, 1879, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(26, 1, 0, '', '', '', 'dime', 0, '', '', 0, '', 10, 10, 1879, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(27, 1, 0, '', '', '', 'dime', 0, '', '', 0, '', 10, 10, 1879, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(28, 1, 0, '', '', '', 'Greco', 0, '10', '4', 45, 'Maltés', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(29, 1, 0, '', '', '', 'Greco', 0, '10', '4', 45, 'Maltés', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(30, 1, 0, '', '', '', 'Benny', 0, '10', '1 año y medio', 45, 'Pug', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(31, 1, 0, '', '', '', 'Benny', 0, '10', '1 y medio', 45, 'Pug', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(32, 1, 0, '', '', '', 'Pug', 0, '10', '1 y medio', 45, 'Pug', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(33, 1, 0, '', '', '', 'Hola', 0, '10', '1', 40, 'Pug', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 1, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(34, 1, 0, '', '', '', 'Greco', 0, '4', '4', 45, 'Macho', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(35, 4, 0, '', '', '', 'h', 0, 'b', 'j', 0, 'b', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(37, 4, 0, '', '', '', 'Macarena', 0, '3 kg', '.8', 30, 'frensh ', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(38, 4, 0, '', '', '', 'Benny', 0, '4', '1 año y medio', 90, 'pug', 0, 0, 0, 'cafe', 'negro', 0, 0, '', '2', '3', 0, 'ninguna', 0, 'un amor, latoso', 0, 0, 0, '', 0, 'ninguna', 0, 'ninguna', 0, 0, 0, 0, 'ninguna', '', '', '', '', '', '0000-00-00'),
(95, 39, 0, '', '', '', '', 0, '', '', 0, '', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(94, 39, 0, '', '', '', '', 0, '', '', 0, '', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(93, 39, 0, '', '', '', '', 0, '', '', 0, '', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(92, 39, 0, '', '', '', '', 0, '', '', 0, '', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(91, 68, 1, '', '', '', 'Bongo', 0, '7 kg', '0 años', 34, 'Cruza Schnauzer', 0, 0, 0, 'negros', 'negro', 0, 1, '', '', '', 0, '', 0, '', 0, 1, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(90, 69, 1, '', '', '', 'Chloe', 0, '2 kg', '2 años', 30, 'chihuahua', 0, 0, 0, 'miel', 'blanca', 1, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(89, 69, 1, '', '', '', 'Chlor', 0, '2 kg', '2años', 30, 'chihuahua', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(88, 66, 1, '', '', '', 'rio', 0, '10 kg', '5 años', 2, 'labrador', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(80, 2, 0, '', '', '', 'mascotaZ', 0, '32', '2', 23, '', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(81, 2, 0, '', '', '', 'Grecondio', 1, '3', '4', 50, 'Maltes', 0, 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(87, 66, 1, '', '', '', 'pepe', 0, '10 kg', '3 años', 4, 'labrador', 0, 0, 0, 'verdes', 'negro', 0, 0, '', '', '', 0, '', 0, '', 0, 0, 0, '1021', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(84, 39, 0, '', '', '', 'krónos ', 1, '3 kilos', '47 días ', 0, 'chow chow', 0, 0, 0, '', '', 0, 0, '', '2', '3', 0, '', 0, '', 0, 0, 0, '', 0, '', 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '0000-00-00'),
(85, 2, 1, '', '', '', 'perro', 0, '1 kg', '2 años', 3, 'la', 0, 0, 0, '', '', 0, 0, '', '', '', 1, 'gcch', 0, '', 6, 0, 0, '', 1, '', 1, '', 0, 0, 0, 1, '', '', '', '', '', '', '0000-00-00'),
(86, 49, 1, '', '', '', 'Cuco', 0, '4 kg', '4 años', 34, 'labrador', 0, 0, 0, 'azulea', 'cafeaoso', 1, 1, 'muy chiquita', '', '', 0, '', 0, 'rara', 2, 0, 0, '', 1, 'a los gatos', 1, 'dislexia', 0, 0, 0, 0, 'no', '', '', '', '', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `mascota_alimento`
--

CREATE TABLE `mascota_alimento` (
  `id_mascota_alimento` int(11) NOT NULL,
  `id_mascota` int(11) NOT NULL,
  `id_frecuencia` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `frecuencia_texto` varchar(33) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mascota_alimento`
--

INSERT INTO `mascota_alimento` (`id_mascota_alimento`, `id_mascota`, `id_frecuencia`, `nombre`, `frecuencia_texto`) VALUES
(1, 1, 1, 'Verdura hervidas', '1-2 veces por semana'),
(8, 0, 0, '', ''),
(7, 0, 0, '', ''),
(6, 0, 0, '', ''),
(5, 1, 0, 'pastasss', '2 veces por semana'),
(9, 0, 0, '', ''),
(10, 0, 0, '', ''),
(11, 0, 0, '', ''),
(12, 0, 0, '', ''),
(13, 12, 0, 'pet', 'peth'),
(14, 0, 0, '', ''),
(15, 0, 0, '', ''),
(16, 0, 0, '', ''),
(17, 1, 0, 'carne', '1 vez al día'),
(18, 0, 0, '', ''),
(19, 1, 0, 'Arroz', '1 vez al día'),
(20, 0, 0, '', ''),
(21, 1, 0, '', ''),
(22, 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mascota_foto`
--

CREATE TABLE `mascota_foto` (
  `id_foto` bigint(20) NOT NULL,
  `id_categoria_entrada` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `introduccion` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `archivos` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `fotos` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `likes` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `busqueda_tipo` int(12) NOT NULL DEFAULT '2',
  `fecha_nice` varchar(211) COLLATE utf8_unicode_ci NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_mascota` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mascota_foto`
--

INSERT INTO `mascota_foto` (`id_foto`, `id_categoria_entrada`, `nombre`, `introduccion`, `contenido`, `imagen`, `archivos`, `fotos`, `extra`, `fecha_alta`, `likes`, `estatus`, `busqueda_tipo`, `fecha_nice`, `id_usuario`, `id_mascota`) VALUES
(1, 0, 'Radiografia', '', '', 'http://honeywell-show.info/uploaded/1564104793.jpg', '', 'http://honeywell-show.info/uploaded/1564104793.jpg', 0, '2019-07-25 00:00:00', 0, 0, 2, '', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mascota_producto`
--

CREATE TABLE `mascota_producto` (
  `id_mascota_producto` int(11) NOT NULL,
  `id_mascota` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `frecuencia` int(11) NOT NULL,
  `frecuencia_texto` varchar(22) COLLATE utf8_unicode_ci NOT NULL,
  `producto_texto` varchar(22) COLLATE utf8_unicode_ci NOT NULL,
  `marca_texto` varchar(111) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mascota_producto`
--

INSERT INTO `mascota_producto` (`id_mascota_producto`, `id_mascota`, `id_producto`, `frecuencia`, `frecuencia_texto`, `producto_texto`, `marca_texto`) VALUES
(1, 1, 6, 1, '1-2 veces al dia', 'Natural cachorro raza ', 'Diamond'),
(2, 1, 6, 0, '1 vez al dia', 'CACHORROS MINIS Pavo c', 'Beneful'),
(3, 1, 6, 0, '1 vez al dia', 'Cachorro', 'Best Choice'),
(4, 1, 6, 1, '1-2 veces al dia', 'Premium Cachorro ', 'Criador'),
(5, 15, 6, 1, '1-2 veces al dia', 'BLUE Life Protection F', 'Blue Buffalo'),
(6, 1, 9, 3, '3-4 veces al dia', 'Adulto', 'Campeón'),
(7, 1, 6, 3, '3-4 veces al dia', 'Premium Cachorro ', 'Criador'),
(8, 1, 6, 0, '1 vez al dia', 'CACHORROS Pollo y camo', 'Beneful'),
(9, 15, 53, 0, '1 vez al dia', 'Cachorro y Adulto', 'Top Choice'),
(10, 0, 0, 0, '', '', ''),
(11, 0, 0, 0, '', '', ''),
(12, 0, 0, 0, '', '', ''),
(13, 0, 0, 0, '', '', ''),
(14, 0, 0, 0, '', '', ''),
(15, 0, 0, 0, '', '', ''),
(16, 0, 0, 0, '', '', ''),
(17, 12, 0, 0, 'pet', 'pet', 'pet'),
(18, 0, 0, 0, '', '', ''),
(19, 1, 0, 0, 'ew', 'ew', '32'),
(20, 1, 0, 0, 'diarios', '1', 'dognchow'),
(21, 1, 0, 0, 'diarios', '1', 'dognchow'),
(22, 1, 0, 0, 'dog chow', 'dog chow', 'dog chow'),
(23, 1, 0, 0, 'dog chow', 'dog chow', 'dog chow'),
(24, 1, 0, 0, '2-3 veces al día', 'hola', 'nuevo'),
(25, 0, 0, 0, '', '', ''),
(26, 0, 0, 0, '', '', ''),
(27, 84, 0, 0, '3-4 veces al día', '', 'Ken-L'),
(28, 85, 40, 4, '4-5 veces al dia', 'Cachorro', 'North Paw'),
(29, 90, 53, 1, '1-2 veces al dia', 'Cachorro ', 'Nupec'),
(30, 0, 0, 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mascota_servicio`
--

CREATE TABLE `mascota_servicio` (
  `id_mascota_servicio` int(11) NOT NULL,
  `id_mascota` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `id_frecuencia` int(11) NOT NULL,
  `frecuencia_texto` varchar(22) COLLATE utf8_unicode_ci NOT NULL,
  `servicio_texto` varchar(33) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mascota_servicio`
--

INSERT INTO `mascota_servicio` (`id_mascota_servicio`, `id_mascota`, `id_servicio`, `id_frecuencia`, `frecuencia_texto`, `servicio_texto`) VALUES
(10, 0, 0, 0, '', ''),
(11, 0, 0, 0, '', ''),
(7, 1, 2, 0, '1 vez por mes', 'Baño y corte'),
(9, 0, 0, 0, '', ''),
(8, 15, 0, 0, '1 vez por semana', 'Baño completo'),
(6, 1, 4, 0, '1 vez por mes', 'Corte  de uñas y cuidado cojinete'),
(12, 0, 0, 0, '', ''),
(13, 0, 0, 0, '', ''),
(14, 0, 0, 0, '', ''),
(15, 0, 0, 0, '', ''),
(16, 0, 0, 0, '', ''),
(17, 0, 0, 0, '', ''),
(18, 0, 0, 0, '', ''),
(19, 12, 0, 0, 'hiu', 'gol'),
(20, 0, 0, 0, '', ''),
(21, 1, 0, 0, 'diario ', 'correr'),
(22, 1, 0, 0, 'jugar', 'jugar'),
(23, 1, 0, 0, 'bailar', 'bailar'),
(24, 1, 0, 0, 'bailar', 'bailar'),
(25, 1, 0, 0, 'ser', 'ser'),
(26, 1, 0, 0, '1 vez por mes', 'baño'),
(27, 0, 0, 0, '', ''),
(28, 1, 0, 0, '1 vez al día', 'Pelo Spa'),
(29, 0, 0, 0, '', ''),
(30, 85, 13, 0, '1 vez al dia', 'Fisioterapia y rehabilitación'),
(31, 85, 4, 0, '1 vez por mes', 'Corte  de uñas y cuidado cojinete'),
(32, 0, 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mascota_vacuna`
--

CREATE TABLE `mascota_vacuna` (
  `id_mascota_vacuna` int(11) NOT NULL,
  `id_mascota` int(11) NOT NULL,
  `id_vacuna` int(11) NOT NULL,
  `notificacion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `municipio`
--

CREATE TABLE `municipio` (
  `id_municipio` int(11) NOT NULL,
  `id_estado` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `longitud` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `zoom` float NOT NULL,
  `extra` int(11) NOT NULL,
  `ubicado` int(11) NOT NULL,
  `lugares_api` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `municipio`
--

INSERT INTO `municipio` (`id_municipio`, `id_estado`, `nombre`, `latitud`, `longitud`, `zoom`, `extra`, `ubicado`, `lugares_api`) VALUES
(1, 1, 'Cuauhtemoc', '19.4351146985257', '-99.14621204137802', 13, 0, 1, 15),
(2, 5, 'Morelia', '19.702836937817604', '-101.19239419698715', 13, 0, 1, 2),
(3, 1, 'Benito Juarez', '19.382034711010192', '-99.16263788938522', 13, 0, 1, 14),
(4, 1, 'Miguel Hidalgo', '19.43213506882763', '-99.2004382610321', 12, 0, 1, 13),
(13, 14, 'Monterrey', '25.663905862863622', '-100.31164526939392', 11, 0, 1, 3),
(6, 1, 'Naucalpan de Juarez', '19.42622623569175', '-99.2596185207367', 14, 0, 1, 7),
(7, 1, 'Alvaro Obregon ', '19.346071547365323', '-99.19567465782166', 13, 0, 1, 11),
(8, 11, 'Naucalpan ', '19.463294535069608', '-99.24545645713806', 15, 0, 1, 7),
(9, 11, 'Tlalnepantla de Baz', '19.534655456486405', '-99.19035315513611', 14, 0, 1, 20),
(10, 11, 'Cuautitlan Izcalli', '19.652388601751138', '-99.22314047813416', 12, 0, 1, 2),
(11, 1, 'Cuajimalpa', '19.350282673939958', '-99.29077506065369', 13, 0, 1, 5),
(12, 9, 'Zapopan', '20.672720781740043', '-103.41699957847595', 12, 0, 1, 3),
(14, 14, 'Guadalupe', '25.675741889864838', '-100.21517157554626', 12, 0, 1, 3),
(15, 14, 'Apodaca', '25.781749475889878', '-100.18530249595642', 12, 0, 1, 1),
(16, 14, 'San Nicolas de los Garza', '25.755314420313248', '-100.28709769248962', 12, 0, 1, 0),
(17, 14, 'General Escobedo', '25.808333180710537', '-100.31233191490173', 12, 0, 1, 3),
(18, 9, 'Zapopan', '20.672720781740043', '-103.41699957847595', 12, 0, 1, 3),
(19, 1, 'Azcapotzalco', '19.482391923734205', '-99.18640494346619', 14, 0, 1, 10),
(20, 1, 'CoyoacÃ¡n', '19.323090649551997', '-99.15477633476257', 16, 0, 1, 9),
(21, 1, 'Tlalpan', '19.222848101523002', '-99.20666098594666', 14, 0, 1, 3),
(22, 9, 'Guadalajara', '20.681794889782903', '-103.36095213890076', 13, 0, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `notificacion`
--

CREATE TABLE `notificacion` (
  `id_notificacion` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `titulo` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje` varchar(222) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notificacion`
--

INSERT INTO `notificacion` (`id_notificacion`, `tipo`, `titulo`, `mensaje`) VALUES
(4, 0, 'Mensaje de navidad', 'Mensaje de navidad'),
(5, 0, 'Mensaje de fin de año', 'Mensaje del fin del año');

-- --------------------------------------------------------

--
-- Table structure for table `paseador`
--

CREATE TABLE `paseador` (
  `id_paseador` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_mascota` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_nacimiento` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `ano` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `paseador`
--

INSERT INTO `paseador` (`id_paseador`, `id_usuario`, `id_mascota`, `nombre`, `apellido`, `fecha_nacimiento`, `email`, `telefono`, `extra`, `dia`, `mes`, `ano`) VALUES
(1, 2, 3, 'dsad', 'dasdsad', '', 'dasd', 'dasdasd', 0, 4, 4, 2011),
(2, 2, 3, 'dasd', 'adsd', '', 'dasd', 'asdsad', 0, 8, 1, 2019),
(3, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(4, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(5, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(6, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(7, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(8, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(9, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(10, 1, 12, 'pásame', 'pa', '', 'p', 'p', 0, 10, 10, 1988),
(11, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(12, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(13, 2, 1, 'paseador', 'apellido de paseadorwer', '', 'ewe@c.com', '12345', 0, 1, 1, 0),
(14, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(15, 0, 0, '', '', '', '', '', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL,
  `id_marca` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `nombre` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoria_marca` int(11) NOT NULL,
  `id_marca_categoria` int(11) NOT NULL,
  `tipo_mascota` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`id_producto`, `id_marca`, `orden`, `nombre`, `descripcion`, `id_categoria_marca`, `id_marca_categoria`, `tipo_mascota`) VALUES
(1, 8, 0, 'Blue bufalo adultos', '-', 1, 1, 0),
(2, 8, 0, 'Blue Bufalo cachorro', '-', 1, 1, 0),
(3, 6, 0, 'Beneful adulto', '-', 2, 2, 0),
(4, 6, 0, 'Beneful cachorro', '-', 2, 2, 0),
(5, 10, 0, 'Campeon adulto', '-', 4, 4, 0),
(6, 0, 0, 'Ninguna', '-', 4, 4, 0),
(7, 8, 0, 'Blue bufalo adultos', '-', 1, 1, 0),
(8, 9, 0, 'Cachorro', '-', 3, 3, 0),
(9, 9, 0, 'Adulto todas las razas', '-', 3, 3, 0),
(10, 10, 0, 'Cachorro', '-', 4, 4, 0),
(11, 11, 0, 'Cachorro', '-', 5, 5, 0),
(12, 11, 0, 'Adultos', '-', 5, 5, 0),
(13, 12, 0, 'Chop Cachorro', '-', 6, 6, 0),
(14, 12, 0, 'Chop Adulto', '-', 6, 6, 0),
(15, 13, 0, 'Premium Cachorro ', '-', 7, 7, 0),
(16, 13, 0, 'Premium Adulto', '-', 7, 7, 0),
(17, 14, 0, 'Natural cachorro raza grande', '-', 8, 8, 0),
(18, 14, 0, 'Natural cachorro raza pequeña pollo y arroz', '-', 8, 8, 0),
(19, 14, 0, 'Natural Adulto cordero y arroz', '-', 8, 8, 0),
(20, 14, 0, 'Natural Adulto pollo y arroz', '-', 8, 8, 0),
(21, 14, 0, 'Natural Adulto atleta pollo y arroz', '-', 8, 8, 0),
(22, 14, 0, 'Natural Adulto cordero y arroz light ', '-', 8, 8, 0),
(23, 14, 0, 'Natural Senior pollo arroz y avena', '-', 8, 8, 0),
(24, 14, 0, 'Natural Adulto raza pequeña pollo y arroz', '-', 8, 8, 0),
(25, 14, 0, 'Natural Adulto raza pequeña cordero y arroz', '-', 8, 8, 0),
(26, 14, 0, 'Diamond Natural Super Premium ', '-', 8, 8, 0),
(27, 14, 0, 'Premium Cachorro ', '-', 8, 8, 0),
(28, 14, 0, 'Premium perro activo moderado', '-', 8, 8, 0),
(29, 14, 0, 'Premium perro altamente activo', '-', 8, 8, 0),
(30, 14, 0, 'Premium perro atleta', '-', 8, 8, 0),
(31, 14, 0, 'Taste of the Wild PACIFIC STREAM CANINE IN GRAVY', '-', 8, 8, 0),
(32, 14, 0, 'Taste of the Wild WETLANDS CANINE IN GRAVY', '-', 8, 8, 0),
(33, 14, 0, 'Taste of the Wild HIGH PRAIRIE CANINE', '-', 8, 8, 0),
(34, 14, 0, 'Taste of the Wild HIGH PRAIRIE PUPPY', '-', 8, 8, 0),
(35, 14, 0, 'Taste of the Wild PACIFIC STREAM CANINE', '-', 8, 8, 0),
(36, 14, 0, 'Taste of the Wild PACIFIC STREAM PUPPY', '-', 8, 8, 0),
(37, 14, 0, 'Taste of the Wild SIERRA MOUNTAIN CANINE', '-', 8, 8, 0),
(38, 14, 0, 'Taste of the Wild WETLANDS CANINE', '-', 8, 8, 0),
(39, 14, 0, 'Care control de peso', '-', 8, 8, 0),
(40, 14, 0, 'Care estómago sensible', '-', 8, 8, 0),
(41, 14, 0, 'Care piel sensible', '-', 8, 8, 0),
(42, 14, 0, 'Care Rx renal', '-', 8, 8, 0),
(43, 15, 0, 'CACHORROS SIN COLORANTES TODOS LOS TAMAÑOS', '-', 9, 9, 0),
(44, 15, 0, 'Cachorros salud visible minis y pequeños', '-', 9, 9, 0),
(45, 15, 0, 'Cachorros salud visible medianos y grandes', '-', 9, 9, 0),
(46, 15, 0, 'ADULTOS SIN COLORANTES MINIS Y PEQUEÑOS', '-', 9, 9, 0),
(47, 15, 0, 'Adultos control de peso todos los tamaños', '-', 9, 9, 0),
(48, 15, 0, 'ADULTOS 7+ LONGEVIDAD TODOS LOS TAMAÑOS', '-', 9, 9, 0),
(49, 15, 0, 'ADULTOS salud visible medianos y grandes', '-', 9, 9, 0),
(50, 15, 0, 'ADULTOS minis y pequeños', '-', 9, 9, 0),
(51, 16, 0, 'Healthy Extra Cachorro', '-', 10, 10, 0),
(52, 16, 0, 'Adulto Raza Pequeña ', '-', 10, 10, 0),
(53, 16, 0, 'Healthy Extras Adult Small Breed ', '-', 10, 10, 0),
(54, 16, 0, 'Adulto Raza Mediana', '-', 10, 10, 0),
(55, 16, 0, 'Healthy Extras Adult Medium & Large Breed ', '-', 10, 10, 0),
(56, 16, 0, 'Adulto Raza Grande', '-', 10, 10, 0),
(57, 16, 0, 'Premium Performance', '-', 10, 10, 0),
(58, 16, 0, 'Healthy Extra Cachorro', '-', 10, 10, 0),
(59, 16, 0, 'Senior Small Breed', '-', 10, 10, 0),
(60, 16, 0, 'Puppy Small Breed', '-', 10, 10, 0),
(61, 16, 0, 'Senior Medium Breed', '-', 10, 10, 0),
(62, 16, 0, 'Puppy Medium Breed', '-', 10, 10, 0),
(63, 16, 0, 'Control de Peso Raza Grande', '-', 10, 10, 0),
(64, 16, 0, 'Control de Peso Raza Chica', '-', 10, 10, 0),
(65, 16, 0, 'Control de Peso Raza Mediana', '-', 10, 10, 0),
(66, 16, 0, 'Healthy Extras Adult Medium & Large Breed ', '-', 10, 10, 0),
(67, 16, 0, 'Puppy Large Breed', '-', 10, 10, 0),
(68, 17, 0, 'Cachorro raza pequeña', '-', 11, 11, 0),
(69, 17, 0, 'Cachorro', '-', 11, 11, 0),
(70, 17, 0, 'Adulto raza pequeña', '-', 11, 11, 0),
(71, 17, 0, 'Adulto raza mediana y grande', '-', 11, 11, 0),
(72, 17, 0, 'Senior 7+', '-', 11, 11, 0),
(73, 17, 0, 'Peso ideal', '-', 11, 11, 0),
(74, 18, 0, 'Ganador Cachorro todas las razas', '-', 12, 12, 0),
(75, 18, 0, 'Ganador Adulto raza pequeña', '-', 12, 12, 0),
(76, 18, 0, 'Ganador Adulto raza mediana y grande', '-', 12, 12, 0),
(77, 18, 0, 'Ganador Duo Cachorro todas las razas', '-', 12, 12, 0),
(78, 18, 0, 'Ganador Duo Adulto todas las razas', '-', 12, 12, 0),
(79, 18, 0, 'Ganador Premium', '-', 12, 12, 0),
(80, 18, 0, 'Ganador Premium Cachorro raza pequeña', '-', 12, 12, 0),
(81, 18, 0, 'Ganador Premium raza mediana y grande', '-', 12, 12, 0),
(82, 18, 0, 'Ganador Premium Adulto raza pequeña', '-', 12, 12, 0),
(83, 18, 0, 'Ganador Premium raza mediana y grande', '-', 12, 12, 0),
(84, 19, 0, 'Cachorro', '-', 13, 13, 0),
(85, 19, 0, 'Adulto', '-', 13, 13, 0),
(86, 20, 0, 'Cachorro', '-', 14, 14, 0),
(87, 20, 0, 'Adulto', '-', 14, 14, 0),
(88, 21, 0, 'Science Diet Adult Large Breed Dog Food', '-', 15, 15, 0),
(89, 21, 0, 'Science Diet Youthful Vitality Adult 7+ Seco Receta de Pollo y Arroz', '-', 15, 15, 0),
(90, 21, 0, 'Science Diet Adult Active Dog Food', '-', 15, 15, 0),
(91, 21, 0, 'Science Diet, Adult 7+ Youthful Vitality, Chicken & Vegetable Stew, alimento en lata', '-', 15, 15, 0),
(92, 21, 0, 'Science Diet Adult Receta de pollo y cebada', '-', 15, 15, 0),
(93, 21, 0, 'Science Diet Youthful Vitality Adult 7+ Small & Toy Breed Chicken & Rice Recipe, Alimento Seco', '-', 15, 15, 0),
(94, 21, 0, 'Science Diet Adult Light Dog Food', '-', 15, 15, 0),
(95, 21, 0, 'Science Die Adult Light with Liver Dog Food canned', '-', 15, 15, 0),
(96, 21, 0, 'Science Diet Adult Light with Liver Dog Food canned', '-', 15, 15, 0),
(97, 21, 0, 'Science Diet Puppy Healthy Development dry', '-', 15, 15, 0),
(98, 21, 0, 'Science Diet Adult Sensitive Stomach & Skin Dog Food dry', '-', 15, 15, 0),
(99, 21, 0, 'Science Diet Adult Small & Toy Breed Dog Food', '-', 15, 15, 0),
(100, 21, 0, 'Science Diet Puppy Large Breed', '-', 15, 15, 0),
(101, 21, 0, 'Science Diet Puppy Healthy Development Small Bites', '-', 15, 15, 0),
(102, 21, 0, 'Science Diet Puppy Small & Toy Breed', '-', 15, 15, 0),
(103, 21, 0, 'Science Diet Adult 7+ Active Longevity Small Bites Dog Food', '-', 15, 15, 0),
(104, 21, 0, 'Science Diet Adult Light Small & Toy Breed', '-', 15, 15, 0),
(105, 21, 0, 'Science Diet Adult Advanced Fitness Small Bites', '-', 15, 15, 0),
(106, 22, 0, 'Prescription Diet w/d Canine lata', '-', 16, 16, 0),
(107, 22, 0, 'Prescription Diet Canine metabolic lata', '-', 16, 16, 0),
(108, 22, 0, 'Prescription Diet w/d Canine dry', '-', 16, 16, 0),
(109, 22, 0, 'Prescription Diet Canine metabolic dry', '-', 16, 16, 0),
(110, 22, 0, 'Prescription Diet Metabolic + Mobility Canine Vegetable & Tuna Stew lata', '-', 16, 16, 0),
(111, 22, 0, 'Prescription Diet r/d Canine lata', '-', 16, 16, 0),
(112, 22, 0, 'Prescription Diet Metabolic + Mobility Canine dry', '-', 16, 16, 0),
(113, 22, 0, 'Prescription Diet r/d Canine dry', '-', 16, 16, 0),
(114, 22, 0, 'Prescription Diet w/d Canine lata', '-', 16, 16, 0),
(115, 22, 0, 'Prescription Diet z/d Canine lata', '-', 16, 16, 0),
(116, 22, 0, 'Prescription Diet i/d Low Fat Canine lata', '-', 16, 16, 0),
(117, 22, 0, 'Prescription Diet w/d Canine dry', '-', 16, 16, 0),
(118, 22, 0, ' Prescription Diet i/d Canine seco', '-', 16, 16, 0),
(119, 22, 0, 'Prescription Diet a/d Canine/Feline lata', '-', 16, 16, 0),
(120, 22, 0, 'Prescription Diet a/d Canine/Feline lata', '-', 16, 16, 0),
(121, 22, 0, 'Prescription Diet i/d Canine lata', '-', 16, 16, 0),
(122, 22, 0, 'Prescription Diet i/d Small Bites Canine dry', '-', 16, 16, 0),
(123, 22, 0, 'Prescription Diet i/d Low Fat Canine dry', '-', 16, 16, 0),
(124, 22, 0, 'Prescription Diet i/d Low Fat Canine dry', '-', 16, 16, 0),
(125, 22, 0, 'Prescription Diet z/d Canine dry', '-', 16, 16, 0),
(126, 22, 0, 'Prescription Diet z/d Canine lata', '-', 16, 16, 0),
(127, 22, 0, 'Prescription Diet w/d Canine lata', '-', 16, 16, 0),
(128, 22, 0, 'Prescription Diet s/d Canine lata', '-', 16, 16, 0),
(129, 22, 0, 'Prescription Diet i/d Low Fat Canine dry', '-', 16, 16, 0),
(130, 22, 0, 'Prescription Diet c/d Multicare Canine lata', '-', 16, 16, 0),
(131, 22, 0, 'Prescription Diet c/d Multicare Canine Chicken & Vegetable dry', '-', 16, 16, 0),
(132, 22, 0, 'Prescription Diet u/d Canine lata', '-', 16, 16, 0),
(133, 22, 0, 'Prescription Diet u/d Canine dry', '-', 16, 16, 0),
(134, 22, 0, 'Prescription Diet c/d Multicare Canine dry', '-', 16, 16, 0),
(135, 22, 0, 'Prescription Diet k/d Canine', '-', 16, 16, 0),
(136, 22, 0, 'Prescription Diet k/d Canine', '-', 16, 16, 0),
(137, 22, 0, 'Prescription Diet k/d Canine Chicken & Vegetable Stew', '-', 16, 16, 0),
(138, 23, 0, 'Ideal Balance Natural Chicken & Brown Rice Recipe Mature Adult', '-', 17, 17, 0),
(139, 23, 0, 'Ideal Balance? Adult Receta de pollo y arroz integral naturales', '-', 17, 17, 0),
(140, 23, 0, 'Ideal Balance Natural Chicken & Brown Rice Recipe Puppy', '-', 17, 17, 0),
(141, 23, 0, 'Ideal Balance Small Breed Natural Chicken & Brown Rice Recipe Adult', '-', 17, 17, 0),
(142, 23, 0, 'Ideal Balance Adult LIBRE DE GRANOS Receta de pollo y papa naturales', '-', 17, 17, 0),
(143, 24, 0, 'Cachorro pollo y arroz', '-', 18, 18, 0),
(144, 24, 0, 'Natures Domain, alimento de salmón y camote', '-', 18, 18, 0),
(145, 24, 0, 'Natures Domain, alimento de pavo y camote', '-', 18, 18, 0),
(146, 24, 0, 'Adulto cordero y arroz', '-', 18, 18, 0),
(147, 24, 0, 'Adulto pollo y arroz', '-', 18, 18, 0),
(148, 24, 0, 'Pollo y arroz en lata', '-', 18, 18, 0),
(149, 25, 0, 'Natural para todas las razas', '-', 19, 19, 0),
(150, 25, 0, 'Barf premium', '-', 19, 19, 0),
(151, 25, 0, 'Barf al vapor', '-', 19, 19, 0),
(152, 26, 0, 'Cachorro', '-', 20, 20, 0),
(153, 26, 0, 'Raza pequeña', '-', 20, 20, 0),
(154, 26, 0, 'Adulto', '-', 20, 20, 0),
(155, 27, 0, 'Pato y pavo', '-', 21, 21, 0),
(156, 27, 0, 'Salmón', '-', 21, 21, 0),
(157, 27, 0, 'Pollo', '-', 21, 21, 0),
(158, 27, 0, 'Res y cordero', '-', 21, 21, 0),
(159, 27, 0, 'Raw carne', '-', 21, 21, 0),
(160, 27, 0, 'Raw pollo', '-', 21, 21, 0),
(161, 27, 0, 'Raw cordero', '-', 21, 21, 0),
(162, 27, 0, 'ULTIMATE PROTEIN Pollo', '-', 21, 21, 0),
(163, 27, 0, 'ULTIMATE PROTEIN Pato', '-', 21, 21, 0),
(164, 28, 0, 'Sabor pollo y camote', '-', 22, 22, 0),
(165, 28, 0, 'Sabor salmón y camote ', '-', 22, 22, 0),
(166, 28, 0, 'Sabor cordero y res', '-', 22, 22, 0),
(167, 28, 0, 'Bisonte y camote', '-', 22, 22, 0),
(168, 28, 0, 'Pato & Camote', '-', 22, 22, 0),
(169, 28, 0, 'Cerdo', '-', 22, 22, 0),
(170, 28, 0, 'Cordero ', '-', 22, 22, 0),
(171, 28, 0, 'Pescado', '-', 22, 22, 0),
(172, 28, 0, 'Pavo ', '-', 22, 22, 0),
(173, 28, 0, 'Pollo', '-', 22, 22, 0),
(174, 28, 0, 'Res', '-', 22, 22, 0),
(175, 28, 0, 'Trucha', '-', 22, 22, 0),
(176, 29, 0, 'Cachorro', '-', 23, 23, 0),
(177, 29, 0, 'Cachorro Raza Pequeña ', '-', 23, 23, 0),
(178, 29, 0, 'Adulto', '-', 23, 23, 0),
(179, 29, 0, 'Adulto raza pequeña', '-', 23, 23, 0),
(180, 29, 0, 'Senior', '-', 23, 23, 0),
(181, 29, 0, 'Senior raza pequeña', '-', 23, 23, 0),
(182, 30, 0, 'Adulto', '-', 24, 24, 0),
(183, 30, 0, 'Adulto todas las razas', '-', 24, 24, 0),
(184, 30, 0, 'Carne y pollo', '-', 24, 24, 0),
(185, 31, 0, 'Adulto', '-', 25, 25, 0),
(186, 31, 0, 'Adulto todas las razas', '-', 25, 25, 0),
(187, 32, 0, 'Cachorro raza pequeña', '-', 26, 26, 0),
(188, 32, 0, 'Cachorro raza mediana y grande', '-', 26, 26, 0),
(189, 32, 0, 'Adulto raza pequeña', '-', 26, 26, 0),
(190, 32, 0, 'Adulto mediana y grande', '-', 26, 26, 0),
(191, 33, 0, 'Exceed Cordero y Arroz', '-', 27, 27, 0),
(192, 33, 0, 'Sportsmans Choice Fórmula Original', '-', 27, 27, 0),
(193, 33, 0, 'Mark Sportsmans Choice Trozos y Huesos', '-', 27, 27, 0),
(194, 33, 0, 'Sportsmans Choice Alta Proteína', '-', 27, 27, 0),
(195, 33, 0, 'Sportsmans Choice Balance Óptimo', '-', 27, 27, 0),
(196, 33, 0, 'Sportsmans Choice Razas Pequeñas', '-', 27, 27, 0),
(197, 33, 0, 'Sportsmans Choice Cachorro', '-', 27, 27, 0),
(198, 34, 0, 'Cachorro pollo y camote', '-', 28, 28, 0),
(199, 34, 0, 'Adulto cordero y camote', '-', 28, 28, 0),
(200, 34, 0, 'Adulto pato y camote', '-', 28, 28, 0),
(201, 34, 0, 'Adulto pallo y camote', '-', 28, 28, 0),
(202, 34, 0, 'Adulto res y camote', '-', 28, 28, 0),
(203, 34, 0, 'Adulto salmón y camote', '-', 28, 28, 0),
(204, 34, 0, 'Adulto res, pollo y camote', '-', 28, 28, 0),
(205, 34, 0, 'Adulto raza pequeña pollo y camote', '-', 28, 28, 0),
(206, 34, 0, 'Adulto raza pequeña res y camote', '-', 28, 28, 0),
(207, 34, 0, 'Adulto raza pequeña salmón y camote', '-', 28, 28, 0),
(208, 35, 0, 'Leche para cachorro', '-', 29, 29, 0),
(209, 35, 0, 'Papilla para cachorro', '-', 29, 29, 0),
(210, 35, 0, 'Cachorro carne y frutos del bosque seco', '-', 29, 29, 0),
(211, 35, 0, 'Carne y frutos del bosque raza pequeña', '-', 29, 29, 0),
(212, 35, 0, 'Carne y frutos del bosque raza mediana y grande', '-', 29, 29, 0),
(213, 35, 0, 'Cachorro carne y frutos del bosque húmedo ', '-', 29, 29, 0),
(214, 36, 0, 'Cachorro ', '-', 30, 30, 0),
(215, 36, 0, 'Adulto ', '-', 30, 30, 0),
(216, 36, 0, 'Balanceado cachorro', '-', 30, 30, 0),
(217, 36, 0, 'Balanceado Adulto', '-', 30, 30, 0),
(218, 37, 0, 'Pequeños bocados', '-', 31, 31, 0),
(219, 37, 0, 'Cachorro', '-', 31, 31, 0),
(220, 37, 0, 'Adulto', '-', 31, 31, 0),
(221, 38, 0, 'Cachorro ', '-', 32, 32, 0),
(222, 38, 0, 'Cachorro raza pequeña', '-', 32, 32, 0),
(223, 38, 0, 'Adulto', '-', 32, 32, 0),
(224, 38, 0, 'Adulto raza pequeña', '-', 32, 32, 0),
(225, 38, 0, 'Senior', '-', 32, 32, 0),
(226, 38, 0, 'Senior raza pequeña', '-', 32, 32, 0),
(227, 38, 0, '1st Care', '-', 32, 32, 0),
(228, 38, 0, 'High Performance', '-', 32, 32, 0),
(229, 38, 0, 'Weight Control', '-', 32, 32, 0),
(230, 38, 0, 'Weight Control raza pequeña', '-', 32, 32, 0),
(231, 38, 0, 'Sensitive ', '-', 32, 32, 0),
(232, 38, 0, 'Sensitive raza pequeña', '-', 32, 32, 0),
(233, 38, 0, 'Nufit', '-', 32, 32, 0),
(234, 39, 0, 'Adulto carne', '-', 33, 33, 0),
(235, 39, 0, 'Original bebé', '-', 33, 33, 0),
(236, 39, 0, 'Original adulto', '-', 33, 33, 0),
(237, 39, 0, 'Natural cachorro', '-', 33, 33, 0),
(238, 39, 0, 'Natural Adulto', '-', 33, 33, 0),
(239, 40, 0, 'Lata pollo', '-', 34, 34, 0),
(240, 40, 0, 'Lata res', '-', 34, 34, 0),
(241, 40, 0, 'Croqueta pollo ', '-', 34, 34, 0),
(242, 40, 0, 'Croqueta res', '-', 34, 34, 0),
(243, 41, 0, 'Cachorro Razas Medianas y Grandes', '-', 35, 35, 0),
(244, 41, 0, 'Cachorro Razas Pequeñas y Minis', '-', 35, 35, 0),
(245, 41, 0, 'Cachorro Pollo en Filetes', '-', 35, 35, 0),
(246, 41, 0, 'Cachorro Res en Filetes', '-', 35, 35, 0),
(247, 41, 0, 'Cachorro Pollo Carne Molida', '-', 35, 35, 0),
(248, 41, 0, 'Cachorro Res Carne Molida', '-', 35, 35, 0),
(249, 41, 0, 'Cachorro Pollo en Paté', '-', 35, 35, 0),
(250, 41, 0, 'Adulto', '-', 35, 35, 0),
(251, 41, 0, 'Adulto raza pequeña', '-', 35, 35, 0),
(252, 41, 0, 'Adulto Minis', '-', 35, 35, 0),
(253, 41, 0, 'Balance Natural ', '-', 35, 35, 0),
(254, 41, 0, 'Senior', '-', 35, 35, 0),
(255, 41, 0, 'Adulto Pollo en Filetes', '-', 35, 35, 0),
(256, 41, 0, 'Adulto Res en Filetes', '-', 35, 35, 0),
(257, 41, 0, 'Adulto Pollo Carne Molida', '-', 35, 35, 0),
(258, 41, 0, 'Adulto Res Carne Molida', '-', 35, 35, 0),
(259, 41, 0, 'Adulto Razas Pequeñas Pollo en Filetes', '-', 35, 35, 0),
(260, 41, 0, 'Adulto Razas Pequeñas Pollo y Vegetales en Filetes', '-', 35, 35, 0),
(261, 41, 0, 'Adulto Razas Pequeñas Res en Filetes', '-', 35, 35, 0),
(262, 41, 0, 'Adulto Res en Paté', '-', 35, 35, 0),
(263, 41, 0, 'Adulto Pollo en Paté', '-', 35, 35, 0),
(264, 41, 0, 'Balance Natural Pollo Estilo Campirano en Filetes', '-', 35, 35, 0),
(265, 41, 0, 'Balance Natural Res Estilo Campirano en Filetes', '-', 35, 35, 0),
(266, 41, 0, 'Adulto Cordero en Filetes', '-', 35, 35, 0),
(267, 41, 0, 'Adulto Razas Pequeñas Cordero en Filetes', '-', 35, 35, 0),
(268, 41, 0, 'Adulto Razas Pequeñas Pollo en Paté', '-', 35, 35, 0),
(269, 41, 0, 'Adulto Razas Pequeñas Res en Paté', '-', 35, 35, 0),
(270, 41, 0, 'Puppy Milk', '-', 35, 35, 0),
(271, 42, 0, 'Cachorro pollo y arroz', '-', 36, 36, 0),
(272, 42, 0, 'Cachorro Razas Pequeñas Salmón y Arroz', '-', 36, 36, 0),
(273, 42, 0, 'Adulto pollo y arroz', '-', 36, 36, 0),
(274, 42, 0, 'Adulto Cordero y arroz', '-', 36, 36, 0),
(275, 42, 0, 'Adulto Salmón y arroz Razas pequeñas', '-', 36, 36, 0),
(276, 43, 0, 'Pollo y y arroz integral', '-', 37, 37, 0),
(277, 43, 0, 'Salmón y arroz integral', '-', 37, 37, 0),
(278, 43, 0, 'Cordero y avena', '-', 37, 37, 0),
(279, 43, 0, 'Res y cebada', '-', 37, 37, 0),
(280, 44, 0, 'Cachorro pollo y arroz húmedo', '-', 38, 38, 0),
(281, 44, 0, 'Optistart Cachorro raza pequeña', '-', 38, 38, 0),
(282, 44, 0, 'Optistart Cachorro raza mediana', '-', 38, 38, 0),
(283, 44, 0, 'Optistart Cachorro raza grande', '-', 38, 38, 0),
(284, 44, 0, 'Optistart Cachorro raza pequeña', '-', 38, 38, 0),
(285, 44, 0, 'Optistart Cachorro raza grande', '-', 38, 38, 0),
(286, 44, 0, 'Optistart Cachorro raza mediana', '-', 38, 38, 0),
(287, 44, 0, 'Adulto pollo y arroz húmedo', '-', 38, 38, 0),
(288, 44, 0, 'Optipower Perro Adulto', '-', 38, 38, 0),
(289, 44, 0, 'Optihealth adulto raza pequeña', '-', 38, 38, 0),
(290, 44, 0, 'Optihealth adulto raza mediana', '-', 38, 38, 0),
(291, 44, 0, 'Optihealth adulto raza grande', '-', 38, 38, 0),
(292, 44, 0, 'Exigent Optienrich Adulto Raza Pequeña', '-', 38, 38, 0),
(293, 44, 0, 'Reduced Calorie Optifit adulto', '-', 38, 38, 0),
(294, 44, 0, 'Sensitive Digestion Optisensis Cachorro', '-', 38, 38, 0),
(295, 44, 0, 'Sensitive Skin Optiderma Cachorro', '-', 38, 38, 0),
(296, 44, 0, 'Sensitive Digestion Optisensis Adulto', '-', 38, 38, 0),
(297, 45, 0, 'Cachorro pollo', '-', 39, 39, 0),
(298, 45, 0, 'Adulto cordero', '-', 39, 39, 0),
(299, 45, 0, 'Adulto pollo ', '-', 39, 39, 0),
(300, 45, 0, 'Adulto pollo ', '-', 39, 39, 0),
(301, 45, 0, 'Adulto pollo ', '-', 39, 39, 0),
(302, 45, 0, 'Adulto bisonte', '-', 39, 39, 0),
(303, 45, 0, 'Adulto salmón', '-', 39, 39, 0),
(304, 45, 0, 'Adulto pato', '-', 39, 39, 0),
(305, 45, 0, 'Adulto Jabalí', '-', 39, 39, 0),
(306, 46, 0, 'Babydog milk', '-', 40, 40, 0),
(307, 46, 0, 'Starter Mousse', '-', 40, 40, 0),
(308, 46, 0, 'Small Starter', '-', 40, 40, 0),
(309, 46, 0, 'Small Puppy', '-', 40, 40, 0),
(310, 46, 0, 'Small Adult', '-', 40, 40, 0),
(311, 46, 0, 'Small Mature +8', '-', 40, 40, 0),
(312, 46, 0, 'Mini Special', '-', 40, 40, 0),
(313, 46, 0, 'Mini Weight Care', '-', 40, 40, 0),
(314, 46, 0, 'Mini Dermacomfort', '-', 40, 40, 0),
(315, 46, 0, 'Medium Puppy', '-', 40, 40, 0),
(316, 46, 0, 'Medium Weight Care', '-', 40, 40, 0),
(317, 46, 0, 'Medium Dermacomfort', '-', 40, 40, 0),
(318, 46, 0, 'Large Puppy', '-', 40, 40, 0),
(319, 46, 0, 'Large Adult', '-', 40, 40, 0),
(320, 46, 0, 'Large Adult 5+', '-', 40, 40, 0),
(321, 46, 0, 'Maxi Weight Care', '-', 40, 40, 0),
(322, 46, 0, 'Maxi Dermacomfort', '-', 40, 40, 0),
(323, 46, 0, 'Giant Junior', '-', 40, 40, 0),
(324, 46, 0, 'Giant Puppy', '-', 40, 40, 0),
(325, 46, 0, 'Giant Adult', '-', 40, 40, 0),
(326, 46, 0, 'Miniature Schnauzer Puppy', '-', 40, 40, 0),
(327, 46, 0, 'Pug Puppy', '-', 40, 40, 0),
(328, 46, 0, 'Pug ', '-', 40, 40, 0),
(329, 46, 0, 'French Bulldog Adult', '-', 40, 40, 0),
(330, 46, 0, 'Shih Tzu Puppy', '-', 40, 40, 0),
(331, 46, 0, 'Shih Tzu', '-', 40, 40, 0),
(332, 46, 0, 'Shih Tzu', '-', 40, 40, 0),
(333, 46, 0, 'Yorkshire Terrier Puppy', '-', 40, 40, 0),
(334, 46, 0, 'Yorkshire Terrier', '-', 40, 40, 0),
(335, 46, 0, 'Chihuahua Puppy', '-', 40, 40, 0),
(336, 46, 0, 'Chihuahua', '-', 40, 40, 0),
(337, 46, 0, 'Chihuahua Adult WET', '-', 40, 40, 0),
(338, 46, 0, 'Dachshund', '-', 40, 40, 0),
(339, 46, 0, 'Poodle', '-', 40, 40, 0),
(340, 46, 0, 'Bulldog Puppy', '-', 40, 40, 0),
(341, 46, 0, 'Bulldog ', '-', 40, 40, 0),
(342, 46, 0, 'Labrador Retriever Puppy', '-', 40, 40, 0),
(343, 46, 0, 'Labrador Retriever', '-', 40, 40, 0),
(344, 46, 0, 'Boxer', '-', 40, 40, 0),
(345, 46, 0, 'German Shepherd', '-', 40, 40, 0),
(346, 46, 0, 'Yorkshire Terrier WET', '-', 40, 40, 0),
(347, 46, 0, 'Adult Beauty', '-', 40, 40, 0),
(348, 46, 0, 'Mature 8+', '-', 40, 40, 0),
(349, 46, 0, 'Adult Wet all Dogs in Gel', '-', 40, 40, 0),
(350, 46, 0, 'Mature All Dogs Wet in Gel', '-', 40, 40, 0),
(351, 46, 0, 'Puppy all Dogs in Gel', '-', 40, 40, 0),
(352, 46, 0, 'Weight Care Wet in Gel', '-', 40, 40, 0),
(353, 47, 0, 'Cachorro small bites ', '-', 41, 41, 0),
(354, 47, 0, 'Cachorro', '-', 41, 41, 0),
(355, 47, 0, 'Adulto small bites ', '-', 41, 41, 0),
(356, 47, 0, 'Adulto', '-', 41, 41, 0),
(357, 47, 0, 'Skin ', '-', 41, 41, 0),
(358, 47, 0, 'Light', '-', 41, 41, 0),
(359, 48, 0, 'Grain free pollo', '-', 42, 42, 0),
(360, 48, 0, 'Grain free pavo', '-', 42, 42, 0),
(361, 48, 0, 'Grain free res', '-', 42, 42, 0),
(362, 48, 0, 'Grain free pescado', '-', 42, 42, 0),
(363, 48, 0, 'Grain free vegetales, nuez y semillas', '-', 42, 42, 0),
(364, 48, 0, 'Grain free frutas y vegetales', '-', 42, 42, 0),
(365, 48, 0, 'Whole grain pollo', '-', 42, 42, 0),
(366, 48, 0, 'Whole grain pavo', '-', 42, 42, 0),
(367, 48, 0, 'Whole grain res', '-', 42, 42, 0),
(368, 48, 0, 'Limited ingredient pollo', '-', 42, 42, 0),
(369, 48, 0, 'Limited ingredient pavo', '-', 42, 42, 0),
(370, 48, 0, 'Limited ingredient pescado', '-', 42, 42, 0),
(371, 48, 0, 'Limited ingredient pato', '-', 42, 42, 0),
(372, 48, 0, 'Limited ingredient res', '-', 42, 42, 0),
(373, 53, 0, 'Cachorro y Adulto', '-', 47, 47, 0),
(374, 53, 0, 'Adulto todas las razas', '-', 47, 47, 0),
(375, 49, 0, 'Cachorro pollo y salmón', '-', 43, 43, 0),
(376, 49, 0, 'Adulto pollo y carne', '-', 43, 43, 0),
(377, 53, 0, 'Adulto pollo y salmón', '-', 47, 47, 0),
(378, 50, 0, 'Cachorro Pollo ', '-', 44, 44, 0),
(379, 50, 0, 'Adulto raza pequeña pollo', '-', 44, 44, 0),
(380, 50, 0, 'Adulto pollo', '-', 44, 44, 0),
(381, 50, 0, 'Pollo y arroz integral', '-', 44, 44, 0),
(382, 50, 0, 'Salmón y Chícharo', '-', 44, 44, 0),
(383, 50, 0, 'Raza grande pollo y chícharo', '-', 44, 44, 0),
(384, 50, 0, 'Pato y lenteja', '-', 44, 44, 0),
(385, 50, 0, 'Cordero y lenteja', '-', 44, 44, 0),
(386, 50, 0, 'Skin & Coat Care Alimento Natural Adulto Salmon y Chícharo', '-', 44, 44, 0),
(387, 50, 0, 'Skin & Coat Care Alimento Natural Adulto Cordero y Chícharo', '-', 44, 44, 0),
(388, 50, 0, 'Easy Digestion Receta Papa y Huevo', '-', 44, 44, 0),
(389, 50, 0, 'Senior Pollo', '-', 44, 44, 0),
(390, 51, 0, 'Cachorro ', '-', 45, 45, 0),
(391, 51, 0, 'Adulto ', '-', 45, 45, 0),
(392, 51, 0, 'Básico', '-', 45, 45, 0),
(393, 51, 0, 'Force', '-', 45, 45, 0),
(394, 52, 0, 'Cachorro raza mini y pequeño', '-', 46, 46, 0),
(395, 52, 0, 'Cachorro raza mediana y grande', '-', 46, 46, 0),
(396, 52, 0, 'Adulto', '-', 46, 46, 0),
(397, 52, 0, 'Adulto raza mediana y grande', '-', 46, 46, 0);

-- --------------------------------------------------------

--
-- Table structure for table `servicio`
--

CREATE TABLE `servicio` (
  `id_servicio` int(11) NOT NULL,
  `nombre` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `servicio`
--

INSERT INTO `servicio` (`id_servicio`, `nombre`, `descripcion`, `orden`) VALUES
(1, 'Baño completo', '', 1),
(2, 'Corte', '', 2),
(3, 'Baño y corte', '', 3),
(4, 'Limpieza de oidos', '', 4),
(5, 'Corte  de uñas y cuidado cojinetes', '', 5),
(6, 'Spa', '', 6),
(7, 'Cepillado de dientes ', '', 7),
(8, 'Despunte de carita', '', 8),
(9, 'Desenredo de nudos', '', 9),
(10, 'Masaje de relajación baño con aromaterapia', '', 10),
(11, 'Tratamiento terapéutico para piel', '', 11),
(12, 'Reiki', '', 12),
(13, 'Flores de Bach', '', 13),
(14, 'Fisioterapia y rehabilitación', '', 14);

-- --------------------------------------------------------

--
-- Table structure for table `tipo`
--

CREATE TABLE `tipo` (
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL,
  `datos` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tipo`
--

INSERT INTO `tipo` (`id_tipo`, `nombre`, `extra`, `datos`) VALUES
(1, 'Veterinarios', 0, 0),
(2, 'Servicios', 0, 0),
(3, 'Lugares petfriendly', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tips`
--

CREATE TABLE `tips` (
  `id_tips` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tips`
--

INSERT INTO `tips` (`id_tips`, `nombre`, `descripcion`, `extra`) VALUES
(1, 'No olvides agregar tips contantemente', '', 0),
(2, 'Agrega fotos a la galeria todo el tiempo', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_alta` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre`, `apellido`, `email`, `password`, `fecha_alta`) VALUES
(5, 'John', 'Doe', 'babeldublin2019@gmail.com', 'Qwerty19!@', '0000-00-00 00:00:00'),
(2, 'a', 'a', 'a', 'a', '0000-00-00 00:00:00'),
(4, 'Nombre(s)', 'Apellido(s)', 'b', 'b', '0000-00-00 00:00:00'),
(6, 'sebastian', 'aguilar', 'sebastian.aguilar@gmail.com', 'a', '0000-00-00 00:00:00'),
(7, 'Luis', 'Perez', 'luis.perezrosales@gmail.com', 'lapr1981', '0000-00-00 00:00:00'),
(8, 'Adriana', 'Garcia Fandiño', 'garcia.fandino.adriana@gmail.com', 'Badger', '0000-00-00 00:00:00'),
(9, 'cindy', 'lozano', 'lozano.cindy@gmail.com', 'Mexico86', '0000-00-00 00:00:00'),
(39, '', '', '', '', '2019-11-01 02:30:40'),
(38, 'Pedro', 'Pérez', 'pedro.perez@gmail.com', '', '0000-00-00 00:00:00'),
(12, 'Sebastián AguiLar', '', 'sebastian.aguilar@gmail.com', 'Sebastián AguiLar', '2019-07-04 08:13:57'),
(13, 'Sebastián AguiLar', '', 'sebastian.aguilar@gmail.com', 'Sebastián AguiLar', '2019-07-04 08:17:28'),
(14, 'Sebastián AguiLar', '', 'sebastian.aguilar@gmail.com', 'Sebastián AguiLar', '2019-07-04 08:27:23'),
(15, 'Sebastián AguiLar', '', 'sebastian.aguilar@gmail.com', 'Sebastián AguiLar', '2019-07-04 08:29:56'),
(16, 'Sebastián AguiLar', '', 'sebastian.aguilar@gmail.com', 'Sebastián AguiLar', '2019-07-04 08:33:32'),
(17, 'Sebastián AguiLar', '', 'sebastian.aguilar@gmail.com', 'Sebastián AguiLar', '2019-07-04 09:14:27'),
(18, 'Eduardo', 'Ruiz', 'siredrum@hotmail.com', '4dm1n', '0000-00-00 00:00:00'),
(19, 'Sebastián AguiLar', '', 'sebastian.aguilar@gmail.com', 'Sebastián AguiLar', '2019-07-04 20:01:59'),
(20, 'Sebastián AguiLar', '', 'sebastian.aguilar@gmail.com', 'Sebastián AguiLar', '2019-07-04 20:04:00'),
(21, 'Adriana García Fandiño', '', 'adrissg@gmail.com', 'Adriana García Fandiño', '2019-07-04 20:04:57'),
(22, 'Sebastián AguiLar', '', 'sebastian.aguilar@gmail.com', 'Sebastián AguiLar', '2019-07-08 16:22:03'),
(23, 'Morgana Del Rey', '', 'morganadelrey@gmail.com', 'Morgana Del Rey', '2019-07-08 16:37:12'),
(24, 'Sebastián AguiLar', '', 'sebastian.aguilar@gmail.com', 'Sebastián AguiLar', '2019-07-10 16:42:53'),
(25, 'David Perez', '', 'silver_hawk_dava@hotmail.com', 'David Perez', '2019-07-24 14:57:06'),
(26, 'text', 'text', 'Correo electrónico', 'text', '0000-00-00 00:00:00'),
(27, 'Nombre(s)', 'Apellido(s)', 'text', 'text', '0000-00-00 00:00:00'),
(28, 'Play Zone', '', 'kymguerrero16@gmail.com', 'Play Zone', '2019-08-14 17:22:45'),
(29, 'Mike Herrera', '', 'adem_st12@hotmail.com', 'Mike Herrera', '2019-08-16 13:13:09'),
(30, 'Abraham Mendoza', '', 'amb_12345@hotmail.com', 'Abraham Mendoza', '2019-08-16 13:15:14'),
(31, 'Guillermo', 'Uribe', 'guillermoarturo.uribe@gmail.com', 'perrosygatos19', '0000-00-00 00:00:00'),
(32, 'd', 'd', 'd', 'd', '0000-00-00 00:00:00'),
(33, 'b', 'b', 'b', '', '0000-00-00 00:00:00'),
(34, 'c', 'c', 'c', '', '0000-00-00 00:00:00'),
(35, 'e', 'e', 'e', '', '0000-00-00 00:00:00'),
(40, 'David', 'Pérez Espino', 'dpereze1105@hotmail.com', 'Q4zWsx3r', '0000-00-00 00:00:00'),
(41, 'regina', 'urbina', 'regina.urbina@gmail.com', '', '0000-00-00 00:00:00'),
(42, 'iana', 'Slavynska ', 'inslavynska@icloud.com', '', '0000-00-00 00:00:00'),
(43, 'Martín', 'Iglesias', 'martiniglesiase@gmail.com', '12345678', '0000-00-00 00:00:00'),
(44, 'Maria Nieves ', 'Castedo Espeso ', 'mariacastedo_08@hotmail.com', '', '0000-00-00 00:00:00'),
(45, 'Patricia', 'Rosales', 'patricorea@gmail.com', '', '0000-00-00 00:00:00'),
(46, 'fernando', 'maquez', 'fgil@corefix.com.mx', 'getthefunk', '0000-00-00 00:00:00'),
(47, 'David', 'Barahona', 'dbarahona@gmail.com', '', '0000-00-00 00:00:00'),
(48, 'Abril', 'Barahona', 'abrilbarahona@gmail.com', '', '0000-00-00 00:00:00'),
(49, 'fernando', 'gil', 'fgil@corefix.com.mx', '123456', '0000-00-00 00:00:00'),
(50, 'Macarena', 'roach', 'makitaroach@gmail.com', '', '0000-00-00 00:00:00'),
(51, 'Lucero', 'Catalán Pérez ', 'lucero.catalan1096@gmail.com', '', '0000-00-00 00:00:00'),
(52, 'Lucero', 'Catalán Pérez ', 'lucero.catalan1096@gmail.con', '', '0000-00-00 00:00:00'),
(53, '特許庁', '意匠課', 'tokkyo.isyo.2018.204@gmail.com', '', '0000-00-00 00:00:00'),
(54, 'ana ', 'loaiza', 'pinchewera@gmail.com', 'Lorenzo15', '0000-00-00 00:00:00'),
(55, 'patricia margarita', 'Sanchez Reyes', 'pattysanchez84@hotmail.com', 'pamasare', '0000-00-00 00:00:00'),
(56, 'marani', 'ceja', 'ztatusdf@gmail.com', 'chicharito13', '0000-00-00 00:00:00'),
(57, 'Manuel Gustavo', 'Sáenz Ursua', 'manuelsaenz@outlook.com', 'Grandeza01', '0000-00-00 00:00:00'),
(58, 'Rodrigo ', 'Sámano', 'rsamanolara@gmail.com', 'Elefante2014', '0000-00-00 00:00:00'),
(59, 'john', 'doe', 'rxplus2018rxplus2018@gmail.com', 'Googl3t3st!', '0000-00-00 00:00:00'),
(60, 'bonny', 'clyde', 'elgoogclass3@gmail.com', ' 1needed', '0000-00-00 00:00:00'),
(61, 'Espanhussos Elgoog', '', 'elgoogclass3@gmail.com', 'Espanhussos Elgoog', '2020-01-03 04:35:52'),
(62, 'Rubén', 'AlPo', 'rub3obipint@live.com.mx', '', '0000-00-00 00:00:00'),
(63, 'Nancy ', 'Cardenas ', 'nancycardenas.05@gmail.com', '', '0000-00-00 00:00:00'),
(64, 'Diana', 'Ramos', 'dramos@thgye.com.ec', '', '0000-00-00 00:00:00'),
(65, 'Yusleydi', 'Fernandez', 'ydelmonte2015@gmail.com', 'Goon2020', '0000-00-00 00:00:00'),
(66, 'yus', 'fer', 'ydelmonte2015@gmail.com', 'aaa', '0000-00-00 00:00:00'),
(67, 'Fatima', 'Castiglione Maldonado', 'castiglionemaldonado@gmail.com', 'yOtraMas234.', '0000-00-00 00:00:00'),
(68, 'Roger', 'Velazquez', 'brandy.bubble@gmail.com', 'Temporal25', '0000-00-00 00:00:00'),
(69, 'Evelyn', 'Vega', 'joannavega1907@gmail.com', 'ojitos...', '0000-00-00 00:00:00'),
(70, 'keka', 'machu', 'jessia708@gmail.com', '', '0000-00-00 00:00:00'),
(71, 'oscar', 'maldonado', 'ozbulnes@gmail.com', 'osez20', '0000-00-00 00:00:00'),
(72, 'José Luis', 'Basantes', 'asesorias.legal2016@gmail.com', '', '0000-00-00 00:00:00'),
(73, 'Alexis', 'Mejia', 'yacallate.si@gmail.com', 'Wacareada5', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `usuario_evento`
--

CREATE TABLE `usuario_evento` (
  `id_usuario_evento` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_mascota` int(11) NOT NULL,
  `evento` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `id_evento` int(11) NOT NULL,
  `asunto` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `lugar` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `anio` int(11) NOT NULL,
  `hora` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `duracion` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usuario_evento`
--

INSERT INTO `usuario_evento` (`id_usuario_evento`, `id_usuario`, `id_mascota`, `evento`, `id_evento`, `asunto`, `lugar`, `dia`, `mes`, `anio`, `hora`, `duracion`, `extra`) VALUES
(7, 2, 1, 'PASEO', 0, 'Gahua', 'Haya', 12, 7, 2019, '654', '1 Hora', 1),
(6, 2, 1, 'PASEO', 0, 'Had', 'Jsjs', 3, 5, 2019, 'Jsus', 'Hhs', 1),
(5, 2, 1, 'VACUNAS', 5, 'ertret', 'tertert', 22, 7, 2019, '5643', '654', 1),
(4, 2, 1, 'PASEO', 0, 'werwer', 'fsdfdsf', 20, 7, 2019, '45.86', '3 dfv v', 1),
(8, 2, 1, 'EVENTO', 4, 'Bajaua', 'Hjajs', 9, 7, 2019, 'Jajajja', 'Bajjaja', 1),
(9, 2, 1, 'PASEO', 0, 'paseo', 'cdmx', 7, 8, 2019, '', '', 1),
(10, 23, 1, 'PASEO', 0, 'jsjs', 'cdmx', 19, 7, 2019, '', '', 1),
(11, 23, 1, 'PASEO', 0, 'gdd', 'grg', 19, 7, 2019, 'g', 'gh', 1),
(12, 23, 1, 'CITA VETERINARIO', 2, 'rx', 'gb', 5, 7, 2019, 'dgg', 'df', 1),
(13, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(14, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(15, 23, 1, 'CITA VETERINARIO', 2, 'cambio de vendas', 'pet', 24, 7, 2019, '', '', 1),
(16, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(17, 2, 1, 'SERVICIOS', 3, 'Cita medica', 'Centro medico', 12, 7, 2019, '09:00am', '1 hora', 1),
(18, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(19, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(20, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(21, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(22, 2, 1, 'PASEO', 0, 'j', 'j', 5, 10, 2019, 'hgg', 'hhu', 1),
(23, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(24, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(25, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(26, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(27, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(28, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(29, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(30, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(31, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(32, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(33, 2, 1, 'VACUNAS', 5, 'a', 'a', 30, 8, 2019, 'a', 'a', 1),
(34, 2, 1, 'PASEO', 0, 'a', 'a', 30, 8, 2019, 'a', 'a', 1),
(35, 2, 1, 'CITA VETERINARIO', 2, 'a', 'a', 30, 8, 2019, 'a', '1', 1),
(36, 2, 1, 'EVENTO', 4, 'e', 'e', 30, 8, 2019, 'e', '1', 1),
(37, 2, 1, 'PASEO', 0, 'a', 'a', 30, 9, 2019, '12', '1 hora', 1),
(38, 2, 1, 'VACUNAS', 5, 'v', 'v', 30, 9, 2019, '4', '2 horas', 1),
(39, 2, 1, 'CITA VETERINARIO', 2, 'c', 'c', 30, 9, 2019, '5', '3 horas', 1),
(40, 2, 1, 'EVENTO', 4, 'p', 'p', 30, 9, 2019, 'p', 'p', 1),
(41, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(42, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(43, 23, 1, 'PASEO', 0, 'jj', 'j', 15, 11, 2019, '4', '1', 1),
(44, 2, 1, 'SERVICIOS', 3, 'pr', 'pr', 11, 12, 2019, 'y', 'y', 1),
(45, 49, 1, 'CITA VETERINARIO', 2, 'vacuna', 'veterinarip', 7, 12, 2019, '4', '1', 1),
(46, 2, 1, 'PASEO', 0, 'ghf', 'guj', 1, 2, 2019, '', '', 1),
(47, 23, 1, 'PASEO', 0, 'paseo', 'cdmx', 17, 12, 2019, '', '', 1),
(48, 23, 1, 'PASEO', 0, 'h', 'h', 22, 12, 2021, '', '', 1),
(49, 23, 1, 'EVENTO', 4, 'j', 'j', 23, 12, 2019, '', 'x', 1),
(50, 23, 1, 'PASEO', 0, 'p', 'p', 31, 12, 2019, '', '', 1),
(51, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 1),
(52, 66, 1, 'PASEO', 0, 'tr', 'tr', 1, 1, 2021, '', '', 1),
(53, 66, 1, 'PASEO', 0, 't', 't', 1, 2, 2019, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `usuario_foto`
--

CREATE TABLE `usuario_foto` (
  `id_usuario_foto` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_foto` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usuario_foto`
--

INSERT INTO `usuario_foto` (`id_usuario_foto`, `id_usuario`, `id_foto`, `fecha`) VALUES
(1, 2, 79, '2019-07-19 21:27:14'),
(2, 2, 77, '2019-07-19 21:32:40'),
(3, 2, 74, '2019-07-19 22:09:42'),
(4, 0, 0, '2019-07-20 08:31:29'),
(5, 23, 79, '2019-07-22 13:26:27'),
(6, 23, 78, '2019-07-22 13:26:35'),
(7, 23, 73, '2019-07-22 13:27:19'),
(8, 0, 0, '2019-07-23 21:03:40'),
(9, 0, 0, '2019-07-26 23:09:19'),
(10, 0, 0, '2019-07-28 20:30:10'),
(11, 0, 0, '2019-07-29 11:41:21'),
(12, 25, 79, '2019-07-31 23:01:33'),
(13, 25, 78, '2019-07-31 23:01:38'),
(14, 25, 77, '2019-07-31 23:01:42'),
(15, 25, 76, '2019-07-31 23:01:45'),
(16, 0, 0, '2019-08-06 06:34:49'),
(17, 0, 0, '2019-08-08 12:18:08'),
(18, 0, 0, '2019-08-09 17:51:11'),
(19, 27, 81, '2019-08-14 10:46:52'),
(20, 26, 81, '2019-08-14 11:15:50'),
(21, 0, 0, '2019-08-19 21:11:41'),
(22, 0, 0, '2019-08-20 13:44:19'),
(23, 0, 0, '2019-08-25 15:26:57'),
(24, 0, 0, '2019-08-30 21:07:47'),
(25, 0, 0, '2019-08-31 13:18:58'),
(26, 2, 81, '2019-09-04 17:19:51'),
(27, 2, 78, '2019-09-14 19:01:32'),
(28, 0, 0, '2019-09-15 04:26:38'),
(29, 0, 0, '2019-09-15 15:56:35'),
(30, 0, 0, '2019-09-23 08:37:07'),
(31, 0, 0, '2019-09-26 14:24:14'),
(32, 0, 0, '2019-09-28 00:46:53'),
(33, 0, 0, '2019-10-01 17:54:36'),
(34, 0, 0, '2019-10-05 22:38:25'),
(35, 0, 0, '2019-10-09 00:38:09'),
(36, 0, 0, '2019-10-13 17:11:33'),
(37, 0, 0, '2019-10-19 23:01:30'),
(38, 0, 0, '2019-10-27 01:48:12'),
(39, 0, 0, '2019-10-29 06:59:06'),
(40, 0, 0, '2019-11-01 02:29:18'),
(41, 0, 0, '2019-11-02 15:31:31'),
(42, 0, 0, '2019-11-17 14:08:37'),
(43, 0, 0, '2019-11-24 01:54:34'),
(44, 43, 78, '2019-11-27 14:22:53'),
(45, 43, 77, '2019-11-27 14:22:57'),
(46, 43, 76, '2019-11-27 14:22:59'),
(47, 43, 79, '2019-11-27 14:23:02'),
(48, 0, 0, '2019-11-28 14:08:22'),
(49, 0, 0, '2019-11-29 18:35:43'),
(50, 0, 0, '2019-11-29 18:56:49'),
(51, 2, 76, '2019-12-04 12:21:42'),
(52, 2, 60, '2019-12-07 10:22:05'),
(53, 2, 62, '2019-12-07 10:22:13'),
(54, 2, 64, '2019-12-07 10:22:17'),
(55, 2, 71, '2019-12-07 10:39:06'),
(56, 23, 74, '2019-12-09 08:19:51'),
(57, 0, 0, '2019-12-12 10:55:56'),
(58, 0, 0, '2019-12-13 00:57:49'),
(59, 23, 75, '2019-12-17 11:45:11'),
(60, 0, 0, '2019-12-20 16:23:07'),
(61, 0, 0, '2019-12-22 10:17:27'),
(62, 0, 0, '2019-12-23 11:53:02'),
(63, 0, 0, '2019-12-23 12:12:55'),
(64, 0, 0, '2020-01-10 23:03:56'),
(65, 0, 0, '2020-01-20 15:44:39'),
(66, 0, 0, '2020-01-24 07:52:41'),
(67, 0, 0, '2020-01-26 02:12:21'),
(68, 0, 0, '2020-02-04 21:34:36'),
(69, 0, 0, '2020-02-06 03:35:11'),
(70, 0, 0, '2020-02-22 00:27:20'),
(71, 0, 0, '2020-02-22 03:43:21');

-- --------------------------------------------------------

--
-- Table structure for table `vacuna`
--

CREATE TABLE `vacuna` (
  `id_vacuna` int(11) NOT NULL,
  `tipo_mascota` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vacuna`
--

INSERT INTO `vacuna` (`id_vacuna`, `tipo_mascota`, `nombre`, `descripcion`, `orden`) VALUES
(1, 1, 'RABIA', '', 1),
(2, 1, 'DESPARACITACIÓN', '', 2),
(3, 1, 'CORONAVIRUS', '', 3),
(4, 1, 'PARVOVIRUS', '', 4),
(5, 1, 'MOQUILLO/DISTEMPER', '', 5),
(6, 1, 'HEPATITIS', '', 6),
(7, 1, 'PARAINFLUENZA', '', 7),
(8, 1, 'LEPTOSPIRA', '', 8),
(9, 1, 'GIARDIA', '', 9);

-- --------------------------------------------------------

--
-- Table structure for table `vacuna_historial`
--

CREATE TABLE `vacuna_historial` (
  `id_vacuna_historial` int(100) NOT NULL,
  `id_mascota` int(100) NOT NULL,
  `fecha_aplicacion` date NOT NULL,
  `fecha_proxima` date NOT NULL,
  `id_veterinario` int(100) NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `extra` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `veterinario`
--

CREATE TABLE `veterinario` (
  `id_veterinario` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_mascota` int(11) NOT NULL,
  `nombre` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_nacimiento` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `extra` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `ano` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `veterinario`
--

INSERT INTO `veterinario` (`id_veterinario`, `id_usuario`, `id_mascota`, `nombre`, `apellido`, `fecha_nacimiento`, `email`, `telefono`, `extra`, `dia`, `mes`, `ano`) VALUES
(1, 2, 3, 'tret', 'dfgfdg', '', 'rtet', 'dfgfdg', 0, 1, 1, 2019),
(2, 2, 3, 'sdfdsf', 'fsdf', '', 'sdf', '424342', 0, 3, 1, 2019),
(3, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(4, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(5, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(6, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(7, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(8, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(9, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(10, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(11, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(12, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(13, 0, 0, '', '', '', '', '', 0, 0, 0, 0),
(14, 2, 1, 'ver', 'jus', '', 'hola', '5534679954', 0, 4, 1, 1953),
(15, 2, 85, ' sebs', 'ag', '', 'seb', '555555555', 0, 1, 11, 2008),
(16, 0, 0, '', '', '', '', '', 0, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apimaps`
--
ALTER TABLE `apimaps`
  ADD PRIMARY KEY (`id_lugar`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `categoria_entrada`
--
ALTER TABLE `categoria_entrada`
  ADD PRIMARY KEY (`id_categoria_entrada`);

--
-- Indexes for table `categoria_marca`
--
ALTER TABLE `categoria_marca`
  ADD PRIMARY KEY (`id_categoria_marca`);

--
-- Indexes for table `codueno`
--
ALTER TABLE `codueno`
  ADD PRIMARY KEY (`id_codueno`);

--
-- Indexes for table `dueno`
--
ALTER TABLE `dueno`
  ADD PRIMARY KEY (`id_dueno`);

--
-- Indexes for table `entrada`
--
ALTER TABLE `entrada`
  ADD PRIMARY KEY (`id_entrada`);

--
-- Indexes for table `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id_foto`);

--
-- Indexes for table `frecuencia`
--
ALTER TABLE `frecuencia`
  ADD PRIMARY KEY (`id_frecuencia`);

--
-- Indexes for table `gps_test`
--
ALTER TABLE `gps_test`
  ADD PRIMARY KEY (`id_gps`);

--
-- Indexes for table `lugar`
--
ALTER TABLE `lugar`
  ADD PRIMARY KEY (`id_lugar`);

--
-- Indexes for table `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id_marca`);

--
-- Indexes for table `mascota`
--
ALTER TABLE `mascota`
  ADD PRIMARY KEY (`id_mascota`);

--
-- Indexes for table `mascota_alimento`
--
ALTER TABLE `mascota_alimento`
  ADD PRIMARY KEY (`id_mascota_alimento`);

--
-- Indexes for table `mascota_foto`
--
ALTER TABLE `mascota_foto`
  ADD PRIMARY KEY (`id_foto`);

--
-- Indexes for table `mascota_producto`
--
ALTER TABLE `mascota_producto`
  ADD PRIMARY KEY (`id_mascota_producto`);

--
-- Indexes for table `mascota_servicio`
--
ALTER TABLE `mascota_servicio`
  ADD PRIMARY KEY (`id_mascota_servicio`);

--
-- Indexes for table `mascota_vacuna`
--
ALTER TABLE `mascota_vacuna`
  ADD PRIMARY KEY (`id_mascota_vacuna`);

--
-- Indexes for table `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`id_municipio`);

--
-- Indexes for table `notificacion`
--
ALTER TABLE `notificacion`
  ADD PRIMARY KEY (`id_notificacion`);

--
-- Indexes for table `paseador`
--
ALTER TABLE `paseador`
  ADD PRIMARY KEY (`id_paseador`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indexes for table `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id_servicio`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indexes for table `tips`
--
ALTER TABLE `tips`
  ADD PRIMARY KEY (`id_tips`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indexes for table `usuario_evento`
--
ALTER TABLE `usuario_evento`
  ADD PRIMARY KEY (`id_usuario_evento`);

--
-- Indexes for table `usuario_foto`
--
ALTER TABLE `usuario_foto`
  ADD PRIMARY KEY (`id_usuario_foto`);

--
-- Indexes for table `vacuna`
--
ALTER TABLE `vacuna`
  ADD PRIMARY KEY (`id_vacuna`);

--
-- Indexes for table `vacuna_historial`
--
ALTER TABLE `vacuna_historial`
  ADD PRIMARY KEY (`id_vacuna_historial`);

--
-- Indexes for table `veterinario`
--
ALTER TABLE `veterinario`
  ADD PRIMARY KEY (`id_veterinario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apimaps`
--
ALTER TABLE `apimaps`
  MODIFY `id_lugar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=900;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `categoria_entrada`
--
ALTER TABLE `categoria_entrada`
  MODIFY `id_categoria_entrada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categoria_marca`
--
ALTER TABLE `categoria_marca`
  MODIFY `id_categoria_marca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `codueno`
--
ALTER TABLE `codueno`
  MODIFY `id_codueno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `dueno`
--
ALTER TABLE `dueno`
  MODIFY `id_dueno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `entrada`
--
ALTER TABLE `entrada`
  MODIFY `id_entrada` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `estado`
--
ALTER TABLE `estado`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `foto`
--
ALTER TABLE `foto`
  MODIFY `id_foto` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `frecuencia`
--
ALTER TABLE `frecuencia`
  MODIFY `id_frecuencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `gps_test`
--
ALTER TABLE `gps_test`
  MODIFY `id_gps` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `lugar`
--
ALTER TABLE `lugar`
  MODIFY `id_lugar` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `marca`
--
ALTER TABLE `marca`
  MODIFY `id_marca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `mascota`
--
ALTER TABLE `mascota`
  MODIFY `id_mascota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `mascota_alimento`
--
ALTER TABLE `mascota_alimento`
  MODIFY `id_mascota_alimento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `mascota_foto`
--
ALTER TABLE `mascota_foto`
  MODIFY `id_foto` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mascota_producto`
--
ALTER TABLE `mascota_producto`
  MODIFY `id_mascota_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `mascota_servicio`
--
ALTER TABLE `mascota_servicio`
  MODIFY `id_mascota_servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `mascota_vacuna`
--
ALTER TABLE `mascota_vacuna`
  MODIFY `id_mascota_vacuna` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `municipio`
--
ALTER TABLE `municipio`
  MODIFY `id_municipio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `notificacion`
--
ALTER TABLE `notificacion`
  MODIFY `id_notificacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `paseador`
--
ALTER TABLE `paseador`
  MODIFY `id_paseador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=398;

--
-- AUTO_INCREMENT for table `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id_servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id_tipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tips`
--
ALTER TABLE `tips`
  MODIFY `id_tips` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `usuario_evento`
--
ALTER TABLE `usuario_evento`
  MODIFY `id_usuario_evento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `usuario_foto`
--
ALTER TABLE `usuario_foto`
  MODIFY `id_usuario_foto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `vacuna`
--
ALTER TABLE `vacuna`
  MODIFY `id_vacuna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `vacuna_historial`
--
ALTER TABLE `vacuna_historial`
  MODIFY `id_vacuna_historial` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `veterinario`
--
ALTER TABLE `veterinario`
  MODIFY `id_veterinario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
