DELIMITER ;

CREATE PROCEDURE GetCustomers() 
BEGIN
    DECLARE idUsuario INT DEFAULT 1;
	CREATE TEMPORARY TABLE IF NOT EXISTS mascota_temp
	SELECT id_mascota
	FROM mascota
	WHERE id_usuario = idUsuario
	
    
    CREATE TEMPORARY TABLE IF NOT EXISTS codueno_temp
    SELECT id_codueno, id_mascota, id_usuario
    FROM codueno
    WHERE id_mascota IN (SELECT id_mascota
                        FROM mascota_temp)
                        
	IF EXISTS(select 1 from codueno_temp)
    BEGIN
    	INSERT INTO dueno (id_usuario, id_mascota, nombre, apellido, fecha_nacimiento, email, telefono, extra, dia, mes, ano)
        SELECT id_usuario, id_mascota, nombre, apellido, fecha_nacimiento, email, telefono, extra, dia, mes, ano
        FROM codueno, codueno_temp
        WHERE codueno.id_codueno = codueno_temp.id_codueno
        AND codueno.id_mascota = codueno_temp.id_mascota
        AND codueno.id_usuario = codueno_temp.id_usuario
        ORDER BY codueno.id_codueno ASC
    END
    ELSE
    BEGIN
    	DELETE FROM mascota
        WHERE id_usuario = idUsuario

		DELETE FROM mascota_alimento
        WHERE id_mascota IN (SELECT id_mascota
                             FROM mascota_temp)

        DELETE FROM mascota_producto 
        WHERE id_mascota IN (SELECT id_mascota
                             FROM mascota_temp)

        DELETE FROM mascota_servicio
        WHERE id_mascota IN (SELECT id_mascota
                             FROM mascota_temp)

        DELETE FROM mascota_vacuna
        WHERE id_mascota IN (SELECT id_mascota
                             FROM mascota_temp)

        DELETE FROM paseador
		WHERE id_mascota IN (SELECT id_mascota
                             FROM mascota_temp)        
        DELETE FROM veterinario
        WHERE id_mascota IN (SELECT id_mascota
                             FROM mascota_temp)

	END

	DELETE FROM dueno
    WHERE id_usuario IN (SELECT id_mascota
                         FROM mascota_temp)
                         
	DELETE FROM usuario
    WHERE id_usuario = idUsuario
    
    DELETE FROM usuario_evento
    WHERE id_usuario = idUsuario
    
    DELETE FROM usuario_foto
    WHERE id_usuario = idUsuario
    
    return 0

END;

DELIMITER ;